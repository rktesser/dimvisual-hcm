#ifndef DIMVCOLLECTOR_H
#define DIMVCOLLECTOR_H

#import <Foundation/NSObject.h>
#import <Foundation/NSArray.h>
#import <Foundation/NSString.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSConnection.h>
#import <Foundation/NSPortNameServer.h>
#import <Foundation/NSRunLoop.h>
#import <Foundation/NSEnumerator.h>
#import <Foundation/NSThread.h>
#import <Foundation/NSDistantObject.h>
#import <Foundation/NSHost.h>
#import <Foundation/NSThread.h>
#import <Foundation/NSLock.h>
#import <Foundation/NSValue.h>

#import "Observer.h"
#import "DIMVCollectorProto.h"
#import "DIMVAggregatorProto.h"
#import "DIMVConstants.h"

#define COLLECTORNOTINITIALIZED_COND 0
#define COLLECTORINITIALIZED_COND 1

@interface DIMVCollector: NSObject <DIMVCollector>
{
 NSString *collectorId;
 NSString *type;
 NSMutableDictionary *aggregators;
 NSMutableDictionary *subscribers;
 NSThread *connectionThread;//Thread where the comunication is handled.
 NSConditionLock *connectionLock;//Used to wait until the connections are made.
}


//methods required by the DIMVCollector protocol
- (void)subscribe: (NSString *)aggregatorId;
- (void)unsubscribe: (NSString *)aggregatorId;
- (NSString *)getId;
- (NSString *)getType;

//initialize and register the collector with a set of aggregators
- (DIMVCollector *)initWithId: (NSString *)collectorId andType: (NSString *)colType andAggregators: (NSArray *)aggregatorNames;

- (void) dealloc;
- (id<DIMVAggregator>)connectWithAggregator: (NSString *)aggregatorName;
- (BOOL)registerWithAggregator: (id<DIMVAggregator>)aggregator;

#ifndef DIMV_EVALUATE
- (void)sendData: (id)data;
#else
- (void)sendData: (id)data counter: (unsigned)counter;
#endif

@end

#endif
