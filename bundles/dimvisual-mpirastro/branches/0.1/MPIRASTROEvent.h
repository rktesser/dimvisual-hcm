/*
   Copyright (c) 2005 Lucas Mello Schnorr <schnorr@gmail.com>
   
   This file is part of DIMVisual.
   
   DIMVisual is free software; you can redistribute it and/or modify it under
   the terms of the GNU Lesser General Public License as published by the
   Free Software Foundation; either version 2 of the License, or (at your
   option) any later version.
   
   DIMVisual is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
   for more details.
   
   You should have received a copy of the GNU Lesser General Public License
   along with DIMVisual; if not, write to the Free Software Foundation, Inc.,
   59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/
#ifndef __MPILIBRASTROEVENT_H
#define __MPILIBRASTROEVENT_H
#include <Foundation/Foundation.h>
#include <DIMVisual/Protocols.h>
#include <rastro_public.h>

@interface MPIRASTROEvent : NSObject <Event>
{
	NSString *type;
	NSString *time;
	NSString *machine;
	NSString *thread;
	NSString *node;
	
	NSMutableArray *adouble;
	NSMutableArray *afloat;
	NSMutableArray *aint8;
	NSMutableArray *aint64;
	NSMutableArray *aint32;
	NSMutableArray *aint16;
	NSMutableArray *astring;
}
- initWithEvent: (rst_event_t *) event withProvider: (id) provider;
- (void) addDoubleValue: (double) value;
- (void) addFloatValue: (float) value;
- (void) addInt8Value: (u_int8_t) value;
- (void) addInt64Value: (u_int64_t) value;
- (void) addInt32Value: (u_int32_t) value;
- (void) addInt16Value: (u_int16_t) value;
- (void) addStringValue: (char *) value;
- (NSString *)doubleAtIndex: (int) i;
- (NSString *)floatAtIndex: (int) i;
- (NSString *)int8AtIndex: (int) i;
- (NSString *)int16AtIndex: (int) i;
- (NSString *)int32AtIndex: (int) i;
- (NSString *)int64AtIndex: (int) i;
- (NSString *)stringAtIndex: (int) i;
- (NSString *) machine;
- (NSString *) thread;
- (NSString *) node;
@end

#endif
