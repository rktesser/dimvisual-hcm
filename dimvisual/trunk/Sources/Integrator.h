/*
    DIMVisual, the Data Integration Model for Visualization of trace files.
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __INTEGRATOR_H_
#define __INTEGRATOR_H_

#include <Foundation/Foundation.h>
#include <Paje/PajeHierarchy.h>
#include <GenericEvent/GTimestamp.h>
#include "Order.h"
#include "Protocols.h"
#include "Command.h"
#include "DIMV-HCM/DIMVClient.h"

#define CLIENTIDKEY @"clientId"
#define AGGREGATORSKEY @"aggregators"

@interface Integrator : NSObject <Integrator, HCMSubscriptionHandler>
{
	PajeHierarchy *hierarchy;
	Order *ordering;
	NSMutableDictionary *containers;
	BOOL usesHCM;
	NSMutableDictionary *registeredCollectors;
	DIMVClient *HCMClient;
	id<HCMSubscriptionHandler> subscriptionHandler;
}

- (void)newDIMVClientWithArgs: (id)args;
- (NSDictionary *)getRegisteredCollectors;
@end

#endif
