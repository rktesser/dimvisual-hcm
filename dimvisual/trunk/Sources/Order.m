/*
    DIMVisual, the Data Integration Model for Visualization of trace files.
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "Order.h"

@implementation Order
- (id) convert
{
	int i;
	id<DataSource> firstOverall = nil;
	NSMutableArray *ret = nil;
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	
	firstOverall = (id<DataSource>)[self mostRecent];
	if (firstOverall == nil){
		return nil;
	}

	ret = [firstOverall convert];
	if (ret == nil){
		[sources removeObjectIdenticalTo: firstOverall];
		return [self convert];
	}
	if ([ret count] == 0){
		return ret;
	}
	[ret retain];
	if (firstTime == nil){
		firstTime = [[ret objectAtIndex: 0] time];
		[firstTime retain];
	}

	for (i = 0; i < [ret count]; i++){
		[[ret objectAtIndex: i] subtractFloatTimeByThis: firstTime];
	}
	[pool release];
	[ret autorelease];
	return ret;
}
@end
