#ifndef DIMVCOLPROTO_DEF
#define DIMVCOLPROTO_DEF

@protocol DIMVCollector

//register the subscription of an aggregator to the collector
- (oneway void) subscribe: (bycopy NSString *) aggregatorId;
 
//cancel the subscription of an aggregator to the collector
- (oneway void) unsubscribe: (bycopy NSString *) aggregatorId;

- (bycopy NSString *) getId;
- (bycopy NSString *) getType;
- (bycopy NSString *) getLocation;

//test to discover how to pass arguments by copy
//- (void) testArg: (bycopy NSMutableString *)testStr withAggreg: (id)aggreg;

- (oneway void)subscribeComponent: (bycopy NSString*)componentId toCollector:
(bycopy NSString *)collectorId; 

- (oneway void)unsubscribeComponent: (bycopy NSString*)componentId toCollector:
(bycopy NSString *)collectorId; 

@end
#endif
