/*
    DIMVisual-KAAPI, the DIMVisual bundle for KAAPI trace files
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual-KAAPI.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "KAAPIParser.h"
#include <GenericEvent/GEvent.h>

extern double gettime();
#define AVALSTIME double t1 = gettime();
#define AVALETIME(str) double t2=gettime();\
		fprintf (stderr, "%s = %f\n", str, t2-t1);

@implementation KAAPIParser (Util)
- (GEvent *) threadName: (Util::RecordReader::const_iterator) r
{
	GEvent *ev;
	GName *name;
	GIdentifier *nameid;
	GFields *fields;

	ev = [[GEvent alloc] init];
	name = [[GName alloc] init]; 
	fields = [[GFields alloc] init];
	nameid = [[GIdentifier alloc] init];

	int namev;
	namev = (int)*(char *)r->structure->get_field_data("name", r->buffer);
	[nameid addValue: [NSString stringWithFormat: @"name"]];
	[fields setFieldWithKey: nameid withValue: [NSString stringWithFormat: @"%d", namev]];

	[name setName: @"EVT_UTIL_THREAD_NAME"];
	[ev setName: name];	
	[ev setFields: fields];

	[name release];
	[fields release];
	[nameid release];

	[ev autorelease];
//	r->structure->print (std::cout, r->buffer);
//	NSLog (@"ev=%@", ev);
	return ev;
}

- (GEvent *) threadStart: (Util::RecordReader::const_iterator) r
{
	GEvent *ev;
	GName *name;
	GIdentifier *contextid;
	GFields *fields;

	ev = [[GEvent alloc] init];
	name = [[GName alloc] init]; 
	fields = [[GFields alloc] init];
	contextid = [[GIdentifier alloc] init];

	int cids;
	cids = (int)*(char *)r->structure->get_field_data("cid", r->buffer);

	[contextid addValue: [NSString stringWithFormat: @"cid-created"]];
	[fields setFieldWithKey: contextid withValue: [NSString stringWithFormat: @"%d", cids]];

	[name setName: @"EVT_UTIL_THREAD_START"];
	[ev setName: name];	
	[ev setFields: fields];

	[contextid release];
	[name release];
	[fields release];

	[ev autorelease];
	return ev;
}

- (GEvent *) threadRun: (Util::RecordReader::const_iterator) r
{
	GEvent *ev;
	GName *name;
	GFields *fields;

	ev = [[GEvent alloc] init];
	name = [[GName alloc] init]; 
	fields = [[GFields alloc] init];

	[name setName: @"EVT_UTIL_THREAD_RUN"];
	[ev setName: name];	
	[ev setFields: fields];
	
	[name release];
	[fields release];

	[ev autorelease];
	return ev;
}

- (GEvent *) threadTerm: (Util::RecordReader::const_iterator) r
{
	GEvent *ev;
	GName *name;
	GFields *fields;

	ev = [[GEvent alloc] init];
	name = [[GName alloc] init]; 
	fields = [[GFields alloc] init];

	[name setName: @"EVT_UTIL_THREAD_TERM"];
	[ev setName: name];	
	[ev setFields: fields];
	
	[name release];
	[fields release];

	[ev autorelease];
	return ev;
}

- (GEvent *) threadYieldto: (Util::RecordReader::const_iterator) r
{
	GEvent *ev;
	GName *name;
	GFields *fields;

	ev = [[GEvent alloc] init];
	name = [[GName alloc] init]; 
	fields = [[GFields alloc] init];

	[name setName: @"EVT_UTIL_THREAD_YIELDTO"];
	[ev setName: name];	
	[ev setFields: fields];
	
	[name release];
	[fields release];

	[ev autorelease];
	return ev;
}

- (GEvent *) threadResume: (Util::RecordReader::const_iterator) r
{
	GEvent *ev;
	GName *name;
	GFields *fields;

	ev = [[GEvent alloc] init];
	name = [[GName alloc] init]; 
	fields = [[GFields alloc] init];

	[name setName: @"EVT_UTIL_THREAD_RESUME"];
	[ev setName: name];	
	[ev setFields: fields];
	
	[name release];
	[fields release];

	[ev autorelease];
	return ev;
}

- (GEvent *) utilProcessBlocName: (Util::RecordReader::const_iterator) r
{
	GEvent *ev;
	GName *name;
	GFields *fields;
	
	ev = [[GEvent alloc] init];
	name = [[GName alloc] init]; 
	fields = [[GFields alloc] init];

	[name setName: @"EVT_UTIL_PROCESS_BLOC_NAME"];
	[ev setName: name];	
	[ev setFields: fields];

	char *namestr;
	namestr = (char *)r->structure->get_field_data ("name", r->buffer);

	[fields setFieldWithStringKey: @"name" withValue:
                [NSString stringWithFormat: @"%s", namestr]];

	[name release];
	[fields release];

	[ev autorelease];
	return ev;
}


- (GEvent *) utilProcessName: (Util::RecordReader::const_iterator) r
{
	GEvent *ev;
	GName *name;
	GFields *fields;
	
	ev = [[GEvent alloc] init];
	name = [[GName alloc] init]; 
	fields = [[GFields alloc] init];

	[name setName: @"EVT_UTIL_PROCESS_NAME"];
	[ev setName: name];	
	[ev setFields: fields];

	int csize;
	csize = (int)*(char *)r->structure->get_field_data("csize", r->buffer);
	[fields setFieldWithStringKey: @"csize" withValue: [NSString stringWithFormat: @"%d", csize]];

	[name release];
	[fields release];

	[ev autorelease];
	return ev;
}

- (GEvent *) utilProcessInfo: (Util::RecordReader::const_iterator) r
{
	GEvent *ev;
	GName *name;
	GFields *fields;
	
	ev = [[GEvent alloc] init];
	name = [[GName alloc] init]; 
	fields = [[GFields alloc] init];

	[name setName: @"EVT_UTIL_PROCESS_INFO"];
	[ev setName: name];
	[ev setFields: fields];

	double ticks, timeofday;
	ticks = (double)*(double *)r->structure->get_field_data("tick_per_s", r->buffer);
	timeofday = (double)*(double *)r->structure->get_field_data("timeofday", r->buffer);
	[fields setFieldWithStringKey: @"tick_per_s" withValue: [NSString stringWithFormat: @"%f", ticks]];
	[fields setFieldWithStringKey: @"timeofday" withValue: [NSString stringWithFormat: @"%f", timeofday]];

	[name release];
	[fields release];

	[ev autorelease];
	return ev;
}



- (GEvent *) parseKAAPIUtilLevel: (Util::RecordReader::const_iterator) r
	withIdentifier: (GIdentifier *) identifier
	withTimestamp: (GTimestamp *) timestamp
{
	GEvent *ev = nil;
	int event;
	event = (int)*(char *)r->structure->get_field_data("_event",r->buffer);
	switch (event){
		case Event::UTIL_THREAD_NAME:
			ev = [self threadName: r];
			break;
		case Event::UTIL_THREAD_START:
			ev = [self threadStart: r];
			break;
		case Event::UTIL_THREAD_RUN:
			ev = [self threadRun: r];
			break;
		case Event::UTIL_THREAD_TERM:
			ev = [self threadTerm: r];
			break;
		case Event::UTIL_THREAD_YIELDTO:
			ev = [self threadYieldto: r];
			break;
		case Event::UTIL_THREAD_RESUME:
			ev = [self threadResume: r];
			break;
		case Event::UTIL_PROCESS_INFO:
			ev = [self utilProcessInfo: r];
			break;
		case Event::UTIL_PROCESS_NAME:
			ev = [self utilProcessName: r];
			break;
		case Event::UTIL_PROCESS_BLOC_NAME:
			ev = [self utilProcessBlocName: r];
			break;
		default:
			NSLog (@"Warning: Util_Level -> type = %d, not completely translated", event);
			break;
	}
	if (ev != nil){
		[ev setTimestamp: timestamp];
		[ev setIdentifier: identifier];
	}
	return ev; //already autoreleased
}

@end
