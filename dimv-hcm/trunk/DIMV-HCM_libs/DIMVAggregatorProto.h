#ifndef DIMVAGGREGPROTO_DEF
#define DIMVAGGREPROTO_DEF
#import <Foundation/NSString.h>
#import <Foundation/NSDictionary.h>
#import "AuxCopiableDictionary.h"
#import  "CollectorRegister.h"

@protocol DIMVAggregator
#ifndef DIMV_EVALUATE
- (oneway void) receiveData: (in bycopy id)data fromCollector: (in bycopy NSString *)collectorId;
#else
- (oneway void) receiveData: (in bycopy id)data fromCollector: (in bycopy NSString *)collectorId counter: (in bycopy unsigned) counter;
#endif
- (oneway void) subscribe:  (id<DIMVAggregator>)subscriber withCollector: (bycopy NSString *)collectorId;
- (oneway void) unsubscribe: (NSString *)subscriberId withCollector: (NSString *)collectorId;
- (oneway void) registerCollector: (id)collector;
- (oneway void) registerCollectorWithId: (bycopy NSString *)colReg type: (bycopy NSString *)colType location: (bycopy NSString *)colLocation origin: (bycopy NSString *)originId;
- (oneway void) unregisterCollector: (NSString *) collectorId withOrigin: (bycopy NSString *)aggregatorId;
- (oneway void) registerClient: (id) client withId: (NSString *) clientId;
- (oneway void) unregisterClient: (NSString *) clientId;
- (bycopy NSString *) getId;
- (bycopy NSString *) getName;
- (id<DIMVAggregator>) connectWithAggregator: (NSString *)name;
- (BOOL) registerSubordinate: (id<DIMVAggregator>)Subordinate withId: (bycopy NSString *)aggregatorId;

- (bycopy AuxCopiableDictionary *) getRegisteredCollectors;

- (oneway void)subscribeComponent: (bycopy NSString*)componentId toCollector:
(bycopy NSString *)collectorId; 

- (oneway void)unsubscribeComponent: (bycopy NSString*)componentId toCollector:
(bycopy NSString *)collectorId; 

//test to discover how to pass arguments by copy
//- (void) testArg: (bycopy NSMutableString *)testStr withCol: (id) col;
@end
#endif
