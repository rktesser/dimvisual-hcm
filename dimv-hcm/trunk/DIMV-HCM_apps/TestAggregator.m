#import "DIMVAggregator.h" 
#import <Foundation/NSAutoreleasePool.h>


int main(int argc, char **argv)
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	int i;
	
	NSMutableArray *supNames = nil; //names direct superiors in the hierarchy
	DIMVAggregator *aggregator = nil;
	NSString *aggregName;
	NSString *aggregId;

	if(argc < 3){
		printf("Error: Insuficient arguments\n");
		return 1;
	}else if(argc > 3){//an aggregator can have no superiors
		supNames = [NSMutableArray arrayWithCapacity: (argc - 3)];
		for(i=3; i < argc; i++){
			[supNames addObject: [[NSString alloc] initWithCString: argv[i]]];
			NSLog(@"Received superior name \"%@\"\n",[supNames objectAtIndex: (i - 3)]);
		}
	}
	aggregId = [[NSString alloc] initWithCString: argv[1]];
	NSLog(@"Aggregator Id is: %@\n", aggregId);
	aggregName = [[NSString alloc] initWithCString: argv[2]];
	NSLog(@"Aggregator name is: %@\n", aggregName);
	aggregator = [[DIMVAggregator alloc] initWithId: aggregId andName: aggregName andSuperiors: supNames];
	if(aggregator != nil){
		[[NSRunLoop currentRunLoop] run];
	}else{
		NSLog(@"ERROR: Aggregator creation failed! Aborting.\n");
	}
	[pool release];
	return 0;
}
