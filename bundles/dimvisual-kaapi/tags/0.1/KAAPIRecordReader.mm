/*
    DIMVisual-KAAPI, the DIMVisual bundle for KAAPI trace files
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual-KAAPI.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "KAAPIRecordReader.h"

/*
KAAPIRecordReader::KAAPIRecordReader () : 
	Util::RecordReader (NULL) 
{
	count = 0; 
}

KAAPIRecordReader::KAAPIRecordReader (Util::IEventStream* input) : 
	Util::RecordReader (input) 
{
	count = 0; 
}
*/

bool KAAPIRecordReader::setInput (const std::string& filename)
{
	_start = (Util::ka_uint8_t*)-1;
	_size = 0;

	/* use mmap version */
	int fd = open( filename.c_str(), O_RDWR, S_IRUSR|S_IWUSR|S_IRGRP );
	if (fd == -1) return false;
	struct stat st;
	int err = fstat( fd, &st );
	if (err !=0) return false;
	
	_size = st.st_size;
	std::cout << "Size file:" << _size << std::endl;
	void* start = mmap( 0, _size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
	if (start == (void*)-1) {
		close( fd );
		return false;
	}
	_start = (Util::ka_uint8_t*)start;
        ipos = _start;
        iend = _start + _size;
	close( fd );
	return true;
}

/*
void KAAPIRecordReader::notify (Util::Record* r, Util::RecordDesc* rd)
{
        if (r->_level == Event::TR_TRACE_LEVEL && r->_event == Event::TR_BUFFER_ALLOCATE){
                EVT_TR_BUFFER_ALLOCATE_Record *x = (EVT_TR_BUFFER_ALLOCATE_Record *)r;
                std::cout << "#date: " << x->date << " #"<< std::endl;
        }else if (r->_level == Event::TR_UTIL_LEVEL && r->_event == Event::UTIL_THREAD_NAME){
                EVT_UTIL_THREAD_NAME_Record *x = (EVT_UTIL_THREAD_NAME_Record *)r;
                std::cout << "#name: " << x->name << " #"<< std::endl;
        }else if (r->_level == Event::TR_UTIL_LEVEL && r->_event == Event::UTIL_THREAD_NAME){
                EVT_CORE_THREAD_RETRSTEAL_Record *x = (EVT_CORE_THREAD_RETRSTEAL_Record *)r;
                std::cout << "#name: " << x->retval << " #"<< std::endl;
        }
        Util::Record rn(*r);
        Util::RecordDesc rnd(*rd);
        records.push_back(rn);
        recordDescs.push_back(rnd);
}
*/

/* Different version to read only one event per method call */
Util::Record* KAAPIRecordReader::read() throw(Util::RuntimeError)
{
//        if (_ies == 0) {



//		fprintf (stderr, "ipos = %d, iend = %d\n", *ipos, *iend);

                if (ipos < iend) {
                        Util::Record* r = (Util::Record*)ipos;
//                        Util::RecordDesc* rd = Util::Init::record_manager.resolve( *r );

			/* so, what's inside? */



                       ipos += r->_size;
//			fprintf (stderr, "inc %d", (int)*ipos);
			return r;
//                        notify(r, rd );
//			return 1;
                }
		return NULL;

/*
        }else{
                Util::Record* buff = 0;
                size_t sz_buff = 0;
                if (_ies->good()){
                        Util::Record r;
                        _ies->read( &r, sizeof(r) );
                        if (!_ies->fail()){
                                Util::RecordDesc* rd = Util::Init::record_manager.resolve(r);
                                if (sz_buff < r._size) {
                                        if (buff !=0) free(buff);
                                        sz_buff = r._size*2;
                                        buff = (Util::Record*)malloc(sz_buff);
                                }
                                *buff = r;
                                if (r._size - sizeof(Util::Record) > 0) {
                                        _ies->read( buff+1, r._size-sizeof(Util::Record) );
                                }
                                notify( buff, rd );
                		if (buff !=0) free(buff);
				return 1;
                        }
                }
        }
	return 0;
*/
}

/*	
int KAAPIRecordReader::size ()
{
	        return records.size();
}
	
Util::Record KAAPIRecordReader::getRecord ()
{
	Util::Record r = records.front();
	records.pop_front();
	std::cout << "\t size is " << records.size() << std::endl;
	return r;
}
	
Util::RecordDesc KAAPIRecordReader::getRecordDesc ()
{
	Util::RecordDesc rd = recordDescs.front();
	recordDescs.pop_front();
	std::cout << "\t size is " << records.size() << std::endl;
	return rd;
}
*/
