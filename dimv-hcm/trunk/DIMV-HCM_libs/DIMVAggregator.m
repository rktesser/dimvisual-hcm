#import "DIMVAggregator.h"

@implementation DIMVAggregator

- (DIMVAggregator *) initWithId: (NSString *)aggregId andName: (NSString *)aggregName andSuperiors: (NSMutableArray *)supNames
{
	id superiorAux;
	int i;
	self = [super init];
	aggregatorId = aggregId;
	RETAIN(aggregId);
	name = aggregName;
	RETAIN(aggregName);
	subordinates = [[NSMutableDictionary alloc] init];
	collectors = [[NSMutableDictionary alloc] init];
	registeredCollectors = [[AuxCopiableDictionary alloc]
	  initWithCapacity: 13];
	subscribedCollectors = [[NSMutableDictionary alloc] init];
	clients = [NSMutableDictionary dictionary];
	RETAIN(clients);
	NSLog(@"Creating new aggregator with Id \"%@\" and name \"%@\"\n",
	  aggregatorId, name);
	if(![self registerWithNameServer]){
		return nil;
	}
	if(supNames != nil){
		superiors = [NSMutableDictionary dictionary];
		RETAIN(superiors);
		for(i = 0;i < [supNames count]; i++){
			superiorAux = (id<DIMVAggregator>) [self 
			  connectWithAggregator: [supNames objectAtIndex: i]];
			[superiorAux registerSubordinate: self withId: 
			  aggregatorId];
			[superiors setObject: superiorAux forKey: 
			  [superiorAux getId]];
			[self forwardRegisteredCollectorsToAggregator:
			  superiorAux];
		}
	}
	#ifdef DIMV_EVALUATE
	NSFileManager * aFileManager;
	NSString * fileName;
	aFileManager = [NSFileManager defaultManager];
	RETAIN(aFileManager);
	fileName = [name stringByAppendingString: @"-timeReceiveData.output"];
	RETAIN(fileName);
	if([aFileManager createFileAtPath: fileName contents: nil 
	   attributes:nil] == NO){
		NSLog(@"Couldn't create the file %@\n",fileName);
		exit(1);
	}
	timeReceiveDataFH = [NSFileHandle 
	   fileHandleForWritingAtPath: fileName];
	RETAIN(timeReceiveDataFH);
	RELEASE(fileName);
/*	fileName = [name stringByAppendingString: @"-durationSend.output"];
	RETAIN(fileName);
	if([aFileManager createFileAtPath: fileName contents: nil 
	   attributes:nil] == NO){
		NSLog(@"Couldn't create the file %@\n",fileName);
		exit(1);
	}
	durationSendFH = [NSFileHandle 
	   fileHandleForWritingAtPath: fileName];
	RETAIN(durationSendFH);
	RELEASE(fileName);*/
	RELEASE(aFileManager);
	#endif
	NSLog(@"An aggregator with Id \"%@\" and name \"%@\" has been created\n",aggregatorId, name);
	return self;
}

- (void) dealloc
{
	RELEASE(aggregatorId);
	RELEASE(name);
	RELEASE(registeredCollectors);
	RELEASE(subscribedCollectors);
	RELEASE(superiors);
	TEST_RELEASE(subordinates);
	RELEASE(collectors);
	RELEASE(clients);
	#ifdef DIMV_EVALUATE
	[timeReceiveDataFH closeFile];
	RELEASE(timeReceiveDataFH);
/*	[durationSendFH closeFile];
	RELEASE(durationSendFH);*/
	#endif
	[super dealloc];
}

- (BOOL) registerWithNameServer
{
	NSLog(@"Trying to register aggregator with the name server.\n");
	NSPort *port = [NSSocketPort port];
	NSConnection *conn = [NSConnection connectionWithReceivePort: port sendPort: port];
	[conn setRootObject: self];
	/*This has been added to test if performance improves.*/
	if(![conn registerName: name withNameServer: [NSSocketPortNameServer sharedInstance]]){
		NSLog(@"Failed to register with name server.\n");
		return (NO);
	}
	NSLog(@"The aggregator has been registered with name server.\n");
	return (YES);
}

#ifdef DIMV_EVALUATE
- (void) receiveData: (id)data fromCollector: (NSString *)collectorId
   counter: (unsigned) counter
#else
- (void) receiveData: (id)data fromCollector: (NSString *)collectorId
#endif
{
	#ifdef DIMV_EVALUATE
	struct timeval to,tf, tb_rc, ta_rc;
	unsigned long long delta_t, delta_t_rc, total_rc, to_usec;
	gettimeofday(&to, NULL);
	#endif
	NSEnumerator *subscrEnum;
	id destination;
	RETAIN(data);
	RETAIN(collectorId);
//	NSLog(@"Received data from the collector with id:%@\n", collectorId);
	subscrEnum = [[[registeredCollectors objectForKey: collectorId]
	  getSubscribers] objectEnumerator];
	#ifdef DIMV_EVALUATE
	total_rc=0;
	#endif
	while((destination = [subscrEnum nextObject])!=nil){
		RETAIN(destination);
		#ifdef DIMV_EVALUATE
		gettimeofday(&tb_rc, NULL);
		gettimeofday(&ta_rc, NULL);
		[destination receiveData: data fromCollector: collectorId 
		  counter: counter];
		delta_t_rc = (ta_rc.tv_usec + ta_rc.tv_sec * 1000000) -
		  (tb_rc.tv_usec + tb_rc.tv_sec * 1000000);
		total_rc += delta_t_rc;
		#else
		[destination receiveData: data fromCollector: collectorId];
		#endif
		RELEASE(destination);
	}
	#ifdef DIMV_EVALUATE
	gettimeofday(&tf, NULL);
	to_usec = (to.tv_usec + to.tv_sec * 1000000);
	delta_t = (tf.tv_usec + tf.tv_sec * 1000000) - to_usec;
	[timeReceiveDataFH writeData: [[NSString stringWithFormat: 
	  @"%@ %010u %llu %llu %llu\n", collectorId, counter, to_usec, 
	  delta_t, total_rc] dataUsingEncoding: NSUTF8StringEncoding]];
	#endif
	RELEASE(data);
	RELEASE(collectorId);
	return;
}

- (void) subscribe: (DIMVAggregator *)subscriber withCollector: (NSString *)collectorId;
{
	return;
}

- (void) unsubscribe: (NSString *) subscriberId withCollector: (NSString *) collectorId
{
	return;
}

- (void)subscribeComponent: (NSString*)componentId toCollector: (NSString *)collectorId
{
	CollectorRegister *colReg;
	id component;
	NSString *originId;
	RETAIN(componentId);
	RETAIN(collectorId);
	NSLog(@"Subscription request received.");
	colReg = [registeredCollectors objectForKey: collectorId];
	if(colReg == nil){
		NSLog(@"ERROR: Invalid subscription request. There's no collector with id \"%@\" registered in this aggregator.", collectorId);
	}else{
		component = (id<DIMVAggregator>)[superiors objectForKey: componentId];
		if(component == nil){
			component = (id<DIMVClient>)[clients objectForKey: componentId];
			if(component != nil){
				NSLog(@"The requester is a client.");
			}
		}else{
			NSLog(@"The requester is an aggregator.");
		}
		if(component == nil){
			NSLog(@"ERROR: There was a subscription request from a component that isn't a superior of this aggregator.");
		}else{
			NSLog(@"Updating the collector refister.");
			[colReg addSubscriber: component withId: componentId];
			NSLog(@"The component with Id \"%@\" has been subscribed to the collector with Id \"%@\".", componentId, collectorId);
		}
	}
		
	/*If we already are subscribed to the collector, then nothing else needs
	 * to be done.*/
	if([subscribedCollectors objectForKey: collectorId] != nil){
		NSLog(@"This aggregator is already subscribed to %@",
		 collectorId);
		return;
	}
	NSLog(@"This aggregator needs to subscribe itself to %@.", 
	  collectorId);	
	/*If this aggregator is not already subscribed to the collectoe, then
	 * we forward the subscription request.*/
	originId = [colReg getOriginId];
	component = (id<DIMVAggregator>)[subordinates objectForKey: originId];
	if(component == nil){//If it's not an registered aggreggator,...
		NSLog(@"Subscription: The origin is a collector.");
		component = (id<DIMVCollector>)[collectors objectForKey: originId];//...then it must be an collector.
	}else{
		NSLog(@"Subscription: The origin is an aggregator.");
	}
	if(component == nil){//Something went very wrong!
		NSLog(@"ERROR: Problem with the collector register. The registered origin (%@) is not a subordinate of this aggregator.", originId);
	}else{//OK! Let's forward the request.
		NSLog(@"Forwarding subscription to origin (%@).", originId);
		[subscribedCollectors setObject: component
		  forKey: collectorId]; 
		[component subscribeComponent: aggregatorId
		  toCollector: collectorId]; 
	}
	RELEASE(componentId);
	AUTORELEASE(collectorId);
	return;
}

- (void) unsubscribeComponent: (NSString*)componentId toCollector: (NSString *)collectorId
{
	CollectorRegister *colReg;
	id component;
	NSString *originId;
	RETAIN(componentId);
	RETAIN(collectorId);
	colReg = [registeredCollectors objectForKey: collectorId];
	if(colReg == nil){
		NSLog(@"ERROR: Invalid unsubscription request. There's no collector with id \"%@\" registered in this aggregator.", collectorId);
	}else{
		component = (id<DIMVAggregator>)[superiors objectForKey: componentId];
		if(component == nil){
			component = (id<DIMVClient>)[clients objectForKey: componentId];
		}
		if(component == nil){
			NSLog(@"ERROR: There was an unsubscription request from a component that isn't a superior of this aggregator.");
		}else{
			[colReg removeSubscriber: componentId];
			NSLog(@"The component with Id \"%@\" has been unsubscribed to the collector with Id \"%@\".", componentId, collectorId);
		}
	}

	/*If there are other registered subscribers to the collector,then
	 * nothing else needs to be done.*/
	if([[colReg getSubscribers] count] != 0){
		return;
	}

	/*If there are subscribers left, then we forward the unsubscription
	 * request.*/

	originId = [colReg getOriginId];
	component = (id<DIMVAggregator>) [subordinates objectForKey: originId];
	if(component == nil){//If it's not a registered aggreggator,...
		component = (id<DIMVCollector>)[collectors
		  objectForKey: originId];//...then it must be a collector.
	}
	if(component == nil){//Something went very wrong!
		NSLog(@"ERROR: Problem with the collector register. The registered origin is not a subordinate of this aggregator.");
	}else{//OK! Let's forward the request.
		[subscribedCollectors removeObjectForKey: collectorId];
		[component unsubscribeComponent: aggregatorId toCollector: collectorId]; 
	}
	RELEASE(componentId);
	AUTORELEASE(collectorId);
	return;
}

- (void) forwardCollectorRegisterWithId: (NSString *)colId type: (NSString *)colType location: (NSString *)colLocation
{
	NSEnumerator *compEnum;
	id component;
	
	RETAIN(colId);
	RETAIN(colType);
	RETAIN(colLocation);

	compEnum = [clients objectEnumerator];
	RETAIN(compEnum);
	while((component = (id<DIMVClient>) [compEnum nextObject])!= nil){
		[component registerCollectorWithId: colId type: colType location: colLocation origin: aggregatorId];
	}
	RELEASE(compEnum);
	compEnum = [superiors objectEnumerator];
	RETAIN(compEnum);
	while((component = (id<DIMVAggregator>) [compEnum nextObject])!= nil){
		[component registerCollectorWithId: colId type: colType location: colLocation origin: aggregatorId];
	}
	RELEASE(compEnum);
	AUTORELEASE(colId);
	AUTORELEASE(colType);
	AUTORELEASE(colLocation);
}

- (oneway void) registerCollectorWithId: (bycopy NSString *)colId type: (NSString *)colType location: (NSString *)colLocation origin: (bycopy NSString *)originId
{

	RETAIN(colId);
	RETAIN(colType);
	RETAIN(originId);
	RETAIN(colLocation);
	CollectorRegister * colReg;
	/*TODO: get the location of the collector*/

	/*Create the register*/
	colReg = [[CollectorRegister alloc] initWithCollectorId: colId andType: colType andLocation: colLocation];
	[colReg setOriginId: originId];
	RELEASE(originId);
	/*Register the collector using it's Id as key.*/
	[registeredCollectors setObject: colReg forKey: colId];
	NSLog(@"Registered a collector with Id = \"%@\" and type = \"%@\"\n", [colReg getCollectorId], [colReg getCollectorType]);
	RELEASE(colReg);
	/*forward the register to the superirors*/
	[self forwardCollectorRegisterWithId: colId type: colType location: colLocation];
	AUTORELEASE(colId);
	RELEASE(colType);
	RELEASE(colLocation);
	return;
}

- (void)registerCollector: (id<DIMVCollector>)collector
{
	CollectorRegister * colReg;
	NSString *colId = [collector getId];
	RETAIN(colId);
	NSString *colType = [collector getType];
	RETAIN(colType);
	NSString *colLocation = [collector getLocation];
	RETAIN(colLocation);
	[collectors setObject: collector forKey: colId];
	/*Create the register*/
	colReg = [[CollectorRegister alloc] initWithCollectorId: colId andType: colType andLocation: colLocation];
	
	/*Register the collector using it's Id as key.*/
	[registeredCollectors setObject: colReg forKey: colId];
	RELEASE(colReg);
	[colReg setOriginId: colId];
	NSLog(@"Registered a collector with Id = \"%@\" and type = \"%@\"\n", [colReg getCollectorId], [colReg getCollectorType]);
	/*forward the register to the superirors*/
	[self forwardCollectorRegisterWithId: colId type: colType location: colLocation];
	AUTORELEASE(colId);
	AUTORELEASE(colType);
	AUTORELEASE(colLocation);
	return;
}

/*OLD VERSION
 *
 * This method forwards a colllectorRegister to the direct superior components
 *of the hierarchy.
- (void) forwardCollectorRegister: (CollectorRegister *)colReg
{
	NSEnumerator *compEnum;
	id component;
	
	RETAIN(colReg);
	compEnum = [clients objectEnumerator];
	RETAIN(compEnum);
	while((component = (id<DIMVClient>) [compEnum nextObject])!= nil){
		[component registerCollectorWithId: [colReg getCollectorId] colReg andOriginId: aggregatorId];
	}
	RELEASE(compEnum);
	compEnum = [superiors objectEnumerator];
	RETAIN(compEnum);
	while((component = (id<DIMVClient>) [compEnum nextObject])!= nil){
		[component registerCollectorWithRegister: colReg andOriginId: aggregatorId];
	}
	RELEASE(compEnum);
	RELEASE(colReg);
}

- (void) registerCollectorWithRegister: (CollectorRegister *)colReg andOriginId: (NSString *)originId
{
	RETAIN(colReg);
	RETAIN(originId);
	[colReg setOriginId: originId];
	RELEASE(originId);
	[registeredCollectors setObject: colReg 
	  forKey: [colReg getCollectorId]];
	[self forwardCollectorRegister: colReg];	
	RELEASE(colReg);
}
*/

- (void) unregisterCollector: (NSString *)collectorId withOrigin: (NSString *)AggregatorId
{
	return;
}


- (void) registerClient: (id) client withId: (NSString *) clientId
{
	if(client != nil){
		NSLog(@"Registering client with Id: %@\n", clientId);
		[clients setObject: (id<DIMVClient>) client forKey: clientId];
	}
	return;
}


- (void) unregisterClient: (NSString *) clientId
{
	[clients removeObjectForKey: clientId];
}

- (NSString *) getId
{
	return aggregatorId;
}

- (BOOL)registerSubordinate: (id)subordinate withId: (NSString *)subordinateId
{
	id sub = (id<DIMVAggregator>) subordinate;
	NSLog(@"Registering subordinate with Id=%@\n",subordinateId);
	[subordinates setObject: sub forKey: subordinateId];

	return YES;
}

- (id<DIMVAggregator>) connectWithAggregator: (NSString *)aggregName
{
	id remoteAggregator;
	NSLog(@"Trying to connect aggregator \"%@\" with aggregator \"%@\"\n", name, aggregName);
	do{
	remoteAggregator = (id<DIMVAggregator>) [NSConnection rootProxyForConnectionWithRegisteredName: aggregName host: @"*" usingNameServer: [NSSocketPortNameServer sharedInstance]];
		if(remoteAggregator == nil){
			NSLog(@"Error: Aggregator \"%@\" couldn\'t connect with aggregator \"%@\"\n", name, aggregName);
			[NSThread sleepForTimeInterval: DIMV_CONN_RETRY_WAIT];
		}
	}while(remoteAggregator == nil);
	RETAIN(remoteAggregator);
	NSLog(@"Aggregator \"%@\" connected with aggregator \"%@\"\n", name, aggregName);
	[[NSNotificationCenter defaultCenter] addObserver: [Observer new] selector: @selector(connectionDied:) name: NSConnectionDidDieNotification object: [(NSDistantObject*) remoteAggregator connectionForProxy]];
	
	AUTORELEASE(remoteAggregator);
	return remoteAggregator;
}


- (NSString *) getName
{
	return name;
}

- (AuxCopiableDictionary *) getRegisteredCollectors
{
	return registeredCollectors;
}

- (void)forwardRegisteredCollectorsToAggregator: (id<DIMVAggregator>)aComponent
{
	CollectorRegister *colReg;
	NSString *colId, *colLocation, *colType;
	NSEnumerator *compEnum = [registeredCollectors objectEnumerator]; 
	RETAIN(compEnum);
	while((colReg = [compEnum nextObject])){
		colId = [colReg getCollectorId];
		colLocation = [colReg getCollectorType];
		colType = [colReg getCollectorType];
		[aComponent registerCollectorWithId: colId type: colType
		  location: colLocation origin:aggregatorId];
	}
	RELEASE(compEnum);
	return;
}

@end
