/*
   Copyright (c) 2005 Lucas Mello Schnorr <schnorr@gmail.com>
   
   This file is part of DIMVisual.
   
   DIMVisual is free software; you can redistribute it and/or modify it under
   the terms of the GNU Lesser General Public License as published by the
   Free Software Foundation; either version 2 of the License, or (at your
   option) any later version.
   
   DIMVisual is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
   for more details.
   
   You should have received a copy of the GNU Lesser General Public License
   along with DIMVisual; if not, write to the Free Software Foundation, Inc.,
   59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/
#include "MPIRASTROEvent.h"
#include "MPIRASTROFileReader.h"

@implementation MPIRASTROEvent
- initWithEvent: (rst_event_t *) event withProvider: (id) provider
{
	int i;
	char temp[100];
	double xx;

	[super init];
	adouble = [[NSMutableArray alloc] init];
	afloat = [[NSMutableArray alloc] init];
	aint8 = [[NSMutableArray alloc] init];
	aint64 = [[NSMutableArray alloc] init];
	aint32 = [[NSMutableArray alloc] init];
	aint16 = [[NSMutableArray alloc] init];
	astring = [[NSMutableArray alloc] init];

	for (i = 0; i < event->ct.n_double; i++)
		[self addDoubleValue: event->v_double[i]];
	for (i = 0; i < event->ct.n_float; i++)
		[self addFloatValue: event->v_float[i]];
	for (i = 0; i < event->ct.n_uint64; i++)
		[self addInt64Value: event->v_uint64[i]];
	for (i = 0; i < event->ct.n_uint32; i++)
		[self addInt32Value: event->v_uint32[i]];
	for (i = 0; i < event->ct.n_uint16; i++)
		[self addInt16Value: event->v_uint16[i]];
	for (i = 0; i < event->ct.n_uint8; i++)
		[self addInt8Value: event->v_uint8[i]];
	for (i = 0; i < event->ct.n_string; i++)
		[self addStringValue: event->v_string[i]];

	xx = (double)event->timestamp/1000000;
	sprintf (temp, "%lld", event->timestamp);
	time = [[NSString alloc] initWithCString: temp];
	sprintf (temp, "%d", event->type);
	type = [[NSString alloc] initWithCString: temp];
	machine = [provider machineForNode: event->id1];
	[machine retain];
	thread = [[NSString alloc] initWithFormat: @"%d", event->id2];
	node = [[NSString alloc] initWithFormat: @"%d", event->id1];
//	NSLog (@"%@(%@)", machine,thread);
	return self;
}


- (void) addDoubleValue: (double) value
{
	NSString *str;
	char temp[100];
	
	sprintf (temp, "%f", value);
	str = [[NSString alloc] initWithCString: temp];
	[adouble insertObject: str atIndex: [adouble count]];
	[str release];
}

- (void) addFloatValue: (float) value
{
	NSString *str;
	char temp[100];
	
	sprintf (temp, "%f", value);
	str = [[NSString alloc] initWithCString: temp];
	[afloat insertObject: str atIndex: [afloat count]];
	[str release];
}

- (void) addInt8Value: (u_int8_t) value
{
	NSString *str;
	char temp[100];
	
	sprintf (temp, "%c", value);
	str = [[NSString alloc] initWithCString: temp];
	[aint8 insertObject: str atIndex: [aint8 count]];
	[str release];
}

- (void) addInt64Value: (u_int64_t) value
{
	NSString *str;
	char temp[100];
	
	sprintf (temp, "%lld", value);
	str = [[NSString alloc] initWithCString: temp];
	[aint64 insertObject: str atIndex: [aint64 count]];
	[str release];
}

- (void) addInt32Value: (u_int32_t) value
{
	NSString *str;
	char temp[100];
	
	sprintf (temp, "%d", value);
	str = [[NSString alloc] initWithCString: temp];
	[aint32 insertObject: str atIndex: [aint32 count]];
	[str release];
}

- (void) addInt16Value: (u_int16_t) value
{
	NSString *str;
	char temp[100];
	
	sprintf (temp, "%d", value);
	str = [[NSString alloc] initWithCString: temp];
	[aint16 insertObject: str atIndex: [aint16 count]];
	[str release];
}

- (void) addStringValue: (char *) value
{
	NSString *str;
	char temp[100];
	
	sprintf (temp, "%s", value);
	str = [[NSString alloc] initWithCString: temp];
	[astring insertObject: str atIndex: [astring count]];
	[str release];
}

- (NSString *)doubleAtIndex: (int) i
{
	if (i > [adouble count] || [adouble count] == 0){
		NSLog (@"adouble=%@ count=%d i=%d -> i > count",
		adouble, [adouble count], i);
		return nil;
	}
	return [adouble objectAtIndex: i];
}

- (NSString *)floatAtIndex: (int) i
{
	if (i > [afloat count] || [afloat count] == 0){
		NSLog (@"afloat=%@ count=%d i=%d -> i > count",
		afloat, [afloat count], i);
		return nil;
	}
	return [afloat objectAtIndex: i];
}

- (NSString *)int8AtIndex: (int) i
{
	if (i > [aint8 count] || [aint8 count] == 0){
		NSLog (@"aint8=%@ count=%d i=%d -> i > count",
		aint8, [aint8 count], i);
		return nil;
	}
	return [aint8 objectAtIndex: i];
}

- (NSString *)int16AtIndex: (int) i
{
	if (i > [aint16 count] || [aint16 count] == 0){
		NSLog (@"aint16=%@ count=%d i=%d -> i > count",
		aint16, [aint16 count], i);
		return nil;
	}
	return [aint16 objectAtIndex: i];
}

- (NSString *)int32AtIndex: (int) i
{
	if (i > [aint32 count] || [aint32 count] == 0){
		NSLog (@"aint32=%@ count=%d i=%d -> i > count , type=%@",
		aint32, [aint32 count], i, type);
		return nil;
	}
	return [aint32 objectAtIndex: i];
}

- (NSString *)int64AtIndex: (int) i
{
	if (i > [aint64 count] || [aint64 count] == 0){
		NSLog (@"aint64=%@ count=%d i=%d -> i > count",
		aint64, [aint64 count], i);
		return nil;
	}
	return [aint64 objectAtIndex: i];
}

- (NSString *)stringAtIndex: (int) i
{
	if (i > [astring count] || [astring count] == 0){
		NSLog (@"astring=%@ count=%d i=%d -> i > count",
		astring, [astring count], i);
		return nil;
	}
	return [astring objectAtIndex: i];
}

- (NSString *) time
{
	return time;
}

- (NSString *) type
{
	return type;
}

- (NSString *) machine
{
	return machine;
}

- (NSString *) thread
{
	return thread;
}

- (NSString *) node
{
	return node;
}

- (void) dealloc
{
	[type release];
	[time release];
	[machine release];
	[thread release];
	[node release];

	[adouble release];
	[afloat release];
	[aint8 release];
	[aint16 release];
	[aint32 release];
	[aint64 release];
	[astring release];

	[super dealloc];
}

@end
