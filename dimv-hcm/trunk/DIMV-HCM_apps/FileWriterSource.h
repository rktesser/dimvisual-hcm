#import <Foundation/NSAutoreleasePool.h>
#import <Foundation/NSArray.h>
#import <Foundation/NSString.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSFileHandle.h>
#import <Foundation/NSFileManager.h>
#import <Foundation/NSLock.h>
#import <Foundation/NSThread.h>
#import "DIMVClientProto.h"

@interface FileWriterSource: NSObject <HCMDataProcessor>
{
	unsigned count;
	BOOL finish;
	NSMutableArray *dataBuffer;
	NSThread *writerThread;
	NSConditionLock *dataLock;
	NSDate *startDate;
	NSString *type;
	NSFileHandle *metaFile;
	NSFileHandle *dataFile;
}
- (FileWriterSource *)initWithType: (NSString *)type;
- (void)finish;
@end
