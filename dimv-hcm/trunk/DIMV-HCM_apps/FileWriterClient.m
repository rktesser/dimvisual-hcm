#import "DIMVClient.h" 
#import "FileWriterSource.h"
#import <Foundation/NSAutoreleasePool.h>
#import <unistd.h>
#import <signal.h>

@interface  testSubscriptionHandler: NSObject <HCMSubscriptionHandler>
{
	DIMVClient *client;
	NSMutableDictionary *registeredCollectors;
}
- (testSubscriptionHandler *)initWithClient: (DIMVClient *)aClient;
- (NSMutableDictionary *)getRegisteredCollectors;
@end

@implementation testSubscriptionHandler

- (testSubscriptionHandler *)initWithClient: (DIMVClient *)aClient
{
	self = [super init];
	client = [aClient retain];
	registeredCollectors = [[NSMutableDictionary alloc] init];
	return self;

}

- (BOOL)registerCollectorWithId: (NSString *) collectorId
  type: (NSString *) type location: (NSString *)location
{
	BOOL ret;
	NSDictionary *colReg = [NSDictionary dictionaryWithObjectsAndKeys:
	collectorId, @"collectorId", type, @"collectorType",
	location, @"collectorLocation", nil];
	[registeredCollectors setObject: colReg forKey: collectorId];
	NSLog(@"Subscribing to %@", collectorId);
	ret = [client subscribeToCollector: collectorId];
	if(ret == NO){
		NSLog(@"WARNING: unsucessful subscription.");
	}
	return YES;
}

- (BOOL)unregisterCollectorWithId: (NSString *) collectorId
{
	NSLog(@"Unsubscribing to %@", collectorId);
	[client unsubscribeToCollector: collectorId];
	[registeredCollectors removeObjectForKey: collectorId];
	return YES;
}

- (NSMutableDictionary *)getRegisteredCollectors
{
	return [NSDictionary dictionaryWithDictionary: registeredCollectors];
}

- (void)setClient: (DIMVClient *)aClient
{
	return;
}
@end

BOOL finishFileWriterClient;

void sigterm()
{
	signal(SIGTERM, sigterm);
	finishFileWriterClient = YES;
	printf("sigterm\n");
}

void sigint()
{
	signal(SIGINT, sigint);
	finishFileWriterClient = YES;
	printf("sigint\n");
}

int main(int argc, char **argv)
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	signal(SIGINT, sigint);
	signal(SIGTERM, sigterm);
	int i;
	finishFileWriterClient = NO;
	NSMutableArray *aggregNames = nil; //names direct superiors in the hierarchy
	DIMVClient *client = nil;
	NSString *clientName;
	NSString *clientId;
	testSubscriptionHandler *subscrHandler;
	NSLog(@"Beginning the initalization of the client.\n");
	NSLog(@"Initializing the File Writer Data Source.");
	id dSource= [[FileWriterSource alloc] initWithType: @"ganglia"];
	
	if(argc < 4){
		printf("Error: Insuficient arguments\n");
		return 1;
	}
	aggregNames = [[NSMutableArray arrayWithCapacity: (argc - 3)] retain];
	for(i=3; i < argc; i++){
		[aggregNames addObject: [[NSString alloc] initWithCString: argv[i]]];
	}
	clientId = [[NSString alloc] initWithCString: argv[1]];
	clientName = [[NSString alloc] initWithCString: argv[2]];
	NSLog(@"Initializing the DIMVClient.");
	client = [[DIMVClient alloc] initWithId: clientId andAggregators: aggregNames];
	if(client != nil){
		NSLog(@"Registering the data source with the DIMVClient.");
		[client addDataSource: dSource withType: @"ganglia"];
		NSLog(@"Initializing the subscription handler.");
		subscrHandler = [[testSubscriptionHandler alloc] initWithClient: client];
		NSLog(@"Registering the subscription handler with the DIMVClient.");
		[client setSubscriptionHandler: subscrHandler];
		NSLog(@"Initialization complete.");
		while(!finishFileWriterClient){
			[NSThread sleepForTimeInterval: 2];
		}
		NSLog(@"Finishing program");
		[client release];
		[dSource finish];
		[clientId release];
		[clientName release];
		[aggregNames release];
		[dSource release];
	}else{
		NSLog(@"ERROR: Client creation failed! Aborting.\n");
	}
	[pool release];
	return 0;
}
