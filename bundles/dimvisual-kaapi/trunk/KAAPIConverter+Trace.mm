/*
    DIMVisual-KAAPI, the DIMVisual bundle for KAAPI trace files
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual-KAAPI.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "KAAPIConverter.h"
#include <kaapi> /* for the trace and level stuff (identifier) */

@implementation KAAPIConverter (Trace)
- (id) convertEventTrace: (GEvent *) event
{
	NSMutableArray *ret = [[NSMutableArray alloc] init];
	

	/* for the creation of the container, provider only creates if
	   it does not exist */
/*
	[ret addObjectsFromArray: [provider createContainerForEntity:
		[NSArray arrayWithObjects: @"gid", @"KAAPITrace", nil]
		ids:
		[NSArray arrayWithObjects: gid, @"KAAPITraceContainer", nil]
		time: time]];
*/

	/* this is the container being updated */

/*
	NSString *container;
	container = [provider identifierForEntity:
		[NSArray arrayWithObjects: @"gid", @"KAAPITrace", nil]
		ids:
		[NSArray arrayWithObjects: gid, @"KAAPITraceContainer", nil]];
*/

	/* converting events */
	switch (atoi ([eventid cString])){
/*
		case Event::TR_BUFFER_ALLOCATE:
			{
				PajeNewEvent *ev = [[PajeNewEvent alloc]
initWithTime: time];
				[ev setContainer: container];
				[ev setEntityType: [provider aliasToName: @"KAAPITraceEvent"]];
				[ev setValue: [provider aliasToName: @"TR_BUFFER_ALLOCATE"]];
				[ret addObject: ev];
				[ev release];
			}
			break;
		case Event::TR_BUFFER_FLUSH:
			{
				PajeNewEvent *ev = [[PajeNewEvent alloc]
initWithTime: time];
				[ev setContainer: container];
				[ev setEntityType: [provider aliasToName: @"KAAPITraceEvent"]];
				[ev setValue: [provider aliasToName: @"TR_BUFFER_FLUSH"]];
				[ret addObject: ev];
				[ev release];
			}
			break;
*/
		default: break;
	}
	[ret autorelease];
	return ret;
}
@end
