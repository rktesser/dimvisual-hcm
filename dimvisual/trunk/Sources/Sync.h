/*
    DIMVisual, the Data Integration Model for Visualization of trace files.
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __Sync_H
#define __Sync_H
#include <Foundation/Foundation.h>

@interface Sync : NSObject
{
	NSString *myHost;
	double myA;
	long long myLoc0, myRef0;
	
}
+ (NSArray *) allMachinesOfFileNamed: (NSString *)filename;
- (id) initWithFileName: (NSString *) syncFile toHostname: (NSString *) host;
- (NSString *) correctTime: (NSString *) timeToCorrect;
- (NSString *) host;
@end

#endif
