/*
   Copyright (c) 2005 Lucas Mello Schnorr <schnorr@gmail.com>
   
   This file is part of DIMVisual.
   
   DIMVisual is free software; you can redistribute it and/or modify it under
   the terms of the GNU Lesser General Public License as published by the
   Free Software Foundation; either version 2 of the License, or (at your
   option) any later version.
   
   DIMVisual is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
   for more details.
   
   You should have received a copy of the GNU Lesser General Public License
   along with DIMVisual; if not, write to the Free Software Foundation, Inc.,
   59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/
#include "MPIRASTROConverter.h"

static AllToOne *reduce;
static int size = -1;

@implementation MPIRASTROConverter (Reduce)
- (id) convertEventReduce: (MPIRASTROEvent *) event
{
	NSMutableArray *ret = nil;
	NSString *container;
//	int type;
	int i;

	if (reduce == nil){
		reduce = [[AllToOne alloc] init];
	}
	if (allreduce == nil){
		allreduce = [[NSMutableArray alloc] init];
	}
	
	ret = [[NSMutableArray alloc] init];

	container = [provider identifierForEntity: 
		[NSArray arrayWithObjects: @"Machine", @"Thread", nil]
				ids:
		[NSArray arrayWithObjects: [event machine], [event thread], nil]];

	if (container == nil){
		[ret release];
		return nil;
	}
	
	int etype = atoi ([[event type] cString]);

/*******************************************************************************
          MPI_ALLREDUCE
*******************************************************************************/
	if (etype == MPI_ALLREDUCE_IN){
		if (size == -1){
			size = atoi([[event int32AtIndex: 2] cString]);
			for (i = 0; i < size; i++){
				AllToOne *reduce = [[AllToOne alloc] init];
				[allreduce addObject: reduce];
				[reduce release];
			}
		}
		for (i = 0; i < size; i++){
			[[allreduce objectAtIndex:i] memberEntering: container
				andEventNode: [event node]
				andEventTime: [event time]
				andRootNode: [NSString stringWithFormat:@"%d",i]
				andCommSize: [event int32AtIndex: 2]];
		}
		[ret addObject: [self addPajeSetState: @"MPI_ALLREDUCE" time: [event time] container: container]];


	}else if (etype == MPI_ALLREDUCE_OUT){
		for (i = 0; i < size; i++){
			[ret addObjectsFromArray:
				[[allreduce objectAtIndex:i]
					memberExiting: container
					andEventTime: [event time]
					andOperationName: @"allreduce"
					andProvider: provider]];
		}
		[ret addObject: [self addPajeSetState: @"running" time: [event time] container: container]];

		
/*******************************************************************************
          MPI_REDUCE_SCATTER
*******************************************************************************/
	}else if (etype == MPI_REDUCE_SCATTER_IN){
		[ret addObject: [self addPajeSetState: @"MPI_REDUCE_SCATTER" time: [event time] container: container]];
	}else if ( etype == MPI_REDUCE_SCATTER_OUT){
		[ret addObject: [self addPajeSetState: @"running" time: [event time] container: container]];

/*******************************************************************************
          MPI_REDUCE
*******************************************************************************/
	}else if (etype == MPI_REDUCE_IN){
		[reduce memberEntering: container
			andEventNode: [event node]
			andEventTime: [event time]
			andRootNode: [event int32AtIndex: 1]
			andCommSize: [event int32AtIndex: 2]];
		[ret addObject: [self addPajeSetState: @"MPI_REDUCE" time: [event time] container: container]];


	}else if (etype == MPI_REDUCE_OUT){
		[ret addObjectsFromArray: [reduce memberExiting: container
			andEventTime: [event time]
			andOperationName: @"reduce"
			andProvider: provider]];
		[ret addObject: [self addPajeSetState: @"running" time: [event time] container: container]];
	}


	[ret autorelease];
	return ret;
}
@end
