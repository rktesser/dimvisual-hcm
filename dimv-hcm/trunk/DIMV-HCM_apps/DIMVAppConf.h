#import <Foundation/NSString.h>
#import <Foundation/NSArray.h>
#import <Foundation/NSDictionary.h>

@interface DIMVAppConf : NSObject
{
	NSString *aggregatorId;
	NSString *name;
	NSArray *aggregators;
}

- (DIMVAppConf *) initWithConfFile: (NSString *)confFileName;
- (void) dealloc;
- (NSString *) getName;
- (NSString *) getAggregatorId;
- (NSArray *) getAggregators;

@end

