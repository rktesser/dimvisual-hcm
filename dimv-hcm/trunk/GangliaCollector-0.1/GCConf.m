#import "GCConf.h"

@implementation GCConf

- (GCConf *) initWithConfFile: (NSString *) confFileName
{
	NSDictionary *conf;
	char n_data_str[N_DATA_STR_SIZE];
	BOOL ret;

	self = [super init];
	
	//Populate a dictionary with the contents of the configuration files.
	RETAIN(confFileName);
	conf = [NSDictionary dictionaryWithContentsOfFile: confFileName];
	RETAIN(conf);
	RELEASE(confFileName);
	
	collectorType = @"ganglia";

	//get the configuration
	collectorId = [conf objectForKey: @"id"];
	RETAIN(collectorId);
	aggregators  = [conf objectForKey: @"aggregators"];
	RETAIN(aggregators);
	host = (NSString *)[conf objectForKey: @"host"];
	RETAIN(host);
	port = (NSString *)[conf objectForKey: @"port"]; 
	RETAIN(port);

	//Get the number of data receivings to accumulate before sending them.
	ret = [(NSString *)[conf objectForKey:@"send_each"] getCString: n_data_str maxLength: N_DATA_STR_SIZE encoding: NSASCIIStringEncoding];
	if(ret == NO){
		NSLog(@"Error: send_each name is too long.\n");
		return nil;
	}
	nData = atoi(n_data_str);

	pollingPeriod = [(NSString *)[conf objectForKey:  @"polling_period"] intValue];

	RELEASE(conf);//The configuration has been completely read.
	
	return self;
}

- (void) dealloc
{
	RELEASE(host);
	RELEASE(port);
	RELEASE(aggregators);
	RELEASE(collectorId);
	RELEASE(collectorType);
	[super dealloc];
}


- (BOOL)getHostAsCString: (char *)hostname
{
	BOOL ret;
	ret = [host getCString: hostname maxLength: GC_HOST_STR_SIZE encoding: NSASCIIStringEncoding];
	if(ret == NO){
		NSLog(@"error: getHostAsCString:  string buffer is too small to hold host name.\n");
	}
	return ret;
}

- (BOOL)getPortAsCString: (char *)portnumber
{
	BOOL ret;
	ret = [port getCString: portnumber maxLength: GC_PORT_STR_SIZE encoding: NSASCIIStringEncoding];
	if(ret == NO){
		NSLog(@"error: getPortAsCString:  string buffer is too small to hold port number.\n");
	}
	return ret;
}

- (int) getNData
{
	return nData;
}

- (int) getPollingPeriod
{
	return pollingPeriod;
}

- (NSString *) getCollectorId
{
	RETAIN(collectorId);
	AUTORELEASE(collectorId);
	return collectorId;
}

- (NSString *) getCollectorType
{
	RETAIN(collectorType);
	AUTORELEASE(collectorType);
	return collectorType;
}

- (NSArray *) getAggregators
{
	RETAIN(aggregators);
	AUTORELEASE(aggregators);
	return aggregators;
}

@end

