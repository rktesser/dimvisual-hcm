/*
   Copyright (c) 2005 Lucas Mello Schnorr <schnorr@gmail.com>
   
   This file is part of DIMVisual.
   
   DIMVisual is free software; you can redistribute it and/or modify it under
   the terms of the GNU Lesser General Public License as published by the
   Free Software Foundation; either version 2 of the License, or (at your
   option) any later version.
   
   DIMVisual is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
   for more details.
   
   You should have received a copy of the GNU Lesser General Public License
   along with DIMVisual; if not, write to the Free Software Foundation, Inc.,
   59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/
#ifndef __MPILIBRASTROFILEREADER_H
#define __MPILIBRASTROFILEREADER_H
#include <Foundation/Foundation.h>
#include <DIMVisual/Protocols.h>
#include <DIMVisual/Sync.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
//#include "../FileReader.h"
#include "MPIRASTROEvent.h"
#include <rastro_public.h>

@interface MPIRASTROFileReader : NSObject <FileReader>
{
	NSArray *filenames;
	rst_file_t data;
	rst_event_t event;
	MPIRASTROEvent *mpilibrastroevent;

	NSMutableDictionary *hostnames;
}
- (id) initWithMultipleFiles: (NSArray *) traceFiles
        andSyncFileName: (NSString *) syncFile;
- (NSString *) machineForNode: (int) node;
@end

#endif
