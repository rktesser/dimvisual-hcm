#ifndef DIMVCOLREG_H
#define DIMVCOLREG_H

#import <Foundation/NSCoder.h>
#import <Foundation/NSPortCoder.h>
#import <Foundation/NSArray.h>
#import <Foundation/NSString.h>
#import <Foundation/NSDictionary.h>

/*this class holds the data wich is propagated up into the hierarchy
 * in order to mantain a register of the available collectors in order to
 * indenty them and to be able to follow the path from the client to it.
 */

@interface CollectorRegister : NSObject <NSCoding/*, CollectorRegister*/>
{
 NSString *collectorId;
 NSString *collectorType;
 NSString *collectorLocation;
 NSString *originId; 
 NSMutableDictionary *subscribers;
}


- (NSMutableDictionary *)getSubscribers;
- (void)addSubscriber: (id)subscribe withId: (NSString *)subscriberId;
- (void)removeSubscriber: (NSString *)subscriberId; 

- (void)setCollectorId: (NSString *)colId;
- (void)setCollectorType: (NSString *)colType;
- (void)setOriginId: (NSString *)originId;
- (void)setLocation: (NSString *)aLocation;
- (NSString *)getCollectorId;
- (NSString *)getCollectorType;
- (NSString *)getOriginId;
- (NSString *)getCollectorLocation;
- (CollectorRegister *)initWithCollectorId: (NSString *)collectorId andType: (NSString *)type andLocation: (NSString *)aLocation;
- (void) dealloc;


//Methods from the NSCoding protocol
- (void) encodeWithCoder: (NSCoder *)coder;
- (id) initWithCoder: (NSCoder *)coder;
- (id)replacementObjectForPortCoder:(NSPortCoder *)encoder;

@end

#endif

