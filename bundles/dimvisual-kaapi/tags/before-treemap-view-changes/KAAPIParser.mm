/*
    DIMVisual-KAAPI, the DIMVisual bundle for KAAPI trace files
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual-KAAPI.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "KAAPIParser.h"

@implementation KAAPIParser
- (GEvent *) parseKAAPIEvent: (Util::RecordReader::const_iterator) r
{
	if (r->structure == 0){
		NSLog (@"r->structure is nil, end of file?");
		return nil;
	}

	int level, event, gid, cid;
	level = (int)*(char *)r->structure->get_field_data("_level",r->buffer);
	event = (int)*(char *)r->structure->get_field_data("_event",r->buffer);
	gid = (int)*(Util::NodeId *)r->structure->get_field_data("_gid", r->buffer);
	cid = (int)*(Util::ContextId *)r->structure->get_field_data("_cid", r->buffer);

	NSString *v1 = [NSString stringWithFormat: @"%d", level];
	NSString *v2 = [NSString stringWithFormat: @"%d", event];

	GIdentifier *identifier = [[GIdentifier alloc] init];
	[identifier addValue: v1];
	[identifier addValue: v2];
	NSString *t1 = [NSString stringWithFormat: @"%lli",
		(unsigned long long)*(Util::HighResTimer::type*)
			r->structure->get_field_data ("_date",r->buffer)];

	GTimestamp *timestamp = [[GTimestamp alloc] init];
	[timestamp setTimestamp: t1];

	GEvent *ret = nil;
	

	switch (level){
//		case Event::TR_TRACE_LEVEL:
//			ret = [self parseKAAPITraceLevel: r withIdentifier: identifier withTimestamp: timestamp];
//			break;
		case Event::TR_UTIL_LEVEL:
			ret = [self parseKAAPIUtilLevel: r withIdentifier: identifier withTimestamp: timestamp];
			break;
/*
		case Event::TR_NETWORK_LEVEL:
//			ret = [self parseKAAPINetworkLevel: r withIdentifier: identifier withTimestamp: timestamp];
			break;
*/
		case Event::TR_KERNEL_LEVEL:
			ret = [self parseKAAPIKernelLevel: r withIdentifier: identifier withTimestamp: timestamp];
			break;
/*
		case Event::TR_RFO_LEVEL:
		case Event::TR_WS_LEVEL:
		case Event::TR_DFG_LEVEL:
		case Event::TR_ST_LEVEL:
		case Event::TR_FT_LEVEL:
*/
		default:
//			ret = [self generic: r];
//			[ret setTimestamp: timestamp];
//			[ret setIdentifier: identifier];
			break;
	}
	[identifier release];
	[timestamp release];

	if (ret != nil){
		GFields *f = [ret fields];
        
        
		NSString *value1 = [NSString stringWithFormat: @"%d", gid];
		NSString *value2 = [NSString stringWithFormat: @"%d", cid];
		NSString *value3 = [NSString stringWithFormat: @"%d", level];
		NSString *value4 = [NSString stringWithFormat: @"%d", event];
        
		GIdentifier *nameid1 = [[GIdentifier alloc] init];
		GIdentifier *nameid2 = [[GIdentifier alloc] init];
		GIdentifier *nameid3 = [[GIdentifier alloc] init];
		GIdentifier *nameid4 = [[GIdentifier alloc] init];
		[nameid1 addValue: @"gid"];
		[nameid2 addValue: @"cid"];
		[nameid3 addValue: @"level"];
		[nameid4 addValue: @"event"];
        
		[f setFieldWithKey: nameid1 withValue: value1];
		[f setFieldWithKey: nameid2 withValue: value2];
		[f setFieldWithKey: nameid3 withValue: value3];
		[f setFieldWithKey: nameid4 withValue: value4];

		[nameid1 release];
		[nameid2 release];
		[nameid3 release];
		[nameid4 release];
	}
	return ret;
}

- (GEvent *) generic: (Util::RecordReader::const_iterator) r
{
        GEvent *ev;
        GName *name;
        GFields *fields;

        ev = [[GEvent alloc] init];
        name = [[GName alloc] init];
        fields = [[GFields alloc] init];

        [name setName: @"generic"];
        [ev setName: name];
        [ev setFields: fields];

        [name release];
        [fields release];

        [ev autorelease];
        return ev;
}

@end

