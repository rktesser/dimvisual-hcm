#import "Observer.h"

@implementation Observer
- (void) connectionDied: (NSNotification *)notification
{
	NSLog(@"Connection died! Exiting.");
	exit(1);
}

@end

