/*
    DIMVisual-KAAPI, the DIMVisual bundle for KAAPI trace files
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual-KAAPI.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __KAAPIDATASOURCE_H
#define __KAAPIDATASOURCE_H
#include <Foundation/NSDictionary.h>
#include <DIMVisual/Protocols.h>
#include <DIMVisual/Ordering.h>
#include <GenericEvent/GEvent.h>
#include "KAAPIConverter.h"
#include "KAAPIFileReader.h"
#include "KAAPIOrder.h"
#include "KAAPIConversionType.h"

#define IDKEY @"ID"
#define IDVALUE @"KAAPI"
#define PARAMETERKEY @"parameters"
#define FILEKEY @"files"
#define SYNCKEY @"sync"
#define MACHINEKEY @"machine"

@class KAAPIConverter;
@class KAAPIOrder;


@interface KAAPIDataSource : NSObject <DataSource>
{
	NSMutableDictionary *eventsSelected;

	KAAPIOrder *reader;
	KAAPIConverter *converter;
	id<Integrator> prov;

	KAAPIConversionType conversionType;
}
@end

@interface KAAPIDataSource (SelectedEvents)
- (BOOL) setSelectedEvents: (NSMutableDictionary *) selection;
- (BOOL) setSelectedType: (id) selected;
- (BOOL) readLevel: (int) level andEvent: (int) event;
@end

#endif
