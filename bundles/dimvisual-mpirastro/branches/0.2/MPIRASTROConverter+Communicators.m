/*
   Copyright (c) 2005 Lucas Mello Schnorr <schnorr@gmail.com>
   
   This file is part of DIMVisual.
   
   DIMVisual is free software; you can redistribute it and/or modify it under
   the terms of the GNU Lesser General Public License as published by the
   Free Software Foundation; either version 2 of the License, or (at your
   option) any later version.
   
   DIMVisual is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
   for more details.
   
   You should have received a copy of the GNU Lesser General Public License
   along with DIMVisual; if not, write to the Free Software Foundation, Inc.,
   59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/
#include "MPIRASTROConverter.h"

@implementation MPIRASTROConverter (Communicators)
- (id) convertEventCommunicators: (id<GEvent>) event
{
	NSMutableArray *ret = nil;
	NSString *container;
//	int type;

	
	ret = [[NSMutableArray alloc] init];

	container = [provider identifierForEntity: 
		[NSArray arrayWithObjects: @"Machine", @"Thread", nil]
				ids:
		[NSArray arrayWithObjects: [event machine], [event thread], nil]];

	if (container == nil){
		[ret release];
		return nil;
	}
	
	int etype = atoi ([[event type] cString]);

	if (etype == MPI_COMM_COMPARE_IN){
		[ret addObject: [self addPajeSetState: @"MPI_COMM_COMPARE" time: [event time] container: container]];
	}else if (etype == MPI_COMM_CREATE_IN){
		[ret addObject: [self addPajeSetState: @"MPI_COMM_CREATE" time: [event time] container: container]];
	}else if (etype == MPI_COMM_DUP_IN){
		[ret addObject: [self addPajeSetState: @"MPI_COMM_DUP" time: [event time] container: container]];
	}else if (etype == MPI_COMM_FREE_IN){
		[ret addObject: [self addPajeSetState: @"MPI_COMM_FREE" time: [event time] container: container]];
	}else if (etype == MPI_COMM_GROUP_IN){
		[ret addObject: [self addPajeSetState: @"MPI_COMM_GROUP" time: [event time] container: container]];
	}else if (etype == MPI_COMM_RANK_IN){
		[ret addObject: [self addPajeSetState: @"MPI_COMM_RANK" time: [event time] container: container]];
	}else if (etype == MPI_COMM_REMOTE_GROUP_IN){
		[ret addObject: [self addPajeSetState: @"MPI_COMM_REMOTE_GROUP" time: [event time] container: container]];
	}else if (etype == MPI_COMM_REMOTE_SIZE_IN){
		[ret addObject: [self addPajeSetState: @"MPI_COMM_REMOTE_SIZE" time: [event time] container: container]];

	/* MPI_COMM_SIZE_IN */
	}else if (etype == MPI_COMM_SIZE_IN){
		[ret addObject: [self addPajeSetState: @"MPI_COMM_SIZE" time: [event time] container: container]];

	/* MPI_COMM_SIZE_OUT */
	}else if (etype == MPI_COMM_SIZE_OUT){
		NSString *size = [event int32AtIndex: 0];

		PajeNewEvent *ev;
		ev = [[PajeNewEvent alloc] initWithTime: [event time]];
		[ev setContainer: container];
		[ev setEntityType: [provider aliasToName: @"Events"]];
		[ev setValue: [NSString stringWithFormat: @"MPI_Comm_size (%@)", size]];
		[ret addObject: ev];
		[ev release];

		[ret addObject: [self addPajeSetState: @"running" time: [event time] container: container]];
		
	}else if (etype == MPI_COMM_GET_NAME_IN){
		[ret addObject: [self addPajeSetState: @"MPI_COMM_GET_NAME" time: [event time] container: container]];
	}else if (etype == MPI_COMM_GET_NAME_OUT){
		[ret addObject: [self addPajeSetState: @"running" time: [event time] container: container]];
	}else if (etype == MPI_COMM_SET_NAME_IN){
		[ret addObject: [self addPajeSetState: @"MPI_COMM_SET_NAME" time: [event time] container: container]];
	}else if (etype == MPI_COMM_SET_NAME_OUT){
		[ret addObject: [self addPajeSetState: @"running" time: [event time] container: container]];

	/* MPI_COMM_SPLIT_IN */
	}else if (etype == MPI_COMM_SPLIT_IN){
		NSString *color = [event int32AtIndex: 0];
		NSString *key = [event int32AtIndex: 1];
	
		PajeSetState *set = [self addPajeSetState: @"MPI_COMM_SPLIT" time: [event time] container: container];
		[set retain];
		[set addFieldNamed: @"KEY" withType: @"string"];
		[set setValue: key forFieldNamed: @"KEY"];
		[ret addObject: set];
		[set release];

		NSString *colorState = [NSString stringWithFormat: @"MPI_COMM_SPLIT-color-%@", color];
		NSString *alias = [provider aliasToName: colorState];
		if (alias == nil){
			alias = [provider newAliasToName: colorState];
			PajeDefineEntityValue *dst;
			dst = [[PajeDefineEntityValue alloc] init];
			[dst setTime: nil];
			[dst setName: colorState];
			[dst setAlias: alias];
			[dst setEntityType:[provider aliasToName: @"MPIState"]];
			[ret addObject: dst];
			[dst release];
		}
		
		PajePushState *push;
		push = [[PajePushState alloc] init];
		[push setTime: [event time]];
		[push setEntityType: [provider aliasToName: @"MPIState"]];
		[push setContainer: container];
		[push setValue: alias];
		[ret addObject: push];
		[push release];
		

	/* MPI_COMM_SPLIT_OUT */
	}else if (etype == MPI_COMM_SPLIT_OUT){
	
		PajePopState *pop;
		pop = [[PajePopState alloc] init];
		[pop setTime: [event time]];
		[pop setEntityType: [provider aliasToName: @"MPIState"]];
		[pop setContainer: container];
		[ret addObject: pop];
		[pop release];
		
		[ret addObject: [self addPajeSetState: @"running" time: [event time] container: container]];

	}else if (etype == MPI_COMM_TEST_INTER_IN){
		[ret addObject: [self addPajeSetState: @"MPI_COMM_TEST_INTER" time: [event time] container: container]];
	}else if (
		etype == MPI_COMM_COMPARE_OUT
		|| etype == MPI_COMM_CREATE_OUT
		|| etype == MPI_COMM_DUP_OUT
		|| etype == MPI_COMM_FREE_OUT
		|| etype == MPI_COMM_GROUP_OUT
		|| etype == MPI_COMM_RANK_OUT
		|| etype == MPI_COMM_REMOTE_GROUP_OUT
		|| etype == MPI_COMM_REMOTE_SIZE_OUT
		|| etype == MPI_COMM_TEST_INTER_OUT){
			[ret addObject: [self addPajeSetState: @"running" time: [event time] container: container]];
	}
	[ret autorelease];
	return ret;
}
@end
