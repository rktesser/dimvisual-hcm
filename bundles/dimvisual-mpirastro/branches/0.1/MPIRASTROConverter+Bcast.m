/*
   Copyright (c) 2005 Lucas Mello Schnorr <schnorr@gmail.com>
   
   This file is part of DIMVisual.
   
   DIMVisual is free software; you can redistribute it and/or modify it under
   the terms of the GNU Lesser General Public License as published by the
   Free Software Foundation; either version 2 of the License, or (at your
   option) any later version.
   
   DIMVisual is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
   for more details.
   
   You should have received a copy of the GNU Lesser General Public License
   along with DIMVisual; if not, write to the Free Software Foundation, Inc.,
   59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/
#include "MPIRASTROConverter.h"

static OneToAll *bcast;

@implementation MPIRASTROConverter (Bcast)
- (id) convertEventBcast: (MPIRASTROEvent *) event
{
	NSMutableArray *ret = [[NSMutableArray alloc] init];
	NSString *container = [provider identifierForEntity: 
		[NSArray arrayWithObjects: @"Machine", @"Thread", nil]
				ids:
		[NSArray arrayWithObjects:[event machine],[event thread], nil]];

	if (bcast == nil){
		bcast = [[OneToAll alloc] init];
	}

	if (container == nil){
		[ret release];
		return nil;
	}
	
	int etype = atoi ([[event type] cString]);
	/* MPI_BCAST_IN */
	if (etype == MPI_BCAST_IN){
		NSString *count = [event int32AtIndex: 0];
		NSString *size = [event int32AtIndex: 1];
		NSString *root = [event int32AtIndex: 2];
		NSString *members = [event stringAtIndex: 0];
		
		[bcast memberEntering: container
			andEventNode: [event node]
			andEventTime: [event time]
			andRootNode: root
			andCommSize: size
			andMembersId: members];

		PajeSetState *set;
		set = [self addPajeSetState: @"MPI_BCAST" time: [event time] container: container];
		[set retain];
		[set addFieldNamed: @"COUNT" withType: @"string"];
		[set setValue: count forFieldNamed: @"COUNT"];
		[ret addObject: set];
		[set release];
	
	/* MPI_BCAST_OUT */
	}else if (etype == MPI_BCAST_OUT){
		NSString *root = [event int32AtIndex: 0];
		NSString *members = [event stringAtIndex: 0];

		[ret addObjectsFromArray: [bcast memberExiting: container andEventTime: [event time] andOperationName: @"bcast" andRootNode: root andMembersId: members andProvider: provider]];
		[ret addObject: [self addPajeSetState: @"running" time: [event time] container: container]];
	}

	[ret autorelease];
	return ret;
}
@end
