#import "Classe.h"
int main(int argc, char **argv)
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
	Classe * umaClasse = [[Classe alloc] init];
	while(1){
		[umaClasse imprime: @"main"];
		[umaClasse imprimeRemoto: @"remoto"];
		[NSThread  sleepForTimeInterval: 1];
	}
	[umaClasse release];
	[pool release];
	return 1;
}
