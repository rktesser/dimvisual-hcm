/*
    DIMVisual, the Data Integration Model for Visualization of trace files.
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <Foundation/Foundation.h>
#include "Command.h"
#include "IntegratorExec.h"

int main(int argc, char **argv)
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	Command *command = [[Command alloc] initWithArgc: argc andArgv: argv];

	if (command == nil){
		return 1;
	}
	IntegratorExec *integrator;
	integrator = [[IntegratorExec alloc] initWithCommand: command];
	[command release];
	
	if (integrator == nil){
		NSLog (@"DIMVisual: Problem with command.");
		return 1;
	}
	
	NSLog (@"Starting integration... Please wait");
	
	[integrator convert];
	
	NSLog(@"End of program - pool has %d objects", [pool autoreleaseCount]);
	[pool release];
	return 0;
}
