/*
    DIMVisual-Ganglia, the DIMVisual bundle for Ganglia trace files
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual-Ganglia.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#import "GangliaHCMConverter.h"

@implementation GangliaHCMConverter
- (id) initWithProvider: (id<Integrator>) prov
{
	self = [super init];

	grid5000 = [NSDictionary dictionaryWithContentsOfFile:
		[[NSBundle bundleForClass: [self class]]
		pathForResource: @"Grid5000" ofType: @"plist"]];

	provider = prov;
	return self;
}

- (void) dealloc
{
	[super dealloc];
}

/*
 *
 */
- (PajeSetState *) addPajeSetState: (NSString *) state toState: (NSString *)   statename time: (NSString *) timex container: (NSString *) cont
{
        PajeSetState *set = [[PajeSetState alloc] initWithTime: timex];
        [set setEntityType: [provider aliasToName: statename]];
        [set setValue: [provider aliasToName: state]];
        [set setContainer: cont];
        [set autorelease];
        return set;
}

/*
 *
 */
- (id) convertEvent: (GEvent *) event
{
	NSMutableArray *ret = [[NSMutableArray alloc] init];

//	NSLog (@"(%s) event = %@", __FUNCTION__, event);
	GFields *f = [event fields];

	NSString *time, *hostname;
	time = [[event timestamp] description];
	hostname = [[f fieldWithSimpleStringKey:@"hostname"] description];
	
	if (hostname == nil){
		NSString *str;
		str = [NSString stringWithFormat: @"GangliaHCMConverter (%@): "
			"hostname is not defined. It expect a hostname in "
			"the trace", self];
		[[NSException exceptionWithName: @"DIMVisual-GangliaHCMConverter" 
			reason: str userInfo: nil] raise];
	}

	NSString *site;
	NSString *cluster;
	NSString *machine;
//	NSString *process;

	NSArray *aux = [hostname componentsSeparatedByString: @"-"];
	NSString *straux = [aux objectAtIndex: 0];

	if ([grid5000 objectForKey: straux] != nil) { //grid5000 case
		site = [grid5000 objectForKey: straux];
		cluster = straux;

		NSString *rest = [aux objectAtIndex: 1];
		NSArray *aux2;
		aux2 = [rest componentsSeparatedByString: @"."];
	
		machine = [aux2 objectAtIndex: 0];
	}else{
		site = cluster = machine = hostname;
	}
	
	NSString *category = [[f fieldWithSimpleStringKey: @"category"] description];
/*
	NSArray *creation = [provider 
	  createContainerForEntity: [NSArray arrayWithObjects: @"site", 
	    @"cluster", @"machine", @"category", nil]
	  ids: [NSArray arrayWithObjects: site, cluster, machine, category, nil]
	  time: time];
        NSString *container = [provider 
	  identifierForEntity:[NSArray arrayWithObjects: @"site", @"cluster", 
	    @"machine", @"category", nil]
	  ids: [NSArray arrayWithObjects: site, cluster, machine, category, 
	    nil]];
*/
	NSArray *creation = [provider 
	  createContainerForEntity: [NSArray arrayWithObjects: @"site", 
	    @"cluster", @"machine", category, nil]
	  ids: [NSArray arrayWithObjects: site, cluster, machine, category, nil]
	  time: time];
        NSString *container = [provider 
	  identifierForEntity:[NSArray arrayWithObjects: @"site", @"cluster", 
	    @"machine", category, nil]
	  ids: [NSArray arrayWithObjects: site, cluster, machine, category, 
	    nil]];
	if ([creation count] != 0){
		[ret addObjectsFromArray: creation];
	}

        PajeSetVariable *set = [[PajeSetVariable alloc] initWithTime: time];
        [set setEntityType: [provider aliasToName: [[event name] description]]];
        [set setValue: [[f fieldWithSimpleStringKey: @"value"] description]];
        [set setContainer: container];
	[ret addObject: set];
        [set autorelease];
	
	[ret autorelease];
	return ret;
}
@end
