/*
    DIMVisual, the Data Integration Model for Visualization of trace files.
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __BUNDLECENTER_H
#define __BUNDLECENTER_H
#include <Foundation/Foundation.h>
#include "Protocols.h"

#define RST_CLOCK_RESOLUTION 1000000

@interface BundleCenter : NSObject
{
	NSMutableArray *bundlePaths;
	NSMutableDictionary *bundlesLoaded;
}
- (id) init;
- (void) addBundleDirectory: (NSString *) newPath;
- (NSArray *) listBundlesAvailable;
- (NSDictionary *) configurationForBundleWithName: (NSString *) name;
- (NSDictionary *) configureBundlesWithThis: (NSDictionary *) conf;
- (NSDictionary *) configureBundle: (NSString *) name withThis: (NSDictionary *) conf;
- (BOOL) loadBundleWithName: (NSString *) name;
- (BOOL) isBundleWithNameLoaded: (NSString *) name;
- (NSBundle *) bundleNamed: (NSString *) name;
- (NSString *) principalClassOfBundleNamed: (NSString *) name;
@end

#endif
