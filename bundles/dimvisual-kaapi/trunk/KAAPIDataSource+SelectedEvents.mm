/*
    DIMVisual-KAAPI, the DIMVisual bundle for KAAPI trace files
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual-KAAPI.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "KAAPIDataSource.h"


@implementation KAAPIDataSource (SelectedEvents)
- (BOOL) readLevel: (int) level andEvent: (int) event
{
	/* events that should always be present */
	if (level == Event::TR_UTIL_LEVEL){
		if (event == Event::UTIL_PROCESS_INFO ||
			event == Event::UTIL_PROCESS_NAME ||
			event == Event::UTIL_PROCESS_BLOC_NAME ||
			event == Event::UTIL_THREAD_NAME){
			return YES;
		}
	}

	/* normal filter behavior */
	NSString *levels = [NSString stringWithFormat: @"%d", level];
	NSString *events = [NSString stringWithFormat: @"%d", event];
	BOOL ret;

	if (eventsSelected == nil){
		ret = NO;
	}else{
		NSSet *levelset = [eventsSelected objectForKey: levels];
		if (levelset == nil){
			ret = NO;
		}else{
			if ([levelset containsObject: events]){
				ret = YES;
			}else{
				ret = NO;
			}
		}
	}
	return ret;
}

- (BOOL) setSelectedEvents: (NSMutableDictionary *) selection
{
	NSDictionary *selectableEvents;
	NSSet *selectableEventsSet;
	NSSet *selectedEventsSet;

	selectableEvents = [NSDictionary dictionaryWithContentsOfFile: [[NSBundle bundleForClass: [self class]] pathForResource: @"KAAPIEvents" ofType: @"plist"]];
	selectableEventsSet = [NSSet setWithArray: [selectableEvents allKeys]];

	if (selection == nil){
		NSLog(@"DIMVisual-KAAPI: No events selected. I will read all.");
		selectedEventsSet = selectableEventsSet;
	}else{
		if ([selection isKindOfClass: [NSDictionary class]] == NO){
			NSLog(@"DIMVisual-KAAPI: No events selected. "
				"I will read all.");
			selectedEventsSet = selectableEventsSet;
		}else{
			selectedEventsSet = [NSSet setWithArray:
						[selection allKeys]];
		}
	}

	if (![selectedEventsSet isSubsetOfSet: selectableEventsSet]){
		return NO;
	}

	eventsSelected = [[NSMutableDictionary alloc] init];

	NSEnumerator *enumerator = [selectedEventsSet objectEnumerator];
	id val;
	while ((val = [enumerator nextObject])){
		NSString *s = [selectableEvents objectForKey: val];
		NSArray *ar = [s componentsSeparatedByString:@","];
		NSString *level = [ar objectAtIndex: 0];
		NSString *event = [ar objectAtIndex: 1];

		/* check to see if level is in eventsSelected */
		NSMutableSet *levelset;
		levelset = [eventsSelected objectForKey: level];
		if (levelset != nil){
			[levelset addObject: event];
		}else{
			levelset = [NSMutableSet set];
			[levelset addObject: event];
			[eventsSelected setObject: levelset forKey: level];
		}
	}
	return YES;
}

- (BOOL) setSelectedType: (id) selected
{
	if (selected == nil){
		return NO;
	}

	NSString *t;
	if ([selected isKindOf: [NSSet class]]){
		if ([selected count] != 1){
			return NO;
		}
		t = [selected anyObject];
	}else if ([selected isKindOf: [NSString class]]){
		t = selected;
	}else if ([selected isKindOf: [NSArray class]]){
		if ([selected count] != 1){
			return NO;
		}
		t = [selected objectAtIndex: 0];
	}else{
		return NO;
	}

	if ([t isEqual: @"process"]){
		conversionType = Process;
		return YES;
	}else if ([t isEqual: @"thread"]){
		conversionType = Thread;
		return YES;
	}else if ([t isEqual: @"grid"]){
		conversionType = Grid;
		return YES;
	}else if ([t isEqual: @"gridthread"]){
		conversionType = GridThread;
		return YES;
	}else{
		return NO;
	}
}
@end
