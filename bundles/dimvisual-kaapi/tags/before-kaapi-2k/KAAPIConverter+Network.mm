/*
    DIMVisual-KAAPI, the DIMVisual bundle for KAAPI trace files
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual-KAAPI.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "KAAPIConverter.h"
#include <kaapi> /* for the trace and level stuff (identifier) */

@implementation KAAPIConverter (Network)
- (id) convertEventNetworkMsgSend0: (GEvent *) event
{
        NSMutableArray *ret = [[NSMutableArray alloc] init];

	GFields *f = [event fields];
//	NSString *key = [NSString stringWithFormat: @"n%@-%@-%@", thread, [f fieldWithSimpleStringKey: @"dest"],  [f  fieldWithSimpleStringKey: @"serial"]];

	NSString *key = [NSString stringWithFormat: @"n%@-%@", [mappingGidHostname objectForKey: hostname], [f  fieldWithSimpleStringKey: @"serial"]];

	PajeStartLink *sl;
	sl = [[PajeStartLink alloc] init];
	[sl setTime: time];
	[sl setContainer: [provider identifierForEntity: [NSArray
arrayWithObjects: @"0", nil] ids: [NSArray arrayWithObjects: @"0", nil]]]; 
	[sl setSourceContainer: container];
	[sl setEntityType: [provider aliasToName:@"Network"]];
	[sl setValue: [provider aliasToName: @"send"]];
	[sl setKey: key];
	[ret addObject: sl];
	[sl release];


        [ret autorelease];
        return ret;
}

- (id) convertEventNetworkMsgRecv1: (GEvent *) event
{
        NSMutableArray *ret = [[NSMutableArray alloc] init];

	GFields *f = [event fields];
//	NSString *key = [NSString stringWithFormat: @"n%@-%@-%@", [f fieldWithSimpleStringKey: @"src"], thread, [f  fieldWithSimpleStringKey: @"serial"]]; 
	NSString *key = [NSString stringWithFormat: @"n%@-%@", [f fieldWithSimpleStringKey: @"src"], [f  fieldWithSimpleStringKey: @"serial"]]; 

	PajeEndLink *el;
	el = [[PajeEndLink alloc] init];
	[el setTime: time];
	[el setContainer: [provider identifierForEntity: [NSArray
arrayWithObjects: @"0", nil] ids: [NSArray arrayWithObjects: @"0", nil]]];
	[el setDestContainer: container];
	[el setEntityType: [provider aliasToName:@"Network"]];
	[el setValue: [provider aliasToName: @"send"]];
	[el setKey: key];
	[ret addObject: el];
	[el release];


	[ret autorelease];
        return ret;
}


- (id) convertEventNetwork: (GEvent *) event
{
	NSMutableArray *ret = [[NSMutableArray alloc] init];

	switch (atoi ([eventid cString])){
		case Event::NET_MSG_SEND0:
			[ret addObjectsFromArray: [self convertEventNetworkMsgSend0: event]];
			break;
		case Event::NET_MSG_RECV1:
			[ret addObjectsFromArray: [self convertEventNetworkMsgRecv1: event]];
			break;
		case Event::NET_IODAEMON_STEAL0:
			[ret addObject: [self addPajeSetState: @"STEAL"
toState: @"State" time: time container: container]];
			break;
		case Event::NET_IODAEMON_STEAL1:
			[ret addObject: [self addPajeSetState: @"RUNNING"
toState: @"State" time: time container: container]];
			break;
		default:
//			NSLog (@"%s: Warning, event type %@ not converted", __FUNCTION__, eventid);
			break;
	}

	[ret autorelease];
	return ret;
}
@end
