#import "AuxCopiableDictionary.h"

@implementation AuxCopiableDictionary

- (void) encodeWithCoder: (NSCoder *)encoder
{
	id key;
	unsigned count;
	NSEnumerator * keyEnum;
//	NSLog(@"Encode.\n");
	count = [self count];
	keyEnum = [self keyEnumerator];
//	[super encodeWithCoder: encoder];
	[encoder encodeValueOfObjCType: @encode(unsigned) at: &count];
	while((key = [keyEnum nextObject]) != nil){
		[encoder encodeBycopyObject: key];
		[encoder encodeBycopyObject: [self objectForKey: key]];
	}
}

- (id) initWithCoder: (NSCoder *)decoder
{
	id key;
	id anObject;
	unsigned count;
	int i;
//	self = [super initWithCoder:decoder];
	self = [super init];
	[decoder decodeValueOfObjCType: @encode(unsigned) at: &count];
	embeddedDictionary = [[NSMutableDictionary alloc] initWithCapacity: count];
	for(i=0;i < count;i++){
		key = [[decoder decodeObject] retain];
		anObject = [[decoder decodeObject] retain];
		[embeddedDictionary setObject: anObject forKey: key];
		RELEASE(key);
		RELEASE(anObject);
	}
	return self;
}

- (id) replacementObjectForPortCoder: (NSPortCoder *)encoder
{
	return self;
}


- (void) setObject: (id)anObject forKey: (id)aKey
{
	[embeddedDictionary setObject: anObject forKey: aKey];
//	NSLog(@"%@ is the key to %@.\n", aKey, [embeddedDictionary objectForKey: aKey]);
}

- (void) removeObjectForKey: (id)aKey
{
	[embeddedDictionary removeObjectForKey: aKey];
}

- (id) initWithCapacity: (unsigned)numItems
{
	self = [super init];
	embeddedDictionary = [[NSMutableDictionary alloc] initWithCapacity: numItems];
	return self;
}

- (unsigned) count
{
	return [embeddedDictionary count];
}

- (id) initWithObjects: (id*)objects forKeys: (id*)keys count: (unsigned)count
{
	self = [super init];
	embeddedDictionary = [[NSMutableDictionary alloc] initWithObjects: objects forKeys: keys count: count];
	return self;
}

- (NSEnumerator*) keyEnumerator
{
	return [embeddedDictionary keyEnumerator];
}

- (NSEnumerator*) objectEnumerator
{
	return [embeddedDictionary objectEnumerator];
}

- (id) objectForKey: (id)aKey
{
	return [embeddedDictionary objectForKey: aKey];
}

- (void) dealloc
{
	RELEASE(embeddedDictionary);
	[super dealloc];
}

@end
