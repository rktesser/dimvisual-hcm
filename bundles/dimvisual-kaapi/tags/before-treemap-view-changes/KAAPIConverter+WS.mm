/*
    DIMVisual-KAAPI, the DIMVisual bundle for KAAPI trace files
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual-KAAPI.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "KAAPIConverter.h"
#include <kaapi> /* for the trace and level stuff (identifier) */

@implementation KAAPIConverter (WS)
- (id) convertEventWSSend: (GEvent *) event
{
//	static long long int count = 0;

//	GFields *f = [event fields];
        NSMutableArray *ret = [[NSMutableArray alloc] init];

#ifndef JEITO_ANTIGO_WS
/*
	NSString *origin = [mappingGidHostname objectForKey: hostname];
	NSString *destin = [f fieldWithSimpleStringKey: @"gidvictim"];
//	if ([origin isEqual: @"2"] && [destin isEqual: @"11"])
//		NSLog (@"send from %@ to %@", origin, destin);


	NSString *dictKey, *key;

	dictKey = [NSString stringWithFormat: @"%@-%@", origin, destin];
	key = [NSString stringWithFormat: @"ws%@-%@-%d", origin,destin,count++];
//	NSLog (@"key = %@", key);


	NSMutableArray *a = [workStealingLinks objectForKey: dictKey];
	if (a == nil){
		a = [[NSMutableArray alloc] init];
		[workStealingLinks setObject: a forKey: dictKey];
		[a release];
	}

	if ([a count] == 0){
		[a addObject: key];
//		NSLog (@"\t\t adicionou chave a=%@",a);
	}else{
		unsigned int i;
		PajeEndLink *el = nil;
		for (i=0;i<[a count];i++){
			if ([[a objectAtIndex: i] isKindOfClass: [PajeEndLink class]]){
				el = (PajeEndLink *)[a objectAtIndex: i];
				break;
			}
		}
		if (el != nil){
//			NSLog (@"achei el = %@", el);
			[el setKey: key];
			[ret addObject: el];
			[a removeObjectAtIndex: i];
//			NSLog (@"\t\t gerou endlink com chave %@", key);
		}else{
//			NSLog (@"\t\t aaaa");
			[a addObject: key];
		}
	}

	PajeStartLink *sl;
	sl = [[PajeStartLink alloc] init];
	[sl setTime: time];
	[sl setContainer: [provider identifierForEntity: [NSArray
arrayWithObjects: @"0", nil] ids: [NSArray arrayWithObjects: @"0", nil]]]; 
	[sl setSourceContainer: container];
	[sl setEntityType: [provider aliasToName:@"Network"]];
	[sl setValue: [provider aliasToName: @"send"]];
	[sl setKey: key];
	[ret addObject: sl];
	[sl release];
*/
#endif


        [ret autorelease];
        return ret;
}

- (id) cleanWorkStealingBuffer
{
	NSMutableArray *ret = [[NSMutableArray alloc] init];


	unsigned int i;
	NSArray *ab = [workStealingLinks allKeys];
	for (i = 0; i < [ab count]; i++){
		NSString *destin = [ab objectAtIndex: i];
		if ([mappingGidHostname objectForKey: destin] != nil){
			NSLog (@"cleaning %@", destin);
			NSMutableArray *a = [workStealingLinks objectForKey:
destin];
			unsigned int j;
			NSString *hostname2 = [mappingGidHostname objectForKey:
destin];
			NSString *site;
			site = [[hostname2 componentsSeparatedByString: @"."] objectAtIndex: 1];
			NSString *destinA = [NSString stringWithFormat:
@"%@_at_%@", destin, hostname2];
			NSString *destContainer = [provider identifierForEntity: [NSArray arrayWithObjects: @"site", @"gid", nil] ids: [NSArray arrayWithObjects: site, destinA, nil]];
			PajeEndLink *el;
			for (j = 0; j < [a count]; j++){
				el = [a objectAtIndex: j];
				[el setDestContainer: destContainer];
				[ret addObject: el];
			}
			[a removeAllObjects];
			[workStealingLinks removeObjectForKey: destin];
		}
	}


	[ret autorelease];
	return ret;
}

- (id) convertEventWSRecv: (GEvent *) event
{
	GFields *f = [event fields];
        NSMutableArray *ret = [[NSMutableArray alloc] init];

#ifdef JEITO_ANTIGO_WS
/*
	NSString *origin = [f fieldWithSimpleStringKey: @"gidvictim"];
	NSString *destin = [mappingGidHostname objectForKey: hostname];

//	if ([origin isEqual: @"2"] && [destin isEqual: @"11"])
//		NSLog (@"recv at %@ from %@", destin, origin);

	NSString *dictKey, *key = nil;

	dictKey = [NSString stringWithFormat: @"%@-%@", origin, destin];
	NSMutableArray *a = [workStealingLinks objectForKey: dictKey];
	if (a == nil){
		a = [[NSMutableArray alloc] init];
		[workStealingLinks setObject: a forKey: dictKey];
		[a release];
	}

	if ([a count] == 0){
		PajeEndLink *el;
		el = [[PajeEndLink alloc] init];
		[el setTime: time];
		[el setContainer: [provider identifierForEntity: [NSArray arrayWithObjects: @"0", nil] ids: [NSArray arrayWithObjects: @"0", nil]]];
		[el setDestContainer: container];
		[el setEntityType: [provider aliasToName:@"Network"]];
		[el setValue: [provider aliasToName: @"send"]];
		[el setKey: @"none"];
		[a addObject: el];
		[el release];
	}else{
		unsigned int i;
		for (i=0;i<[a count];i++){
			if ([[a objectAtIndex: i] isKindOfClass: [NSString class]]){
				key = (NSString *)[a objectAtIndex: i];
				break;
			}
		}
		PajeEndLink *el;
		el = [[PajeEndLink alloc] init];
		[el setTime: time];
		[el setContainer: [provider identifierForEntity: [NSArray arrayWithObjects: @"0", nil] ids: [NSArray arrayWithObjects: @"0", nil]]];
		[el setDestContainer: container];
		[el setEntityType: [provider aliasToName:@"Network"]];
		[el setValue: [provider aliasToName: @"send"]];
		if (key != nil){
			[el setKey: key];
			[ret addObject: el];
			[a removeObjectAtIndex: i];
		}else{
			[el setKey: @"none2"];
			[a addObject: el];
		}
		[el release];
	}
*/
#else
#ifdef ORGANIZATION_BY_PROCESS
	static long long int count = 0;
	NSString *origin = [mappingGidHostname objectForKey: hostname];
	NSString *destin = [f fieldWithSimpleStringKey: @"gidvictim"];

	NSString *site;
	site = [[hostname componentsSeparatedByString: @"."] objectAtIndex: 1];

	NSString *originA = [NSString stringWithFormat: @"%@_at_%@", origin, hostname];
	NSString *destinA = [NSString stringWithFormat: @"%@_at_%@", destin, [mappingGidHostname objectForKey: destin]];

//	NSLog (@"originA = %@, destinA = %@", originA, destinA);


	NSString *sourceContainer = [provider identifierForEntity: [NSArray arrayWithObjects: @"site", @"gid", nil] ids: [NSArray arrayWithObjects: site, originA, nil]];

	NSString *hostname2 = [mappingGidHostname objectForKey: destin];
	NSString *site2;
	site2 = [[hostname2 componentsSeparatedByString: @"."] objectAtIndex: 1];
//	NSLog (@"site2 = %@", site2);

	NSString *destContainer = @"";
	if (site2 != nil){
		destContainer = [provider identifierForEntity: [NSArray arrayWithObjects: @"site", @"gid", nil] ids: [NSArray arrayWithObjects: site2, destinA, nil]];
	}


	

	NSString *key;
	key = [NSString stringWithFormat: @"ws%@-%@-%d", origin,destin,count++];

	PajeStartLink *sl;
	sl = [[PajeStartLink alloc] init];
	[sl setTime: time];
	[sl setContainer: [provider identifierForEntity: [NSArray
arrayWithObjects: @"0", nil] ids: [NSArray arrayWithObjects: @"0", nil]]]; 
	[sl setSourceContainer: sourceContainer];
	[sl setEntityType: [provider aliasToName:@"Network"]];
	[sl setValue: [provider aliasToName: @"send"]];
	[sl setKey: key];
	[ret addObject: sl];
	[sl release];


	PajeEndLink *el;
	el = [[PajeEndLink alloc] init];
	[el setTime: time];
	[el setContainer: [provider identifierForEntity: [NSArray arrayWithObjects: @"0", nil] ids: [NSArray arrayWithObjects: @"0", nil]]];
	[el setDestContainer: destContainer];
	[el setEntityType: [provider aliasToName:@"Network"]];
	[el setValue: [provider aliasToName: @"send"]];
	[el setKey: key];

	if ([mappingGidHostname objectForKey: destin] != nil){
		[ret addObject: el];
	}else{
		NSMutableArray *a = [workStealingLinks objectForKey: destin];
		if (a == nil){
			a = [[NSMutableArray alloc] init];
			[workStealingLinks setObject: a forKey: destin];
		}
		[a addObject: el];
	}
	[el release];
#endif
#endif

	[ret autorelease];
	return ret;

	


/*	


	PajeEndLink *el;
	el = [[PajeEndLink alloc] init];
	[el setTime: time];
	[el setContainer: [provider identifierForEntity: [NSArray
arrayWithObjects: @"0", nil] ids: [NSArray arrayWithObjects: @"0", nil]]];
	[el setDestContainer: container];
	[el setEntityType: [provider aliasToName:@"Network"]];
	[el setValue: [provider aliasToName: @"send"]];


	[ret autorelease];
        return ret;
*/
}

- (id) convertEventWS: (GEvent *) event
{
	NSMutableArray *ret;
	ret = [[NSMutableArray alloc] init];



	switch (atoi ([eventid cString])){
#ifdef JEITO_ANTIGO_WS
/*
		case Event::WS_CLOSURE_SEND_DATA0:
			[ret addObjectsFromArray: [self convertEventWSSend: event]];
			break;


		case Event::WS_CLOSURE_RECV_DATA1:
			[ret addObjectsFromArray: [self convertEventWSRecv: event]];
			break;
*/
#else
		case Event::WS_CLOSURE_RECV_DATA1:
			[ret addObjectsFromArray: [self convertEventWSRecv: event]];
			break;


#endif
/*
		case Event::NET_IODAEMON_STEAL0:
			[ret addObject: [self addPajeSetState: @"STEAL"
toState: @"State" time: time container: container]];
			break;
		case Event::NET_IODAEMON_STEAL1:
			[ret addObject: [self addPajeSetState: @"RUNNING"
toState: @"State" time: time container: container]];
			break;
*/
		default:
//			NSLog (@"%s: Warning, event type %@ not converted", __FUNCTION__, eventid);
			break;
	}


	[ret autorelease];
	return ret;
}
@end
