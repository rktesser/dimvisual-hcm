/*
    DIMVisual-KAAPI, the DIMVisual bundle for KAAPI trace files
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual-KAAPI.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "KAAPIParser.h"
#include <GenericEvent/GEvent.h>

@implementation KAAPIParser (Network)
- (GEvent *) msgRecv0: (Util::RecordReader::const_iterator) r
{
	GEvent *ev;
	GName *name;
	GFields *fields;
//	EVT_NET_MSG_RECV0_Record *e;

	ev = [[GEvent alloc] init];
	name = [[GName alloc] init]; 
	fields = [[GFields alloc] init];
//	e = (EVT_NET_MSG_RECV0_Record *)r;

//	[fields setFieldWithStringKey: @"src" withValue:
//		[NSString stringWithFormat: @"%d", (unsigned int)e->src]];
//	[fields setFieldWithStringKey: @"serial" withValue:
//		[NSString stringWithFormat: @"%d", (unsigned int)e->serial]];
//	[fields setFieldWithStringKey: @"size" withValue:
//		[NSString stringWithFormat: @"%d", (unsigned int)e->size]];
//	[fields setFieldWithStringKey: @"cntmsg" withValue:
//		[NSString stringWithFormat: @"%d", (unsigned int)e->cntmsg]];

	[name setName: @"EVT_NET_MSG_RECV0"];
	[ev setName: name];	
	[ev setFields: fields];
	
	[name release];
	[fields release];

	[ev autorelease];
	return ev;
}

- (GEvent *) msgRecv1: (Util::RecordReader::const_iterator) r
{
	GEvent *ev;
	GName *name;
	GFields *fields;
//	EVT_NET_MSG_RECV1_Record *e;

	ev = [[GEvent alloc] init];
	name = [[GName alloc] init]; 
	fields = [[GFields alloc] init];
//	e = (EVT_NET_MSG_RECV1_Record *)r;

//	[fields setFieldWithStringKey: @"src" withValue:
//		[NSString stringWithFormat: @"%d", (unsigned int)e->src]];
//	[fields setFieldWithStringKey: @"serial" withValue:
//		[NSString stringWithFormat: @"%d", (unsigned int)e->serial]];

	[name setName: @"EVT_NET_MSG_RECV1"];
	[ev setName: name];	
	[ev setFields: fields];
	
	[name release];
	[fields release];

	[ev autorelease];
	return ev;
}

- (GEvent *) msgSend0: (Util::RecordReader::const_iterator) r
{
	GEvent *ev;
	GName *name;
	GFields *fields;
//	EVT_NET_MSG_SEND0_Record *e;

	ev = [[GEvent alloc] init];
	name = [[GName alloc] init]; 
	fields = [[GFields alloc] init];
//	e = (EVT_NET_MSG_SEND0_Record *)r;

//	[fields setFieldWithStringKey: @"dest" withValue:
//		[NSString stringWithFormat: @"%d", (unsigned int)e->dest]];
//	[fields setFieldWithStringKey: @"serial" withValue:
//		[NSString stringWithFormat: @"%d", (unsigned int)e->serial]];
//	[fields setFieldWithStringKey: @"size" withValue:
//		[NSString stringWithFormat: @"%d", (unsigned int)e->size]];
//	[fields setFieldWithStringKey: @"cntmsg" withValue:
//		[NSString stringWithFormat: @"%d", (unsigned int)e->cntmsg]];

	[name setName: @"EVT_NET_MSG_SEND0"];
	[ev setName: name];	
	[ev setFields: fields];
	
	[name release];
	[fields release];

	[ev autorelease];
	return ev;
}

- (GEvent *) msgSend1: (Util::RecordReader::const_iterator) r
{
	GEvent *ev;
	GName *name;
	GFields *fields;
//	EVT_NET_MSG_SEND1_Record *e;

	ev = [[GEvent alloc] init];
	name = [[GName alloc] init]; 
	fields = [[GFields alloc] init];
//	e = (EVT_NET_MSG_SEND1_Record *)r;

//	[fields setFieldWithStringKey: @"dest" withValue:
//		[NSString stringWithFormat: @"%d", (unsigned int)e->dest]];
//	[fields setFieldWithStringKey: @"serial" withValue:
//		[NSString stringWithFormat: @"%d", (unsigned int)e->serial]];

	[name setName: @"EVT_NET_MSG_SEND1"];
	[ev setName: name];	
	[ev setFields: fields];
	
	[name release];
	[fields release];

	[ev autorelease];
	return ev;
}

- (GEvent *) ioDaemonSteal0: (Util::RecordReader::const_iterator) r
{
	GEvent *ev;
	GName *name;
	GFields *fields;
//	EVT_NET_IODAEMON_STEAL0_Record *e;

	ev = [[GEvent alloc] init];
	name = [[GName alloc] init]; 
	fields = [[GFields alloc] init];
//	e = (EVT_NET_IODAEMON_STEAL0_Record *)r;

	[name setName: @"EVT_NET_IODAEMON_STEAL0"];
	[ev setName: name];	
	[ev setFields: fields];
	
	[name release];
	[fields release];

	[ev autorelease];
	return ev;
}

- (GEvent *) ioDaemonSteal1: (Util::RecordReader::const_iterator) r
{
	GEvent *ev;
	GName *name;
	GFields *fields;
//	EVT_NET_IODAEMON_STEAL1_Record *e;

	ev = [[GEvent alloc] init];
	name = [[GName alloc] init]; 
	fields = [[GFields alloc] init];
//	e = (EVT_NET_IODAEMON_STEAL1_Record *)r;

	[name setName: @"EVT_NET_IODAEMON_STEAL1"];
	[ev setName: name];	
	[ev setFields: fields];
	
	[name release];
	[fields release];

	[ev autorelease];
	return ev;
}

- (GEvent *) parseKAAPINetworkLevel: (Util::RecordReader::const_iterator) r
	withIdentifier: (GIdentifier *) identifier
	withTimestamp: (GTimestamp *) timestamp
{
	GEvent *ev = nil;
        int event;
        event = (int)*(char *)r->structure->get_field_data("_event",r->buffer);

	switch (event){
		case Event::NET_MSG_SEND0:
			ev = [self msgSend0: r];
			break;
		case Event::NET_MSG_SEND1:
			ev = [self msgSend1: r];
			break;
		case Event::NET_MSG_RECV0:
			ev = [self msgRecv0: r];
			break;
		case Event::NET_MSG_RECV1:
			ev = [self msgRecv1: r];
			break;
		case Event::NET_IODAEMON_STEAL0:
			ev = [self ioDaemonSteal0: r];
			break;
		case Event::NET_IODAEMON_STEAL1:
			ev = [self ioDaemonSteal1: r];
			break;
		case Event::NET_MSG_NOTIFY:
		case Event::NET_MSG_POST:
		case Event::NET_MSG_START:
		case Event::NET_IODAEMON_DECODE0:
		case Event::NET_IODAEMON_DECODE1:
		case Event::NET_SERVICE_CALL0:
		case Event::NET_SERVICE_CALL1:
			break;
		default:
			NSLog (@"Warning: %s -> type = %d, not translated", __FUNCTION__, event);
			break;
	}
	if (ev != nil){
		[ev setTimestamp: timestamp];
		[ev setIdentifier: identifier];
	}
	return ev; //already autoreleased
}

@end
