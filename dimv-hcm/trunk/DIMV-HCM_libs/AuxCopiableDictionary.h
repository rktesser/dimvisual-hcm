#import <Foundation/NSDictionary.h>
#import <Foundation/NSEnumerator.h>
#import <Foundation/NSCoder.h>
#import <Foundation/NSPortCoder.h>


@interface AuxCopiableDictionary: NSMutableDictionary <NSCoding>
{
	NSMutableDictionary * embeddedDictionary;
}

- (void) encodeWithCoder: (NSCoder *)encoder;
- (id) initWithCoder: (NSCoder *)decoder;
- (id) replacementObjectForPortCoder: (NSPortCoder *)encoder;
- (void) setObject: (id)anObject forKey: (id)aKey;
- (void) removeObjectForKey: (id)aKey;
- (id) initWithCapacity: (unsigned)numItems;
- (unsigned) count;
- (id) initWithObjects: (id*)objects forKeys: (id*)keys count: (unsigned)count;
- (NSEnumerator*) keyEnumerator;
- (NSEnumerator*) objectEnumerator;
- (id) objectForKey: (id)aKey;
- (void) dealloc;

@end

