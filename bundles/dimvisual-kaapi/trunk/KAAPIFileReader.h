/*
    DIMVisual-KAAPI, the DIMVisual bundle for KAAPI trace files
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual-KAAPI.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __KAAPIFILEREADER__
#define __KAAPIFILEREADER__
#include <Foundation/Foundation.h>
#include <DIMVisual/Protocols.h>
#include <DIMVisual/Sync.h>
#include <GenericEvent/GEvent.h>
#include <GenericEvent/GEvent.h>
#include "KAAPIParser.h"

@class KAAPIDataSource;

@interface KAAPIFileReader : KAAPIParser <FileReader>
{
	GEvent *topEvent;
	NSMutableDictionary *syncs;

	double ticksPerSecond;
	double timeOfDayEV;
	double tickEV;

	NSString *hostname;
	NSMutableArray *buf;
	Sync *sync;

	KAAPIDataSource *provider;

	NSString *traceFilename;

	NSMutableDictionary *threadNames;
	NSMutableArray *waitingListThreadName;

	//for Read category
	std::map<Util::RecordKey,Util::RecordDesc*> map;
	Util::RecordReader *input;
	Util::RecordReader::const_iterator ibeg;
	Util::RecordReader::const_iterator iend;

	double simpleTime;
}
//- (id) initWithFileName: (NSString *) traceFile
//        andSyncFileName: (NSString *) syncFile;
- (id) initWithFileName: (NSString *) traceFile
	andSyncFileName: (NSString *) syncFile
	andProvider: (KAAPIDataSource *) prov;
- (NSString *) filename;
@end

@interface KAAPIFileReader (Search)
- (void) searchTicksPerSecond;
- (void) searchHostnameAndDefineSync;
- (void) defineTimestampForEvent: (GEvent *) e;
- (void) defineHostnameForEvent: (GEvent *) e;
- (void) synchronizeEvent: (GEvent *) e;
- (void) consumeThreadNameEventsFromBuffer;
@end

@interface KAAPIFileReader (Read)
- (BOOL) initializeWithTraceFile: (NSString *) traceFile
                andDescriptionFile: (NSString *) descFile;
- (GEvent *) read;
@end

#include "KAAPIDataSource.h"
#endif
