/*
    DIMVisual-KAAPI, the DIMVisual bundle for KAAPI trace files
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual-KAAPI.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __KAAPIRECORDREADEROBJC__
#define __KAAPIRECORDREADEROBJC__
#include <Foundation/Foundation.h>
#include <iostream>
#include <kaapi>
#include <utils_init.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>


@interface KAAPIRecordReaderObjc : NSObject
{
//	std::list <Util::Record> records;
//	std::list <Util::RecordDesc> recordDescs;
	Util::ka_uint8_t* _start;
	size_t _size;
	Util::ka_uint8_t* ipos;	
	Util::ka_uint8_t* iend;
}
- (id) initWithFile: (const std::string&) filename;
- (Util::Record*) read;
@end
#endif
