/*
   Copyright (c) 2005 Lucas Mello Schnorr <schnorr@gmail.com>
   
   This file is part of DIMVisual.
   
   DIMVisual is free software; you can redistribute it and/or modify it under
   the terms of the GNU Lesser General Public License as published by the
   Free Software Foundation; either version 2 of the License, or (at your
   option) any later version.
   
   DIMVisual is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
   for more details.
   
   You should have received a copy of the GNU Lesser General Public License
   along with DIMVisual; if not, write to the Free Software Foundation, Inc.,
   59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/
#include "MPIRASTRODataSource.h"

@implementation MPIRASTRODataSource
- (id) initWithProvider: (id<Integrator>) prov
{
	self = [super init];
	provider = prov;
	[provider retain];
	if (provider == nil){
		NSLog (@"MPIRASTRODataSource: provider '%@' (Error)", provider);
		[self release];
		return nil;
	}
	return self;
}

- (void) resetConfiguration
{
	if (reader){
		[reader release];
	}
	if (converter){
		[converter release];
	}
}

- (BOOL) setConfiguration: (NSDictionary *) configuration
{
	self = [super init];

	/* Extract information from configuration providerided */
	id files;
	id nodes; 
	id param;
	id ident;
	id syncs;

	if (configuration == nil){
		NSLog (@"MPIRASTRODataSource: configuration '%@'", configuration);
		[self release];
		return NO;
	}
	if (provider == nil){
		NSLog (@"MPIRASTRODataSource: provider '%@'", provider);
		[self release];
		return NO;
	}
	ident = [configuration objectForKey: @"ID"];
	if (ident == nil || [ident isEqual: @"MPIRASTRO"] == NO){
		NSLog (@"MPIRASTRODataSource: configuration '%@' hasn't a field "
			"'id' or is different from 'MPIRASTRO'", configuration);
		[self release];
		return NO;
	}
	param = [configuration objectForKey: @"parameters"];
	if (param == nil || [param isKindOfClass: [NSDictionary class]] == NO){
		NSLog (@"MPIRASTRODataSource: configuration '%@' hasn't a field "
			"'parameters' or is not a NSArray", configuration);
		[self release];
		return NO;
	}
	files = [param objectForKey: (id)@"files"];
	if (files == nil || [files isKindOfClass: [NSArray class]] == NO){
		NSLog (@"MPIRASTRODataSource: configuration '%@' hasn't a field "
			"'files' or is not a NSArray", configuration);
		[self release];
		return NO;
	}
	/*
	nodes = [param objectForKey: @"nodes"];
	if (nodes == nil || [nodes isKindOfClass: [NSString class]] == NO){
		NSLog (@"MPIRASTRODataSource: configuration '%@' hasn't a field "
			"'nodes' or is not a NSString", configuration);
		[self release];
		return NO;
	}
	*/
	syncs = [param objectForKey: @"sync"];
	if (syncs == nil || [syncs isKindOfClass: [NSArray class]] == NO){
		NSLog (@"MPIRASTRODataSource: warning, configuration '%@' hasn't a field "
			"'sync' or is not a NSString", configuration);
	}

	//NSLog (@"%@ %@ %@", [files class], [nodes class], [syncs class]);
	//NSLog (@"%@ %@ %@", files, nodes, syncs);
	
	/* Instantiate the reader and the converter */
//	reader = [[MPIRASTROFileReader alloc] initWithFileName: files
//			andNodesFile: nodes andSyncFileName: syncs];
	reader = [[MPIRASTROFileReader alloc] initWithMultipleFiles: files
			andSyncFileName: [syncs objectAtIndex: 0]];
	if (reader == nil){
		NSLog (@"MPIRASTRODataSource: couldn't create a MPIRASTROFileReader with "
			"files='%@', nodes='%@' and sync='%@'",
			files, nodes, syncs);
		return NO;
	}

	converter = [[MPIRASTROConverter alloc] initWithProvider: provider];
	if (converter == nil){
		NSLog (@"MPIRASTRODataSource: couldn't create a MPIRASTROConverter with "
			"provider='%@'", provider);
		[reader release];
		return NO;
	}
	return YES;
}

- (NSDictionary *) hierarchy
{
	return [NSDictionary dictionaryWithContentsOfFile: [[NSBundle bundleForClass: [self class]] pathForResource: @"MPIRASTROHierarchy" ofType: @"plist"]];
}

- (GTimestamp *) time;
{
	return [reader time];
}

- (double) simpleTime
{
	return [[reader mostRecent] simpleTime];
}

- (NSComparisonResult) compareTime: (id<Time>) otherObject
{
        double time = [otherObject simpleTime];
        double simpleTime = [self simpleTime];
        if (simpleTime < time){
                return NSOrderedAscending;
        }else if (simpleTime > time){
                return NSOrderedDescending;
        }else{
                return NSOrderedSame;
        }
}

- (id) convert
{
	id event;
	id ret = nil;

	event = [reader event];
	if (event == nil){
		return nil;
	}
	ret = [converter convertEvent: event];
	return ret; 
}

- (void) dealloc
{
	[reader release];
	[converter release];
	[super dealloc];
}

- (NSDictionary *) configuration
{
	NSMutableDictionary *c;
	NSMutableDictionary *parameters;
//	NSArray *entities;

	parameters = [[NSMutableDictionary alloc] init];
	
	[parameters setObject: @"multiple" forKey: @"files"];
//	[parameters setObject: @"single" forKey: @"nodes"];
	[parameters setObject: @"single" forKey: @"sync"];
		
	c = [[NSMutableDictionary alloc] init];
	[c setObject: @"MPIRASTRO" forKey: @"ID"];
	[c setObject: parameters forKey: @"parameters"];
	[parameters release];
//	NSLog (@"OI");
//	entities = [converter entities];
//	[entities retain];
//	NSLog (@"%@", entities);
//	[c setObject: entities forKey: @"entities"];
//	[entities release];
	[c autorelease];
	return c;
}
@end
