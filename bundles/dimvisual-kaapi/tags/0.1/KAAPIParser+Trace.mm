/*
    DIMVisual-KAAPI, the DIMVisual bundle for KAAPI trace files
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual-KAAPI.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "KAAPIParser.h"

@implementation KAAPIParser (Trace)
- (GEvent *) bufferAllocate: (Util::Record *) r
{
        GEvent *ev;
        GName *name;
        GFields *fields;
        EVT_TR_BUFFER_ALLOCATE_Record *revent;

        ev = [[GEvent alloc] init];
        name = [[GName alloc] init];
        fields = [[GFields alloc] init];
        revent = (EVT_TR_BUFFER_ALLOCATE_Record *)r;

        [fields setFieldWithStringKey: @"date" withValue: [NSString stringWithFormat: @"%lli", (unsigned long long)revent->date]];

	[name setName: @"EVT_TR_BUFFER_ALLOCATE"];
        [ev setName: name];
        [ev setFields: fields];

        [name release];
        [fields release];

        [ev autorelease];
        return ev;
}

- (GEvent *) bufferFlush: (Util::Record *) r
{
        GEvent *ev;
        GName *name;
        GFields *fields;
        EVT_TR_BUFFER_FLUSH_Record *revent;

        ev = [[GEvent alloc] init];
        name = [[GName alloc] init];
        fields = [[GFields alloc] init];
        revent = (EVT_TR_BUFFER_FLUSH_Record *)r;

        [fields setFieldWithStringKey: @"date" withValue: [NSString stringWithFormat: @"%lli", (unsigned long long)revent->date]];

        [name setName: @"EVT_TR_BUFFER_FLUSH"];
        [ev setName: name];
        [ev setFields: fields];

        [name release];
        [fields release];

        [ev autorelease];
        return ev;

}


- (GEvent *) parseKAAPITraceLevel: (Util::Record *) r 
	withIdentifier: (GIdentifier *) identifier
	withTimestamp: (GTimestamp *) timestamp
{
	GEvent *ev = nil;

	switch (r->_event){
		case Event::TR_BUFFER_ALLOCATE:
			ev = [self bufferAllocate: r];
			break;

		case Event::TR_BUFFER_FLUSH:
			ev = [self bufferFlush: r];
			break;

		default:
			NSLog (@"Warning: Trace_Level -> type = %d, not completely translated", r->_event);
			break;
	}
	if (ev != nil){
		[ev setTimestamp: timestamp];
		[ev setIdentifier: identifier];
	}
	return ev;
}
@end

