/*
   Copyright (c) 2005 Lucas Mello Schnorr <schnorr@gmail.com>
   
   This file is part of DIMVisual.
   
   DIMVisual is free software; you can redistribute it and/or modify it under
   the terms of the GNU Lesser General Public License as published by the
   Free Software Foundation; either version 2 of the License, or (at your
   option) any later version.
   
   DIMVisual is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
   for more details.
   
   You should have received a copy of the GNU Lesser General Public License
   along with DIMVisual; if not, write to the Free Software Foundation, Inc.,
   59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/
#include "MPIRASTROFileReader.h"

@implementation MPIRASTROFileReader
- (id) initWithFileName: (NSString *) traceFile
	andSyncFileName: (NSString *) syncFile
{
	return nil;
}

- (id) initWithMultipleFiles: (NSArray *) traceFiles
	andSyncFileName: (NSString *) syncFile
{
	int i;

	self = [super init];
	hostnames = [[NSMutableDictionary alloc] init];

	
	if (traceFiles == nil){
		NSLog (@"MPIRASTROFileReader: filenames should be provided");
		return nil;
	}
	if (syncFile == nil){
		syncFile = [NSString stringWithFormat: @"nofile"];
	}else{
//		NSLog (@"MPIRASTROFileReader: sync is '%@'", syncFile);
	}
//	NSLog (@"MPIRASTROFileReader starting (files=%@) (sync=%@)", files,syncFile);
	filenames = traceFiles;
	[filenames retain];

	/* opening many libRastro trace files */
	for (i = 0; i < [filenames count]; i++){
		char *file = (char *)[[filenames objectAtIndex: i] cString];
		char *sync = (char *)[syncFile cString];
		if (rst_open_file (file, &data, sync, 100000) == -1){
			NSLog (@"MPIRASTROFileReader: error opening '%@'",
					[filenames objectAtIndex: i]);
			[filenames release];
			[hostnames release];
			return nil;
		}
	}

	/* decoding first event */
	if (rst_decode_event (&data, &event) == RST_NOK){
		[hostnames release];
		[filenames release];
		return nil;
	}else{
		int i;
		for (i = 0; i < data.quantity; i++){
			rst_one_file_t *x;
			x = data.of_data[i];
			if (x->id1 == event.id1 && x->id2 == event.id2){
				[hostnames setObject: [NSString stringWithCString: x->hostname]	forKey:	[NSString stringWithFormat: @"%d", x->id1]];
			}
		}
		/*
		if (event.type == MPIRASTRO_INIT_NODE || event.type == MPIRASTRO_THREAD_CREATED){
			NSString *key, *name;
			name = [[NSString alloc] initWithFormat:
				@"%s",event.v_string[0]];
			key = [[NSString alloc] initWithFormat:
				@"%d",event.v_uint32[0]];
			[hostnames setObject: name forKey: key];
			[name release];
			[key release];
		}
		*/
	}
	/* creating MPIRASTROEvent object */
	mpilibrastroevent= [[MPIRASTROEvent alloc] initWithEvent: &event withProvider: self];
//	NSLog (@"%@", [mpilibrastroevent machine]);
	return self;
}

/* next method is used by MPIRASTROEvent invocation */
- (NSString *) machineForNode: (int) node
{
//	int i;

	if ([hostnames count] == 0){
		return nil;
	}else{
		return [hostnames objectForKey:[NSString stringWithFormat:@"%d",node]];
	}
}

- (NSString *) time 
{
	return [mpilibrastroevent time];
}

- (void) dealloc
{
	[filenames release];
	[mpilibrastroevent release];
	[hostnames release];
	[super dealloc];
}

- (id<Event>) event
{
	MPIRASTROEvent *ret;

	if (mpilibrastroevent == nil){
		return nil;
	}
	ret = mpilibrastroevent;
	if (rst_decode_event (&data, &event) == RST_NOK){
		mpilibrastroevent = nil;
		[ret autorelease];
		return ret;
	}else{
		int i;
		for (i = 0; i < data.quantity; i++){
			rst_one_file_t *x;
			x = data.of_data[i];
			if (x->id1 == event.id1 && x->id2 == event.id2){
				[hostnames setObject: [NSString stringWithCString: x->hostname]	forKey:	[NSString stringWithFormat: @"%d", x->id1]];
			}
		}
//		NSLog (@"Event of type %d read", event.type);
//		fprintf (stderr, "%llu %d\n", event.timestamp, event.type);
	}

/*	if (event.type == MPIRASTRO_INIT_NODE || event.type == MPIRASTRO_THREAD_CREATED){
		NSString *key, *name;
		name = [[NSString alloc] initWithFormat:
			@"%s",event.v_string[0]];
		key = [[NSString alloc] initWithFormat:
			@"%d",event.v_uint32[0]];
		[hostnames setObject: name forKey: key];
		[name release];
		[key release];
	}
*/	
	mpilibrastroevent = [[MPIRASTROEvent alloc] initWithEvent: &event withProvider: self];
	//NSLog (@"MPIRASTRO %@", [mpilibrastroevent time]);
	[ret autorelease];
	return ret;
}
@end
