/*
    DIMVisual-KAAPI, the DIMVisual bundle for KAAPI trace files
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual-KAAPI.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __KAAPIPARSER__
#define __KAAPIPARSER__
#include <Foundation/Foundation.h>
#include <GenericEvent/GEvent.h>
#include <kaapi>

@interface KAAPIParser : NSObject
{
}
- (GEvent *) parseKAAPIEvent: (Util::RecordReader::const_iterator) r;
- (GEvent *) generic: (Util::RecordReader::const_iterator) r;
@end

@interface KAAPIParser (Util)
- (GEvent *) parseKAAPIUtilLevel: (Util::RecordReader::const_iterator) r withIdentifier: (GIdentifier *) identifier withTimestamp: (GTimestamp *) timestamp;
@end

@interface KAAPIParser (Trace)
- (GEvent *) parseKAAPITraceLevel: (Util::RecordReader::const_iterator) r withIdentifier: (GIdentifier *) identifier withTimestamp: (GTimestamp *) timestamp;
@end

@interface KAAPIParser (Kernel)
- (GEvent *) parseKAAPIKernelLevel: (Util::RecordReader::const_iterator) r withIdentifier: (GIdentifier *) identifier withTimestamp: (GTimestamp *) timestamp;
@end

@interface KAAPIParser (Network)
- (GEvent *) parseKAAPINetworkLevel: (Util::RecordReader::const_iterator) r withIdentifier: (GIdentifier *) identifier withTimestamp: (GTimestamp *) timestamp;
@end

@interface KAAPIParser (WS)
- (GEvent *) parseKAAPIWsLevel: (Util::RecordReader::const_iterator) r withIdentifier: (GIdentifier *) identifier withTimestamp: (GTimestamp *) timestamp;
@end

@interface KAAPIParser (RFO)
- (GEvent *) parseKAAPIRfoLevel: (Util::RecordReader::const_iterator) r withIdentifier: (GIdentifier *) identifier withTimestamp: (GTimestamp *) timestamp;
@end

#endif
