#ifndef DIMVAGGREGATOR_H
#define DIMVAGGREGATOR_H

#import <Foundation/NSDictionary.h>
#import <Foundation/NSString.h>
#import <Foundation/NSArray.h>
#import <Foundation/NSConnection.h>
#import <Foundation/NSNotification.h>
#import <Foundation/NSPortNameServer.h>
#import <Foundation/NSPort.h>
#import <Foundation/NSRunLoop.h>
#import <Foundation/NSEnumerator.h>
#import <Foundation/NSThread.h>
#import <Foundation/NSDistantObject.h>

#ifdef DIMV_EVALUATE
#import <sys/time.h>
#import <Foundation/NSFileHandle.h>
#import <Foundation/NSFileManager.h>
#endif

#import "Observer.h"
#import "CollectorRegister.h"
#import "DIMVAggregatorProto.h"
#import "DIMVCollectorProto.h"
#import "DIMVClientProto.h"
#import "AuxCopiableDictionary.h"
#import "DIMVConstants.h"

@interface DIMVAggregator: NSObject <DIMVAggregator>
{
	NSString *aggregatorId;
	NSString *name;
	AuxCopiableDictionary *registeredCollectors;
	NSMutableDictionary *subscribedCollectors;
	NSMutableDictionary *superiors; 
	NSMutableDictionary *subordinates;
	NSMutableDictionary *clients;
	NSMutableDictionary *collectors;
	#ifdef DIMV_EVALUATE
	NSFileHandle * timeReceiveDataFH;
/*	NSFileHandle * durationSendFH;*/
	#endif
}


/*This method initializes an aggregator. It's arguments are an unique 
 * identificator for the aggregator, an unique name and an array containing 
 * the names of its superior aggregators with which it must connect and 
 * register itself.*/
- (DIMVAggregator *) initWithId: (NSString *)aggregId andName: (NSString *)aggregName andSuperiors: (NSMutableArray *)supNames;

- (void) dealloc;

/*Method called internally by an aggregator in order to register itself with
 * the name sever. This is needed in order to make it possible to other remote
 * components to locate the aggregator.*/
- (BOOL) registerWithNameServer;

/*This method is called internally and connects the aggregator to another one
 * whose name its registered in the name server is provided as argument for 
 * the method. The return values is a reference to a proxy to the remote
 * aggregator.*/
- (id<DIMVAggregator>) connectWithAggregator: (NSString *)name;

/*This method iscalled by a lower level component. It forwards the data
 * referenced by "data" to the proper higher level components.*/
#ifndef DIMV_EVALUATE
- (void) receiveData: (id)data fromCollector: (NSString *)collectorId;
#else
- (void) receiveData: (id)data fromCollector: (NSString *)collectorId 
   counter: (unsigned)counter;
#endif

- (void) subscribe: (DIMVAggregator *)subscriber withCollector: (NSString *)collectorId;
- (void) unsubscribe: (NSString *)subscriberId withCollector: (NSString *)collectorId;

//Method called by a collector in order to register itself
- (void) registerCollector: (id<DIMVCollector>)collector;

- (void) registerCollectorWithId: (NSString *)colReg type: (NSString *)colType location: (NSString *)colLocation origin: (NSString *)originId;

- (void) unregisterCollector: (NSString *)collectorId withOrigin: (NSString *)AggregatorId;

/*This method receives a proxy to a remote client in order to rregister it
 * with the aggregator.*/
- (void) registerClient: (id) client withId: (NSString *) clientId;
- (void) unregisterClient: (NSString *) clientId;

- (NSString *) getId;
- (NSString *) getName;

/*This method is called by an aggregator at lower hierarchy level to register
 * itself as an subordinate aggregator.*/
- (BOOL) registerSubordinate: (id)subordinate withId: (NSString *)aggregatorId;

/*This method is called by the client in order to discover the available 
 *collectors. Its return value is a reference to an dictionary containing the
 registers of the collector accessible through the aggregator. The ckeys of
 the dictionary are the collectors' unique identificators.*/
- (AuxCopiableDictionary *) getRegisteredCollectors;

- (void)forwardRegisteredCollectorsToAggregator: (id<DIMVAggregator>)aComponent;

@end

#endif

