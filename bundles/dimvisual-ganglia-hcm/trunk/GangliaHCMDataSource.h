/*
    DIMVisual-Ganglia, the DIMVisual bundle for Ganglia trace files
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual-Ganglia.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#import <Foundation/NSDictionary.h>
#import <Foundation/NSLock.h>
#import <Foundation/NSThread.h>
#import <DIMVisual/Protocols.h>
#import <DIMVisual/Order.h>
#import <GenericEvent/GEvent.h>
#import "GangliaHCMConverter.h"
#import "GangliaHCMFileReader.h"
#import <DIMV-HCM/DIMVClient.h>
#import <DIMVisual/BinaryMinHeap.h>
#import <float.h>

#define IDKEY @"ID"
#define IDVALUE @"Ganglia"
#define PARAMETERKEY @"parameters"
#define FILEKEY @"files"
#define SYNCKEY @"sync"
#define MACHINEKEY @"machine"


#define DATAQUEUEINITIALCAPACITY 14
#define CONDQUEUEHASDATA 1
#define CONDQUEUEEMPTY 0
#define GANGLIADSTYPE @"ganglia"

@class GangliaHCMConverter;
@class GangliaHCMFileReader;

@interface GangliaHCMDataSource : NSObject <DataSource, HCMDataSource>
{
	BinaryMinHeap *reader; /* of GangliaHCMFileReader */
	NSRecursiveLock *readerLock; //Recursive because of the convert method.
	GangliaHCMConverter *converter;
	id<Integrator> prov;
	NSMutableArray *dataQueue;
	NSConditionLock *dataQueueLock;
	GTimestamp *eldest;
	Order *order; 
}
- (void)setOrder: (Order *)anOrder;

@end

