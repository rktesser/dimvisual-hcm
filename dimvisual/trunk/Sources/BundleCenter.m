/*
    DIMVisual, the Data Integration Model for Visualization of trace files.
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "BundleCenter.h"

@implementation BundleCenter
/*
 *
 */
- (NSDictionary *) configurationForBundleWithName: (NSString *) name
{
	NSBundle *bundle = [bundlesLoaded objectForKey: name];
	id ds = [[[bundle principalClass] alloc] init];

	NSDictionary *ret = [ds configuration];
	[ret retain];
	[ds release];
	[ret autorelease];
	return ret;
}

/*
 * tryToLoadBundle
 */
- (BOOL) tryToLoadBundle: (NSBundle *) bundle
{
	NSLog(@"BundleCenter: trying to load a bundle.\n");
	if ([bundle load]){
		Class x = [bundle principalClass];
		NSLog(@"PrincipalClass: %@\n", NSStringFromClass(x));
		if (x == nil){
			NSLog(@"Bundlecenter: could'n get the principal class of the bundle.\n");
			return NO;
		}
		if ([x conformsToProtocol: @protocol(DataSource)] == NO){
			NSLog(@"Bundlecenter: the bundle doesn't conform to the DataSource protocol.\n");
			return NO;	
		}
			NSLog(@"Bundlecenter: the bundle has been loaded.\n");
		return YES;
	}else{ 
		NSLog(@"Bundlecenter: the loading of the  bundle has failed.\n");
		return NO;
	}
}

/*
 * defaultLocations
 */
- (void) defaultLocations
{
	int i;

	NSMutableArray *paths = (NSMutableArray *)NSStandardLibraryPaths();
	for (i = 0; i < [paths count]; i++){
		NSString *dir;
		BOOL is_Dir;	

		dir = [paths objectAtIndex: i];
		dir = [dir stringByAppendingPathComponent: @"Bundles"];
		dir = [dir stringByAppendingPathComponent: @"DIMVisual"];
		if ([[NSFileManager defaultManager] 
			fileExistsAtPath: dir isDirectory: &is_Dir]){
			[bundlePaths addObject: dir];
		}
	}
}

/*
 * addBundleDirectory
 */
- (void) addBundleDirectory: (NSString *) newPath
{
	[bundlePaths addObject: newPath];
}

/*
 * listBundlesAvailable
 */
- (NSArray *) listBundlesAvailable
{
	int i,j;
	NSMutableArray *ret = [[NSMutableArray alloc] init];

	for (i = 0; i < [bundlePaths count]; i++){

		NSArray *subpaths = [[NSFileManager defaultManager]
				subpathsAtPath: [bundlePaths objectAtIndex: i]];
		for (j = 0; j < [subpaths count]; j++){
			NSString *pna, *s1, *s2;
			pna = [subpaths objectAtIndex: j];
			NSArray *ar1 = [pna componentsSeparatedByString:@"."];
			if ([[ar1 lastObject] isEqual: @"bundle"]){
				s1 = [bundlePaths objectAtIndex: i];
				s2 =[s1 stringByAppendingPathComponent: pna];
				NSBundle *bundle;
				bundle = [NSBundle bundleWithPath: s2];
				if ([self tryToLoadBundle: bundle]){
					[bundlesLoaded setObject: bundle
							forKey: pna];
					[ret addObject: pna];
				}
			}
		}
	}
	[ret autorelease];
	return ret;
}

/*
 *
 */
- (NSString *) pathWithBundleName: (NSString *) name
{
	int i;
	NSLog(@"BundleCenter: trying to find path for bundle with name \'%@\'.\n", name);
	for (i = 0; i < [bundlePaths count]; i++){
		NSString *thispath = [bundlePaths objectAtIndex: i];
		NSString *s1 = [thispath stringByAppendingPathComponent: name];
		NSLog(@"Testing \'%@\'.\n", s1);
		if ([[NSFileManager defaultManager] changeCurrentDirectoryPath: s1] == YES) {
		NSLog(@"The path is: '%@'\n", s1);
			return s1;
		}
	}
	NSLog(@"BundleCenter:a bundle with name \'%@\' has not been found.\n" , name);
	return nil;
}

/*
 *
 */
- (BOOL) loadBundleWithName: (NSString *) name
{
	NSLog(@"Loading bundle with name \'%@\'.\n", name);
	NSString *path = [self pathWithBundleName: name];	

	if (path == nil){
		NSLog(@"BundleCenter: '%@' hasn't been found.\n", name);
		return NO;
	}

	NSBundle *bundle = [NSBundle bundleWithPath: path];
	if(bundle ==nil){
		NSLog(@"BundleCenter: couldn't get a bundle with path '%@'.\n", path);
	}
	if ([self tryToLoadBundle: bundle]){
		[bundlesLoaded setObject: bundle forKey: name];
		return YES;
	}else {
		return NO;
	}
}

/*
 * init
 */
- (id) init
{
	self = [super init];
	bundlePaths = [[NSMutableArray alloc] init];
	bundlesLoaded = [[NSMutableDictionary alloc] init];
	[self defaultLocations];
	return self;
}

/*
 * dealloc
 */
- (void) dealloc
{
	[bundlePaths release];
	[bundlesLoaded release];
	[super dealloc];
}

/*
 *
 */
- (NSDictionary *) configureBundle: (NSString *) name withThis: (NSDictionary *) conf
{
	int i;
	NSLog(@"BundleCenter: configuring bundle with name \'%@\'.\n", name);
	NSDictionary *tofill = [self configurationForBundleWithName: name];
	NSString *bundleid = [tofill objectForKey: @"ID"];
	NSMutableDictionary *parameters = [tofill objectForKey: @"parameters"];
	NSArray *allKeys = [parameters allKeys];
	NSMutableArray *allPossibleConfiguration = [[NSMutableArray alloc]init];
	
	for (i = 0; i < [allKeys count]; i++){
		NSString *oc = [allKeys objectAtIndex: i];
		[allPossibleConfiguration addObject: [NSString stringWithFormat: @"-%@-%@", bundleid, oc]];
	}

	for (i = 0; i < [allPossibleConfiguration count]; i++){

		NSArray *x = [conf objectForKey: [allPossibleConfiguration objectAtIndex: i]];
		if (x != nil){
			[parameters setObject: x forKey: [allKeys objectAtIndex: i]];
		}else{
			[parameters setObject: [NSArray array] forKey: [allKeys objectAtIndex: i]];
		}
	}
	/* remove from parameters the entries that were not defined */
	allKeys = [parameters allKeys];
	for (i = 0; i < [allKeys count]; i++){
		NSString *key = [allKeys objectAtIndex: i];
		id object = [parameters objectForKey: key];
		if ([object isKindOfClass: [NSString class]]){
			if ([object isEqualToString: @""]){
				[parameters removeObjectForKey: key];
			}
		}else if ([object isKindOfClass: [NSArray class]]){
			if ([object count] == 0){
				[parameters removeObjectForKey: key];
			}
		}else if ([object isKindOfClass: [NSDictionary class]]){
			if ([[object allKeys] count] == 0){
				[parameters removeObjectForKey: key];
			}
		}else{
			NSLog (@"DIMVisual warning: configuration sent to "
				"bundle might be not correct. This message "
				"comes from %s:%d", __FILE__, __LINE__);
		}
	}
	return tofill;
} 

/*
 *
 */
- (NSString *) principalClassOfBundleNamed: (NSString *) name
{
	Class x = [[bundlesLoaded objectForKey: name] principalClass];
	NSString *ret = NSStringFromClass (x);
	return ret;
}

/*
 *
 */
- (NSDictionary *) configureBundlesWithThis: (NSDictionary *) conf
{
	int i;

	if ([bundlesLoaded count] == 0){
		return nil;
	}

	NSArray *list = [NSArray arrayWithArray: [bundlesLoaded allKeys]];

	NSMutableDictionary *ret = [[NSMutableDictionary alloc] init];
	for (i = 0; i < [list count]; i++){
		[ret setObject: [self configureBundle: [list objectAtIndex: i] withThis: conf] forKey: [self principalClassOfBundleNamed: [list objectAtIndex: i]]];
	}
	[ret autorelease];
	return ret;
}

- (BOOL) isBundleWithNameLoaded: (NSString *) name
{
	if ([bundlesLoaded objectForKey: name] != nil){
		return YES;
	}else{
		return NO;
	}
}

- (NSBundle *) bundleNamed: (NSString *) name
{
	return (NSBundle *)[bundlesLoaded objectForKey: name];
}
@end
