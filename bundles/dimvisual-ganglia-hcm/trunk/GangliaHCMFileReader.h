/*
    DIMVisual-Ganglia, the DIMVisual bundle for Ganglia trace files
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual-Ganglia.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#import <Foundation/Foundation.h>
#import <DIMVisual/Protocols.h>
#import <DIMVisual/Sync.h>
#import <GenericEvent/GEvent.h>
#import <GenericEvent/GEvent.h>
#import <GNUstepBase/GSXML.h>

@class GangliaHCMDataSource;

@interface GangliaHCMFileReader : NSObject <FileReader, Time>
{
	GEvent *topEvent;
	NSMutableDictionary *syncs;
	NSMutableArray *buf;
	Sync *sync;
	GangliaHCMDataSource *provider;

//	double simpleTime;//unused

	GSXMLParser *parser;
	GSXMLDocument *document;
	GSXMLNode *current;

	NSMutableArray *events; /* of GEvent */
}
- (id) initWithFileName: (NSString *) traceFile
	andSyncFileName: (NSString *) syncFile
	andProvider: (GangliaHCMDataSource *) prov;
- (id) initWithXMLData: (NSData *) traceData
	andSyncFileName: (NSString *) syncFile
	andProvider: (GangliaHCMDataSource *) prov;
- (NSString *) filename;
- (void) readingWithNode: (GSXMLNode *) node;
- (GEvent *) eventWithHostname: (NSString *) hostname
                        andReported: (NSString *) reported
                        andName: (NSString *) name
                        andValue: (NSString *) value;
@end

