/*
    DIMVisual, the Data Integration Model for Visualization of trace files.
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __COMMAND_H
#define __COMMAND_H
//#include "../config.h"
#include <Foundation/Foundation.h>
#include "BundleCenter.h"

#define HELP_COMMAND "-help"
#define BUNDLEDIR_COMMAND "-bundledir"
#define LOAD_COMMAND "-load"
#define LIST_COMMAND "-list"
#define OUTPUT_COMMAND "-output"

/*The commands bellow are for configuring the DIMV-HCM client.*/
#define AGGREG_COMMAND @"-aggregators"
#define CLIENTID_COMMAND @"-clientId"

@interface Command : NSObject
{
	NSMutableDictionary *configuration;
	NSMutableDictionary *configured;
	BundleCenter *bundleCenter;

	NSString *pathOfProgramExecution;
}
/* Initializing with argc,argv data */
- (id) initWithArgc: (int) argc andArgv: (char **) argv;
- (NSDictionary *) configuration;

/* Obtaining info */
/*
- (NSString *) syncFile;
- (NSString *) hierarchyFile;
- (NSString *) outputFile;
- (NSArray *) allFiles;
- (int) numberOfFiles;
*/

/* Adding and setting functions */
/*
- setSyncFile: (NSString *) filename;
- setHierarchyFile: (NSString *) filename;
- setOutputFile: (NSString *) filename;
*/

@end

#endif
