/*
    DIMVisual, the Data Integration Model for Visualization of trace files.
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __PROTOCOLS_H_
#define __PROTOCOLS_H_

#include <Foundation/Foundation.h>
#include <GenericEvent/GEvent.h>

@protocol Time
- (GTimestamp *) time;
- (double) simpleTime;
- (NSComparisonResult) compareTime: (id<Time>) otherObject;
@end

@protocol FileReader <Time>
- (GEvent *) event;
- (id) initWithFileName: (NSString *) traceFile andSyncFileName: (NSString *) syncFile;
@end

@protocol Integrator <NSObject>
- (NSString *) newAliasToName: (NSString *) nameP;
- (NSString *) aliasToName: (NSString *) nameP;
- (NSArray *) entitiesExists: (NSArray *) names;
- (NSString *) identifierForEntity: (NSArray *) names ids: (NSArray *) ids;
- (NSArray *) createContainerForEntity: (NSArray *) names ids: (NSArray *) ids
	time: (NSString *) time;
- (NSDictionary *) mergeHierarchies: (NSArray *) hierarchies;
@end

@protocol Converter
- initWithProvider: (id<Integrator>) provider;
- (id) convertEvent: (GEvent *) event;
@end

@protocol DataSource <Time, NSObject>
- (id) convert;
- (id) initWithProvider: (id<Integrator>) prov;
- (BOOL) setConfiguration: (NSDictionary *) configuration;
- (NSDictionary *) configuration;
- (NSDictionary *) hierarchy;
@end

@protocol HCMDataProcessor;
@class Order;

@protocol HCMDataSource <HCMDataProcessor>
- (void)resetTime; //sets the time to be equal to the time of the oldest event
- (NSString *)getType;
- (void)setOrder: (Order *)anOrder;
@end

#endif
