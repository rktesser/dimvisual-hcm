#import "DIMVAggregatorProto.h"
#import "CollectorRegister.h"
#import <Foundation/NSString.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSArray.h>
#import <Foundation/NSConnection.h>
#import <Foundation/NSPortNameServer.h>
#import <Foundation/NSRunLoop.h>
#import <Foundation/NSThread.h>
#import <Foundation/NSDistantObject.h>
#import <Foundation/NSLock.h>

#import "Observer.h"
#import "DIMVClientProto.h"
#import "AuxCopiableDictionary.h"
#import "DIMVConstants.h"

#ifdef DIMV_EVALUATE
#import <sys/time.h>
#import <Foundation/NSFileManager.h>
#import <Foundation/NSFileHandle.h>
#endif

#define RCCAPACITY 13

@interface DIMVClient : NSObject <DIMVClient>
{
 NSString *clientId;
 NSMutableDictionary *aggregators;
 AuxCopiableDictionary *registeredCollectors;
 NSMutableDictionary *dataSources;
 id<HCMSubscriptionHandler>subscriptionHandler;
 NSThread *connectionThread;//Thread where the communication is handled.
 NSConditionLock *connectionLock;//Used to wait until the connections are made.
 #ifdef DIMV_EVALUATE
 NSFileHandle *timesClientFH;
 #endif
}

- (DIMVClient *)initWithId: (NSString *)clientId andAggregators: (NSArray *)aggregatorNames;
#ifndef DIMV_EVALUATE
- (void)receiveData: (id)data fromCollector: (NSString *)collectorId;
#else
- (void)receiveData: (id)data fromCollector: (NSString *)collectorId
  counter: (unsigned)counter;
#endif
- (id<DIMVAggregator>) connectWithAggregator: (NSString *)name;
- (BOOL)addDataSource: (id)dSource withType: (NSString*)type;
- (void)registerCollectorWithId: (NSString *)colReg type: (NSString *)colType location: (NSString *)colLocation origin: (NSString *)originId;

/*Methods related to the subscripton mechanism*/
- (BOOL)subscribeToCollector: (NSString *)collectorId;
- (BOOL)unsubscribeToCollector: (NSString *)collectorId;

- (BOOL)setSubscriptionHandler: (id<HCMSubscriptionHandler>)aHandler;

@end
