/*
    DIMVisual-Ganglia, the DIMVisual bundle for Ganglia trace files
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual-Ganglia.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __GangliaCONVERTER__
#define __GangliaCONVERTER__
#include <Foundation/Foundation.h>
#include <DIMVisual/Protocols.h>
#include <Paje/PajeHierarchy.h>
#include "GangliaFileReader.h"

@interface GangliaConverter : NSObject <Converter>
{
	id<Integrator> provider;

	NSDictionary *grid5000;
}
- (PajeSetState *) addPajeSetState: (NSString *) state toState: (NSString *)   statename time: (NSString *) timex container: (NSString *) container;
@end

#endif
