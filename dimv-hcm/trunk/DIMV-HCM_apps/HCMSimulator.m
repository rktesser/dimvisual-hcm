#import "HCMSimulator.h"
@implementation HCMSimulator
- (BOOL)registerWithNameServer
{
	NSPort *port = [NSSocketPort port];
	connection = [NSConnection connectionWithReceivePort: port
	  sendPort: port];
	[connection setRootObject: self];
	if(![connection registerName: name withNameServer: 
	 [NSSocketPortNameServer sharedInstance]]){
		return NO;
	}
	return YES;
}

- (HCMSimulator *) initWithId: (NSString *)anId andName: (NSString *)aName
{
	self = [super init];
	myId = [anId retain];
	name = [aName retain];
	clients = [[NSMutableDictionary alloc] init];
	metadata = [[NSMutableArray alloc] init];
	collectors = [[AuxCopiableDictionary alloc] initWithCapacity: 
	  (unsigned)1];
	dataBuffer = [[NSMutableArray alloc] initWithCapacity: 
	  MONDATABUFFERSIZE];
	dataLock = [[NSConditionLock alloc] initWithCondition: 
	 MONDATABUFFERISEMPTY];
	needData = [[NSCondition alloc] init];

	startDate = [[NSDate date] retain];

	if([self registerWithNameServer]){
		return self;
	}else{
		NSLog(@"ERROR: The simulator couldn't be registered with the name server.");
		return nil;
	}
}

- (BOOL)readMetadataFromFile: (NSString *)fileName
{
	NSArray *content = [[NSArray alloc] initWithContentsOfFile: fileName];
	if(!content){
		NSLog(@"ERROR: Error while reading file \"%@\"", fileName);
		return NO;
	}
	[metadata addObjectsFromArray: [content subarrayWithRange: 
	  NSMakeRange(1, [content count] - 1)]];
	[content release];
	return YES;
}

NSComparisonResult sortMetaData(id item1, id item2, void *context)
{
	double sortField1, sortField2;
	sortField1 = [(NSString *)[item1 objectAtIndex: 1]  doubleValue];
	sortField2 = [(NSString *)[item2 objectAtIndex: 1]  doubleValue];
	if (sortField1 > sortField2){
	  	return NSOrderedDescending;
	}else if(sortField1 < sortField2){
		 return NSOrderedAscending;
	}else{
	 	return NSOrderedSame;
	}
}

- (void) forwardCollectorRegister: (CollectorRegister *)colReg
{
	NSEnumerator *clientEnum = [clients objectEnumerator];
	id<DIMVClient> client;
	while((client = [clientEnum nextObject]) != nil){
		[client registerCollectorWithId: [colReg getCollectorId]
		  type: [colReg getCollectorType] 
		  location: [colReg getCollectorLocation]
		  origin: myId];
	}
}

//We need to have at least one collector of each type registered in the client.
- (void)createCollectorRegisters
{
	int i;
	NSString *type;
	CollectorRegister *colReg;
	//the collector Id will be it's type. This eliminates the need to
	//find the collector Id when sending the monitoring data.
	for(i = 0; i < [metadata count]; i++){
		type = [[metadata objectAtIndex: i] objectAtIndex: 0];
		if([collectors objectForKey: type] == nil){
			colReg = [[CollectorRegister alloc] 
			  initWithCollectorId: type andType: type
			  andLocation: @"SIMULATOR"];
			[colReg setOriginId: myId];
			[collectors setObject: colReg  forKey: type];
			[self forwardCollectorRegister: colReg];
		}

	}
}

- (BOOL)readMetaFiles
{
	int i, found;
	BOOL ret;
	NSArray *allFileNames = nil;
	NSString *fileName = nil;
	NSArray *fileExtensions = [NSArray arrayWithObjects:
	  @"HCMMeta", nil];
	found = [@"HCM-" completePathIntoString: &fileName caseSensitive: YES
	  matchesIntoArray: &allFileNames filterTypes: fileExtensions];
	NSLog(@"I found %d meta file(s).", found);
	if(allFileNames != nil){
		for(i = 0; i < [allFileNames count]; i++){
			NSLog(@"%@", [allFileNames objectAtIndex: i]);
			ret = [self readMetadataFromFile:
			 [allFileNames objectAtIndex: i]];
			if(ret == NO){
				[metadata removeAllObjects];
				return NO;
			}
		}
	}
	[metadata sortUsingFunction: sortMetaData context: nil];
	return YES;
}

-(void)sendDataToClients: (id)args
{
	NSArray *argArray = (NSArray *)[args retain];
	NSString *data = [argArray objectAtIndex: 0];
	NSString *collectorId = [argArray objectAtIndex: 1];
 	NSEnumerator *clientEnum;
	id<DIMVClient> client;
	clientEnum = [clients objectEnumerator];
	while((client = [clientEnum nextObject]) != nil){
//		NSLog(@"Sending data to a client");
		#ifndef DIMV_EVALUATE
		[client receiveData: data fromCollector: collectorId];
		#else
		[client receiveData: data fromCollector: collectorId
		  counter: 0];
		#endif
	}
	[argArray release];
}

- (void)sendData: (NSString *)data withMetaData: (NSArray *)dataData
{
	NSString *collectorType = [dataData objectAtIndex: 0];
	double timestamp = [(NSString *)[dataData objectAtIndex: 1] 
	  doubleValue];
	[NSThread sleepUntilDate: [startDate addTimeInterval: timestamp]];
	[self performSelectorOnMainThread: @selector(sendDataToClients:)
	  withObject: [NSArray arrayWithObjects: data, collectorType, nil]
	  waitUntilDone: NO];
}

- (void)readDataWithSize: (unsigned)dataSize
{
	NSData *dataAsData;
	NSString *dataAsString;
	[dataLock lock];
	if([dataBuffer count] == MONDATABUFFERSIZE){
		[needData wait];
	}
//	NSLog(@"Reading file: \"%@\".",fileName);
	dataAsData = [dataFile readDataOfLength: dataSize];
	dataAsString = [[NSString alloc] initWithData: dataAsData
	  encoding: NSISOLatin1StringEncoding];
	[dataBuffer addObject: dataAsString];
	[dataAsString release];
	[dataLock unlockWithCondition: MONDATABUFFERHASDATA];
}

- (void)reader: (id)arg
{
	unsigned i;
	unsigned dataSize;
	NSAutoreleasePool *pool = [NSAutoreleasePool new];
	for(i = 0; i < [metadata count]; i++){
	dataSize = [(NSString *)[[metadata objectAtIndex: i] 
	  objectAtIndex: 2] intValue];
		[self readDataWithSize: dataSize];
		[pool emptyPool];
	}
	[pool release];
}

- (void)runSimulator
{
	int i;
	unsigned count;
	NSString *data;
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	NSThread *readerThread = [[NSThread alloc]  initWithTarget: self
	  selector: @selector(reader:) object: nil];
	[readerThread start];
	for(i = 0; i < [metadata count]; i++){
		[dataLock lockWhenCondition: MONDATABUFFERHASDATA];
		data = [[dataBuffer objectAtIndex: 0] retain];
		[dataBuffer removeObjectAtIndex: 0];
		count = [dataBuffer count];
		if(count < (MONDATABUFFERSIZE / 2)){
			[needData signal];
		}
		if(count == 0){
			[dataLock unlockWithCondition: MONDATABUFFERISEMPTY];
		}else{
			
			[dataLock unlockWithCondition: MONDATABUFFERHASDATA];
		}
//		NSLog(@"Send %d", i);
		[self sendData: data withMetaData: 
		  [metadata objectAtIndex: i]];
		[data release];
		[pool emptyPool];
	}
	[readerThread release];
	[pool release];
}	

- (BOOL)simulateWithDataInDirectory: (NSString *)pathToDirectory
{
	NSString *outputName;
	NSArray *extensions;
	NSArray *matches;
	[pathToDirectory retain];
	NSFileManager *fManager = [NSFileManager defaultManager];

	if(![fManager changeCurrentDirectoryPath: pathToDirectory]){
		NSLog(@"ERROR: \"%@\" is not a valid directory name",
		  pathToDirectory);
		return NO;
	}
	
	extensions = [NSArray arrayWithObjects: @"HCMMeta", nil];
	if([@"HCM-" completePathIntoString: &outputName caseSensitive: YES
		matchesIntoArray: &matches filterTypes: extensions] < 1){
		NSLog(@"ERROR: There are no HCM metadata files in the directory.");
		return NO;
	}
	
	extensions = [NSArray arrayWithObjects: @"HCMData", nil];
	if([@"HCM" completePathIntoString: &outputName caseSensitive: YES
		matchesIntoArray: &matches filterTypes: extensions] < 1){
		NSLog(@"ERROR: There are no HCM data files in the directory.");
		return NO;
	}
	dataFile = [NSFileHandle fileHandleForReadingAtPath: @"HCM.HCMData"];
	[dataFile retain];
	directoryName = [pathToDirectory retain];
	[pathToDirectory release];
	[self readMetaFiles];
	[self createCollectorRegisters];
	[NSThread detachNewThreadSelector: @selector(runSimulator)
	  toTarget: self withObject: nil];
	return YES;
}


- (void)registerClient: (id)client withId:(NSString *)clientId
{
	if(client != nil){
		[clients setObject: (id<DIMVClient>)client forKey: clientId];
	}
	return;
}

#ifndef DIMV_EVALUATE
- (oneway void) receiveData: (in bycopy id)data fromCollector: (in bycopy NSString *)collectorId
{
}
#else
- (oneway void) receiveData: (in bycopy id)data fromCollector: (in bycopy NSString *)collectorId counter: (in bycopy unsigned) counter
{
}
#endif
- (oneway void) subscribe:  (id<DIMVAggregator>)subscriber withCollector: (bycopy NSString *)collectorId
{
}

- (oneway void) unsubscribe: (NSString *)subscriberId withCollector: (NSString *)collectorId
{
}

- (oneway void) registerCollector: (id)collector
{
}

- (oneway void) registerCollectorWithId: (bycopy NSString *)colId type: (bycopy NSString *)colType location: (bycopy NSString *)colLocation origin: (bycopy NSString *)originId
{
}

- (oneway void) unregisterCollector: (NSString *) collectorId withOrigin: (bycopy NSString *)aggregatorId
{
}

- (oneway void) unregisterClient: (NSString *) clientId
{
	[clients removeObjectForKey:clientId];
}

- (bycopy NSString *) getId
{
	return myId;
}

- (bycopy NSString *) getName
{
	return name;
}
- (BOOL) registerSubordinate: (id<DIMVAggregator>)Subordinate withId: (bycopy NSString *)aggregatorId
{
	return NO;
}

- (bycopy AuxCopiableDictionary *) getRegisteredCollectors{
	return collectors;
}

- (oneway void)subscribeComponent: (bycopy NSString*)componentId toCollector:
(bycopy NSString *)collectorId
{
}

- (oneway void)unsubscribeComponent: (bycopy NSString*)componentId toCollector:
(bycopy NSString *)collectorId
{
}

- (id<DIMVAggregator>) connectWithAggregator: (NSString *)name
{
	return nil;
}
@end

int main(int argc, char **argv)
{
	NSString *simulId, *simulName, *dataDirectory;
	if(argc < 4){
		printf("Invalid syntax.\nUSE:\n\t%s <id> <name> <directory>\n", argv[0]);
		return 1;
	}
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	simulId = [NSString stringWithCString: argv[1]];
	simulName = [NSString stringWithCString: argv[2]];
	dataDirectory = [NSString stringWithCString: argv[3]];
	HCMSimulator *simulator = [[HCMSimulator alloc] initWithId: simulId
	  andName: simulName];
	if(!simulator){
		[pool release];
		return 2;
	}
	if(![simulator simulateWithDataInDirectory: dataDirectory]){
		[pool release];
		return 3;
	}
	[[NSRunLoop currentRunLoop] run];
	[pool release];
	return 0;
}

