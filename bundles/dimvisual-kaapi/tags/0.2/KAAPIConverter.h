/*
    DIMVisual-KAAPI, the DIMVisual bundle for KAAPI trace files
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual-KAAPI.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __KAAPICONVERTER__
#define __KAAPICONVERTER__
#include <Foundation/Foundation.h>
#include <DIMVisual/Protocols.h>
#include <Paje/PajeHierarchy.h>
#include "KAAPIFileReader.h"
#include "KAAPIConversionType.h"

@interface KAAPIConverter : NSObject <Converter>
{
	id<Integrator> provider;

	/* the following variables change 
 	 * every time a new event will be converted
 	 */
	NSString *gid, *cid, *time, *levelid, *eventid, *container;

	NSString *hostname;
	NSMutableDictionary *mappingGidHostname;


	NSMutableArray *buffer;
	NSMutableDictionary *names;
	NSMutableDictionary *links;

	NSMutableDictionary *workStealingLinks; //For WS categ

	KAAPIConversionType conversionType;
}
- (PajeSetState *) addPajeSetState: (NSString *) state toState: (NSString *)   statename time: (NSString *) timex container: (NSString *) container;
- (void) setConversionType: (KAAPIConversionType) convType;
@end

@interface KAAPIConverter (Trace)
- (id) convertEventTrace: (GEvent *) event;
@end

@interface KAAPIConverter (Util)
- (BOOL) isEventUtilThreadName;
- (id) convertEventUtilThreadName: (GEvent *) event;
- (id) convertEventUtil: (GEvent *) event;
@end


@interface KAAPIConverter (Kernel)
- (id) convertEventKernel: (GEvent *) event;
@end


@interface KAAPIConverter (RFO)
- (id) convertEventRFO: (GEvent *) event;
@end


@interface KAAPIConverter (DFG)
- (id) convertEventDFG: (GEvent *) event;
@end


@interface KAAPIConverter (WS)
- (id) cleanWorkStealingBuffer;
- (id) convertEventWS: (GEvent *) event;
@end


@interface KAAPIConverter (Network)
- (id) convertEventNetwork: (GEvent *) event;
@end


@interface KAAPIConverter (ST)
- (id) convertEventST: (GEvent *) event;
@end


@interface KAAPIConverter (FT)
- (id) convertEventFT: (GEvent *) event;
@end



#endif
