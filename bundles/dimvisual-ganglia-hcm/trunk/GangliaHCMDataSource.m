/*
    DIMVisual-Ganglia, the DIMVisual bundle for Ganglia trace files
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual-Ganglia.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "GangliaHCMDataSource.h"


@implementation GangliaHCMDataSource
- (id) initWithProvider: (id<Integrator>) provider
{
	self = [super init];
	prov = provider;
	[prov retain];
	if (provider == nil){
		NSLog (@"GangliaHCMDataSource: provider '%@' (Error)", provider);
		[self release];
		return nil;
	}
	eldest = nil;
	dataQueue = [[NSMutableArray alloc] 
	  initWithCapacity: DATAQUEUEINITIALCAPACITY];
	dataQueueLock = [[NSConditionLock alloc] 
	  initWithCondition: CONDQUEUEEMPTY];
	readerLock = [[NSRecursiveLock alloc] init];
	order = nil;
	return self;
}

- (void)setOrder: (Order *)anOrder
{

	if(anOrder != nil){
		[anOrder retain];
		
		/*
		 *Register the data source with the Order object.  This will be
		 used to
		 * determine when there's no active DS, since there may be
		 * active HCM data sources which are not in the heap.  In the
		 * future, a unregistration method may be implemented in order
		 * to make possible a cleaner shutdown of the app.
		 */
		[anOrder registerHCMDataSource: self];
		
		order = anOrder;
	}
}

/*
 * @TODO implement the unregistration of data sources with the Order Objject.
 */

- (void) resetConfiguration
{
	if (reader){
		[reader release];
	}
	if (converter){
		[converter release];
	}
}

/*This method receives a series of concatenated ganglia XML, wich are stored as
 * a string. It returns an array in wich each element is an unique Ganglia XML
 * converted to NSData.*/
- (NSMutableArray *) splitXML: (NSString *)concatData
{
	NSArray *components;
	NSMutableArray *ret;
	NSString *XMLStr;
	NSData * XMLData;
	int i;
	[concatData retain];
//	NSLog(@"concatdata: ###%@###", concatData);
	/*Split the concatenated XML data into an array of strings.*/
	components = [concatData 
			componentsSeparatedByString: @"</GANGLIA_XML>\n"];
	[components retain];
	[concatData release];

	ret = [[NSMutableArray alloc] 
		initWithCapacity: ([components count] -1)];
        
	/*The last component is empty because there is a separator at the end 
	 * of the concatenated XML. So it must be ignored.*/
	for(i = 0; i < ([components count] -1); i++){
		/*The separator is not included in the substrings by the method
		 * whe used to split the XML. Thus we need to append it now.*/
		XMLStr = [[components objectAtIndex: i] 
		  stringByAppendingString: @"</GANGLIA_XML>"];
		[XMLStr retain];
		/*Now we convert the NSStrings to NSData, wich can be used to
		 * create a GSXMLParser.*/
		XMLData = [XMLStr dataUsingEncoding: NSISOLatin1StringEncoding];
		[ret addObject: XMLData]; //Add the XML to the return array.
		[XMLStr release];
	}
	[components release];
	[ret autorelease];
	return ret;
}

- (NSArray *)getData;
{
	
	NSString *concatData;
	NSArray *splitData;
	
	[dataQueueLock lockWhenCondition: CONDQUEUEHASDATA];
	if([dataQueue count] > 0){ //Test if the lock condition is right.
		concatData = [dataQueue objectAtIndex: 0];
		[concatData retain];
		[dataQueue removeObjectAtIndex: 0];
	}else{
		NSLog(@"ERROR: GangliaHCMDataSource::DataConsumer - dataQueue should not be empty.\n");
	}

	if([dataQueue count] > 0){
		[dataQueueLock unlock];
	}else{
		[dataQueueLock unlockWithCondition: CONDQUEUEEMPTY];
	}
	
	splitData = [self splitXML: concatData];
//	[splitData retain];
	[concatData release];

//	[splitData autorelease];
	return splitData;
}


- (void)runDataConsumerWithArgs: (NSString *)arg
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
//	NSLog(@"DataConsumer: initializing.\n");
	NSArray *XMLData;
	GangliaHCMFileReader * aReader;
	int i;
	NSString *sync = arg;
	[sync retain];
	while((XMLData = [self getData]) != nil){
		[XMLData retain];
		for(i = 0; i < [XMLData count]; i++){
			aReader = [[GangliaHCMFileReader alloc] 
			  initWithXMLData: [XMLData objectAtIndex: i]
			  andSyncFileName: sync
			  andProvider: self];
			if(aReader == nil){
				NSLog(@"GangliaHCMDataSource::DataConsumer - Error: couldn't create a GangliaHCMFileReader.\n");
			}else{
//				NSLog(@"Consumer: Locking.\n");
				[readerLock lock];
//				NSLog(@"Consumer: Locked.\n");
				[reader add: aReader];
//				NSLog(@"GangliaHCMDataSource::DataConsumer - reader count = %u\n", [reader count]);
				/* If the the reader heap was empty, then  the
				 * enqueue the DS to be added in the order*/
				if((order != nil) && ([reader count] == 1)){
//					NSLog(@"GangliaHCMDataSource::DataConsumer - enqueueing data source.\n");
					[order enqueueDS: self];
				}
//				NSLog(@"Consumer: Unlocking.\n");
				[readerLock unlock];
				[aReader release];
			}
		}
		[XMLData release];
		[pool emptyPool];
	}
	NSLog(@"GangliaHCMDataSource::Datacomsumer - getData returned nil. The consumer thread will be finished.\n");
	[sync release];
	[pool release];
}

- (BOOL) setConfiguration: (NSDictionary *) configuration
{
	[self resetConfiguration];

	/* Extract information from configuration provided */
/*	id files;*/
	id param;
	id ident;
	id sync;


	if (configuration == nil){
		NSLog (@"GangliaHCMDataSource: configuration '%@'", configuration);
		[self release];
		return NO;
	}
	ident = [configuration objectForKey: IDKEY];
	if (ident == nil || [ident isEqual: IDVALUE] == NO){
		NSLog (@"GangliaHCMDataSource: configuration '%@' hasn't a field "
			"'ID' or is different from 'Ganglia'", configuration);
		[self release];
		return NO;
	}
	param = [configuration objectForKey: PARAMETERKEY];
	if (param == nil || [param isKindOfClass: [NSDictionary class]] == NO){
		NSLog (@"GangliaHCMDataSource: configuration '%@' hasn't a field "
			"'%@' or is not a NSArray", configuration,PARAMETERKEY);
		[self release];
		return NO;
	}
	/*Unneed if using DIMV_HCM
	files = [param objectForKey: (id)FILEKEY];
	if (files == nil || [files isKindOfClass: [NSArray class]] == NO){
		NSLog (@"GangliaHCMDataSource: configuration '%@' hasn't a field "
			"'%@' or is not a NSArray", configuration, FILEKEY);
		[self release];
		return NO;
	}*/
	sync = [param objectForKey: (id)SYNCKEY];
/*
	if ([sync isKindOfClass: [NSArray class]] && [sync count] >= 1){
		sync = [sync objectAtIndex: 0];
	}
	if (sync == nil || [sync isKindOfClass: [NSString class]] == NO){
		NSLog (@"GangliaHCMDataSource: configuration '%@' hasn't a field "
			"'%@' or is not a NSString", configuration, SYNCKEY);
//		[self release];
//		return NO;
	}
*/

	/*Instantiate the converter */
	converter = [[GangliaHCMConverter alloc] initWithProvider: prov];
	if (converter == nil){
		NSLog (@"GangliaHCMDataSource: couldn't create a GangliaHCMConverter"
			" with provider='%@'", prov);
		[self resetConfiguration];
		return NO;
	}
	
	/*The reader must be created here because of the call to [ self
	 * resetConfiguration] in the beginning of this method.*/
	reader = [[BinaryMinHeap alloc] init];

	/*Detach a data consumer thread.*/
//	NSLog(@"DimvisualHCMDatasource: Detaching consumer thread.\n");
	[NSThread detachNewThreadSelector: @selector(runDataConsumerWithArgs:)
	  toTarget: self withObject: sync];

//	NSLog(@"DimvisualHCMDatasource: the data source is configured.\n");
	return YES;
}


- (GTimestamp *) time;
{
	GTimestamp *time;
	//	return [reader time];
	[readerLock lock];
	time = eldest;
	[readerLock unlock];
	[time retain];
	[time autorelease];
	return time;//Hack to prevent messing to much with the Order heap.
}

- (NSString *) getType
{
	return GANGLIADSTYPE;
}

- (void) resetTime
{
	[readerLock lock];
	eldest = [reader time];
	[eldest retain];
	[readerLock unlock];
}



- (id) convert
{	
	GTimestamp *oldTime;
	id event, ret;
	
	[readerLock lock];//The reader array shared with the data consumer.
	GangliaHCMFileReader *oneReader = (id)[reader remove];
	oldTime = eldest;
	[oldTime release];
	if(oneReader == nil){ /* end of Ganglia XML files */
		ret =  nil;
	}else if((event = [oneReader event]) == nil){
		ret = [self convert];
	}else if([event name]){
		GName *n = (GName *)[event name];
		if ([[n description] isEqual: @"Ganglia_NOT_READ"]){
			NSMutableArray *ar = [NSMutableArray array];
			ret = ar;
		}else{
			ret = [converter convertEvent: event];

			/*debug output
			NSLog(@"Read an event with name: %@\n", 
			  [[event name] description]);
			 */
		}
		if([oneReader time] != nil){
			[reader add: oneReader];
		}
	}
	eldest = [reader time];//Returns nil if there's no reader.
	[eldest retain];
	[readerLock unlock];//End of critical section.
	return ret;
}

- (void) dealloc
{
	[converter release];
	[dataQueueLock release];
	[dataQueue release];
	[readerLock release];
	if(reader != nil){
		[reader release];
	}
	[super dealloc];
}

- (NSDictionary *) configuration
{
	NSMutableDictionary *c;
	NSMutableDictionary *parameters;

	parameters = [[NSMutableDictionary alloc] init];
	
	[parameters setObject: [NSArray array] forKey: FILEKEY];
	[parameters setObject: [NSString string] forKey: SYNCKEY];
//	[parameters setObject: [NSDictionary dictionaryWithContentsOfFile: [[NSBundle bundleForClass: [self class]] pathForResource: @"GangliaEvents" ofType: @"plist"]] forKey: @"events"];

//	NSMutableSet *types = [[NSMutableSet alloc] init];
//	[types addObject: @"process"];
//	[types addObject: @"thread"];
//	[types addObject: @"grid"];
//	[parameters setObject: types forKey: @"type"];
//	[types release];
		
	c = [[NSMutableDictionary alloc] init];
	[c setObject: IDVALUE forKey: IDKEY];
	[c setObject: parameters forKey: PARAMETERKEY];
	[parameters release];
	[c autorelease];

	return c;
}

- (NSDictionary *) hierarchy
{
	return [NSDictionary dictionaryWithContentsOfFile: [[NSBundle bundleForClass: [self class]] pathForResource: @"GangliaConfiguration-grid" ofType: @"plist"]];
}

- (double) simpleTime
{
	//return [reader simpleTime];
	return [eldest doubleValue];
}

- (NSComparisonResult) compareTime: (id<Time>) otherObject
{
        double time = [otherObject simpleTime];
	double simpleTime = [self simpleTime];
        if (simpleTime < time){
                return NSOrderedAscending;
        }else if (simpleTime > time){
                return NSOrderedDescending;
        }else{
                return NSOrderedSame;
        }
}


- (void)inputData: (id)data
{
	[data retain];
//	NSLog(@"GangliaHCMDataSource: Receiving data!\n");
	[dataQueueLock lock];
	[dataQueue addObject: data];
	[data release];
	[dataQueueLock unlockWithCondition: CONDQUEUEHASDATA];
}

@end
