/*
    DIMVisual, the Data Integration Model for Visualization of trace files.
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "BinaryMinHeap.h"

@implementation BinaryMinHeap
- (id) init
{
	self = [super init];
	sources = [[NSMutableArray alloc] init];
	DSQueue= [[NSMutableArray alloc] init];
	DSQueueLock = [[NSConditionLock alloc] 
	  initWithCondition: CONDQUEUEEMPTY];
	HCMDSReg = nil;
	lastRemoved = nil;
	return self;
}

- (void) dealloc
{
	[sources release];
	[DSQueue release];
	[DSQueueLock release];
	if(HCMDSReg != nil){
		[HCMDSReg release];
	}
	[super dealloc];
}

- (void) orderToTopIndex: (int) i
{
	if (i == 0){
		return;
	}
	int iparent = (int)((i-1)/2);
	id index, parent;
	index = [sources objectAtIndex: i];
	parent = [sources objectAtIndex: iparent];
	if ([index compareTime: parent] == NSOrderedAscending){
		[sources exchangeObjectAtIndex: i
			     withObjectAtIndex: iparent];
		[self orderToTopIndex: iparent]; 
	}
}

- (void) orderToBottomIndex: (int) i
{
	int max = [sources count];
	if (i == max){
		return;
	}

	int ichild1 = 2*i + 1;
	int ichild2 = 2*i + 2;

	id index = [sources objectAtIndex: i];
	id child1 = nil, child2 = nil;
	if (ichild1 < max){
		child1 = [sources objectAtIndex: ichild1];
	}
	if (ichild2 < max){
		child2 = [sources objectAtIndex: ichild2];
	}

	/* choose child */
	id target = nil;
	int itarget;
	if (!child1){
		target = child2;
		itarget = ichild2;
	}else if (!child2){
		target = child1;
		itarget = ichild1;
	}else if (child1 && child2){
		if ([child1 compareTime: child2] == NSOrderedDescending){
			target = child2;
			itarget = ichild2;
		}else{
			target = child1;
			itarget = ichild1;
		}
	}
	if (target && [index compareTime: target] == NSOrderedDescending){
		[sources exchangeObjectAtIndex: i
			     withObjectAtIndex: itarget];
		[self orderToBottomIndex: itarget];
	}
}

- (id<Time>) remove
{
	id<FileReader,NSObject> ret;
	ret = [sources objectAtIndex: 0];
	if (ret == nil){
		return nil;
	}
	[ret retain];
	if ([sources count] > 1){
		[sources exchangeObjectAtIndex: 0
			     withObjectAtIndex: [sources count] - 1];
	}
	[sources removeObjectAtIndex: [sources count] - 1];
	[self orderToBottomIndex: 0];
	[ret autorelease];
	return ret;
}

- (void) add: (id<Time>) source
{
	[sources addObject: source];
	[self orderToTopIndex: [sources count] - 1];
}


- (id) mostRecent
{
	id ret;
	id waitingDS;
	if (lastRemoved && [lastRemoved time] != nil){
//		NSLog(@"Putting the data source back in the heap!\n");
		[self add: lastRemoved];
		[lastRemoved release];
		lastRemoved = nil;
	}
	while((waitingDS = (id<HCMDataSource>)[self unqueueDS]) != nil){
		[self add: waitingDS];
	}
	while ([sources count] > 0){
		lastRemoved = [self remove];
		[lastRemoved retain];
		if ([lastRemoved time] == nil){
			[lastRemoved autorelease];
			ret = lastRemoved;
			lastRemoved = nil;
		}else{
			ret = lastRemoved;
		}
		return ret; 
	}
	return nil;
}

/*Methods related to the Time protocol;*/

- (GTimestamp *) time
{
	if ([sources count] == 0){
		return nil;
	}
	return [[sources objectAtIndex: 0] time];
}

- (double) simpleTime
{
	if([sources count] == 0){
		return 0;
	}
	return [[sources objectAtIndex: 0] simpleTime];
}

-  (NSComparisonResult) compareTime: (id<Time>) otherObject
{
	double myTime, hisTime;
	myTime = [self simpleTime];
	hisTime = [otherObject simpleTime];
	if(myTime > hisTime){
		return NSOrderedDescending;
	}else if(myTime < hisTime){
		return NSOrderedAscending;
	}else{
		return NSOrderedSame;
	}
}

/*The methods bellow are used only to manage DIMV-HCM data sources.*/


- (void)registerHCMDataSource: (id<HCMDataSource>) ds
{
	if(HCMDSReg == nil){
		HCMDSReg = [[NSMutableArray alloc] init];
	}
	[HCMDSReg addObject: ds];
}

- (void)enqueueDS: (id<HCMDataSource>)ds
{
//	NSLog(@"BinaryMinHeap::enqueueDS - Locking.\n!");
	[DSQueueLock lock];
	[DSQueue addObject: ds];
//	NSLog(@"BinaryMinHeap::enqueueDS - Unlocking with condition CONDQUEUEHASDATA.\n!");
	[DSQueueLock unlockWithCondition: CONDQUEUEHASDATA];
}

- (id<HCMDataSource>)unqueueDS
{
	id ds;
//	NSLog(@"BinaryMinHeap::unqueueDS - Locking.\n!");
	[DSQueueLock lock];

	if((HCMDSReg == nil) || ([HCMDSReg count] == 0)){//End here because there's no HCM data source
//		NSLog(@"BinaryMinHeap::unqueueDS - Unlocking.\n!");
		[DSQueueLock unlock];
		return nil;
	}
/*	NSLog(@"BinaryMinHeap::unqueueDS - sources count = %u.\n!",
	  [sources count]);*/
	
	if([sources count] > 0){ 
		if([DSQueue count] == 0){
//			NSLog(@"BinaryMinHeap::unqueueDS - Unlocking.\n!");
			[DSQueueLock unlock];
			/*No need to return data because there's data sources
			 * in the heap.*/
			return nil;
			}
	}else{
		
		/* If the sources heap is empty, then only acquire the lock if
		 * the DSQueue is not empty. Simply returning nil would lead to
		 * the premature finish of the program due to the emptiness of
		 * the sources heap.*/
//		NSLog(@"enqueued data sourcescount = %u\n", [DSQueue count]);
//		NSLog(@"BinaryMinHeap::unqueueDS - Unlocking.\n!");
		[DSQueueLock unlock];
//		NSLog(@"BinaryMinHeap::unqueueDS - Locking until there's available data sources.\n!");
		[DSQueueLock lockWhenCondition: CONDQUEUEHASDATA];
//		NSLog(@"BinaryMinHeap::unqueuDS Locked.\n!");
	}
	
	/*At this point ([sources count] > 0 && [DSQueeueCount > 0]) should be
	 * true.*/

	ds = (id<HCMDataSource>)[DSQueue objectAtIndex: 0];
	[ds retain];
	[DSQueue removeObjectAtIndex: 0];
	if([DSQueue count] > 0){
//		NSLog(@"BinaryMinHeap::unqueuDS Unlocking with condition:CONDQUEUEHASDATA .\n!");
		[DSQueueLock unlockWithCondition:CONDQUEUEHASDATA];
	}else{
//		NSLog(@"BinaryMinHeap::unqueueDS Unlocking with condition:CONDQUEUEEMPTY.\n!");
		[DSQueueLock unlockWithCondition:CONDQUEUEEMPTY];
	}
	[ds resetTime];
	[ds autorelease];
	return ds;
}

- (unsigned int) count
{
	return [sources count];
}

@end


