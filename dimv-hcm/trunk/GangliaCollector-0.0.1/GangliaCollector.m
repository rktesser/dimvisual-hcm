#import "DIMVCollector.h"
#import <Foundation/NSAutoreleasePool.h>
#import <Foundation/NSArray.h>
#import <Foundation/NSString.h>
#include <unistd.h>
#import "GetGangliaData.h"

int main(int argc, char **argv)
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	NSMutableArray *aggregators;
	NSString *collecType;
	NSString *collecId;
	NSString *gangliaData;
	int i;
	char recv_buf[GANGLIA_MAX_RECV_SIZE];

	if(argc < 5){
		printf("Error: Insufficient arguments!\n");
		printf("Format:\n");
		printf("GangliaCollector <id> <host> <port> <aggregator_1> [aggregator_2 aggregator_3 ...]");
		return 1;
	}

	collecId = [[NSString alloc] initWithCString: argv[1]];//the first argument is the Id of the collector
	NSLog(@"Suplied collector id is: %@\n", collecId);
	collecType = @"ganglia";
	aggregators = [NSMutableArray arrayWithCapacity: 5];
	RETAIN(aggregators);
	for(i = 4; i < argc; i++){//populate the array of Aggregator names
		[aggregators addObject: [[NSString alloc] initWithCString: argv[i]]];
		NSLog(@"\"%@\" has been added to the aggregator names list.\n", [aggregators objectAtIndex: (i - 4)]);
	}
	DIMVCollector *collector = [[DIMVCollector alloc] initWithId: collecId andType: collecType andAggregators: aggregators];
//	RELEASE(collecType);
	RELEASE(aggregators);
	for(i = 1; 1; i++){
		if (getGangliaData(argv[2], argv[3], recv_buf) == 0){
			gangliaData = [NSString stringWithCString: recv_buf];
			RETAIN(gangliaData);
//			NSLog(@"Calling sendData\n");
			[collector sendData: gangliaData];
			AUTORELEASE(gangliaData);

		}
		sleep(1);
	}
	RELEASE(collecId);
	RELEASE(collector);
//	[[NSRunLoop currentRunLoop] run];
	[pool release];
	return 0;
}
