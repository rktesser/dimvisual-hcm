/*
    DIMVisual-KAAPI, the DIMVisual bundle for KAAPI trace files
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual-KAAPI.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "KAAPIParser.h"

@implementation KAAPIParser (WS)

- (GEvent *) parseKAAPIWsLevel: (Util::RecordReader::const_iterator) r
	withIdentifier: (GIdentifier *) identifier
	withTimestamp: (GTimestamp *) timestamp
{
/*
	GName *name = [[GName alloc] init]; 
	GFields *fields = [[GFields alloc] init];
	GIdentifier *nameid = [[GIdentifier alloc] init];
	GIdentifier *nameid2 = [[GIdentifier alloc] init];

	switch (r->_event){
		case Event::WS_CLOSURE_STEAL0:
			[name setName: @"EVT_WS_CLOSURE_STEAL0"];
			{
			EVT_WS_CLOSURE_STEAL0_Record *kaapiev;
			kaapiev = (EVT_WS_CLOSURE_STEAL0_Record *)r;
			NSString *value = [NSString stringWithFormat: @"%l", kaapiev->victim];
			NSString *value2 = [NSString stringWithFormat: @"%p", kaapiev->closure];
			[nameid addValue: @"victim"];
			[nameid2 addValue: @"closure"];
			[fields setFieldWithKey: nameid withValue: value];
			[fields setFieldWithKey: nameid2 withValue: value2];
			}
			break;
		case Event::WS_CLOSURE_STEAL1:
			[name setName: @"EVT_WS_CLOSURE_STEAL1"];
			{
			EVT_WS_CLOSURE_STEAL1_Record *kaapiev;
			kaapiev = (EVT_WS_CLOSURE_STEAL1_Record *)r;
			NSString *value = [NSString stringWithFormat: @"%l", kaapiev->victim];
			NSString *value2 = [NSString stringWithFormat: @"%p", kaapiev->closure];
			[nameid addValue: @"victim"];
			[nameid2 addValue: @"closure"];
			[fields setFieldWithKey: nameid withValue: value];
			[fields setFieldWithKey: nameid2 withValue: value2];
			}
			break;
		case Event::WS_CLOSURE_SEND_DATA0:
			[name setName: @"EVT_WS_CLOSURE_SEND_DATA0"];
			{
			EVT_WS_CLOSURE_SEND_DATA0_Record *kaapiev;
			kaapiev = (EVT_WS_CLOSURE_SEND_DATA0_Record *)r;
			NSString *value = [NSString stringWithFormat: @"%i", kaapiev->gidvictim];
			NSString *value2 = [NSString stringWithFormat: @"%p", kaapiev->closure];
			[nameid addValue: @"gidvictim"];
			[nameid2 addValue: @"closure"];
			[fields setFieldWithKey: nameid withValue: value];
			[fields setFieldWithKey: nameid2 withValue: value2];
			}
			break;
		case Event::WS_CLOSURE_SEND_DATA1:
			[name setName: @"EVT_WS_CLOSURE_SEND_DATA1"];
			{
			EVT_WS_CLOSURE_SEND_DATA1_Record *kaapiev;
			kaapiev = (EVT_WS_CLOSURE_SEND_DATA1_Record *)r;
			NSString *value = [NSString stringWithFormat: @"%i", kaapiev->gidvictim];
			NSString *value2 = [NSString stringWithFormat: @"%p", kaapiev->closure];
			[nameid addValue: @"gidvictim"];
			[nameid2 addValue: @"closure"];
			[fields setFieldWithKey: nameid withValue: value];
			[fields setFieldWithKey: nameid2 withValue: value2];
			}
			break;
		case Event::WS_CLOSURE_RECV_DATA0:
			[name setName: @"EVT_WS_CLOSURE_RECV_DATA0"];
			{
			EVT_WS_CLOSURE_RECV_DATA0_Record *kaapiev;
			kaapiev = (EVT_WS_CLOSURE_RECV_DATA0_Record *)r;
			NSString *value = [NSString stringWithFormat: @"%i", kaapiev->gidvictim];
			[nameid addValue: @"gidvictim"];
			[fields setFieldWithKey: nameid withValue: value];
			}
			break;
		case Event::WS_CLOSURE_RECV_DATA1:
			[name setName: @"EVT_WS_CLOSURE_RECV_DATA1"];
			{
			EVT_WS_CLOSURE_RECV_DATA1_Record *kaapiev;
			kaapiev = (EVT_WS_CLOSURE_RECV_DATA1_Record *)r;
			NSString *value = [NSString stringWithFormat: @"%i", kaapiev->gidvictim];
			NSString *value2 = [NSString stringWithFormat: @"%p", kaapiev->closure];
			[nameid addValue: @"gidvictim"];
			[nameid2 addValue: @"closure"];
			[fields setFieldWithKey: nameid withValue: value];
			[fields setFieldWithKey: nameid2 withValue: value2];
			}
			break;
		case Event::WS_CLOSURE_SIGNAL0:
			[name setName: @"EVT_WS_CLOSURE_SIGNAL0"];
			{
			EVT_WS_CLOSURE_SIGNAL0_Record *kaapiev;
			kaapiev = (EVT_WS_CLOSURE_SIGNAL0_Record *)r;
			NSString *value = [NSString stringWithFormat: @"%p", kaapiev->closure];
			[nameid addValue: @"closure"];
			[fields setFieldWithKey: nameid withValue: value];
			}
			break;
		case Event::WS_CLOSURE_SIGNAL1:
			[name setName: @"EVT_WS_CLOSURE_SIGNAL1"];
			{
			EVT_WS_CLOSURE_SIGNAL1_Record *kaapiev;
			kaapiev = (EVT_WS_CLOSURE_SIGNAL1_Record *)r;
			NSString *value = [NSString stringWithFormat: @"%p", kaapiev->closure];
			[nameid addValue: @"closure"];
			[fields setFieldWithKey: nameid withValue: value];
			}
			break;
		default:
			NSLog (@"Warning: WS_Level -> type = %d, not completely translated", r->_event);
			break;
	}

	GEvent *ev = [[GEvent alloc] init];
	[ev setTimestamp: timestamp];
	[ev setIdentifier: identifier];
	[ev setName: name];
	[ev setFields: fields];
	[ev autorelease];

	[name release];
	[fields release];
	[nameid release];
	[nameid2 release];

	return ev;
*/
	return nil;
}

@end

