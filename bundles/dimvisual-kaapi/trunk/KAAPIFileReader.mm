/*
    DIMVisual-KAAPI, the DIMVisual bundle for KAAPI trace files
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual-KAAPI.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "KAAPIFileReader.h"

@implementation KAAPIFileReader
- (void) setTopEventInit
{
	if (hostname == nil){
		[self searchHostnameAndDefineSync];
	}

	if (ticksPerSecond == 0){
		[self searchTicksPerSecond];
	}

	unsigned int i;
	for (i = 0; i < [buf count]; i++){
		GEvent *e = (GEvent *)[buf objectAtIndex: i];
		[self defineTimestampForEvent: e];
		[self defineHostnameForEvent: e];
		[self synchronizeEvent: e];
	}
}

- (GEvent *) topEvent
{
	GEvent *ret = nil;
	[self consumeThreadNameEventsFromBuffer];
	if ([buf count] > 0){
		ret = [buf objectAtIndex: 0];
		[ret retain];
		[buf removeObjectAtIndex: 0];
	}else if ([waitingListThreadName count] > 0){
		ret = [waitingListThreadName objectAtIndex: 0];
		[ret retain];
		[waitingListThreadName removeObjectAtIndex: 0];
	}else{
		ret = [self read];
		[ret retain];
	}

	if (ret == nil){
		return nil;
	}

	simpleTime = [[ret timestamp] doubleValue];

	// try to find threadName
	NSString *cid = [[ret fields] fieldWithSimpleStringKey: @"cid"];
	NSString *threadName = [threadNames objectForKey: cid];
	if (threadName != nil){
		GFields *f = [ret fields];
		[f setFieldWithStringKey: @"threadName" withValue: threadName];
		[ret autorelease];
		return ret;
	}

	// not found.
	threadName = nil;
	int i;
	for (i = 0; i < 5 && threadName == nil; i++){
		GEvent *kaps = [self read];
		if (kaps == nil){
			break;
		}
		GName *name = [kaps name];
		if ([[name description] isEqual: @"EVT_UTIL_THREAD_NAME"]){
			GFields *f = [kaps fields];
			NSString *kapscid, *kapsThreadName;
			kapscid = [f fieldWithSimpleStringKey: @"cid"];
			kapsThreadName = [f fieldWithSimpleStringKey: @"name"];
			[threadNames setObject: kapsThreadName forKey: kapscid];
			if ([cid isEqual: kapscid]){
				threadName = kapsThreadName;
				break;
			}
		}else{
			[waitingListThreadName addObject: kaps];
		}
	}
	if (threadName == nil){
		//abandon current event
		[ret release];
		return [self topEvent];
	}else{
		GFields *f = [ret fields];
		[f setFieldWithStringKey: @"threadName" withValue: threadName];
	}
	[ret autorelease];
	return ret;
}


- (id) initWithFileName: (NSString *) traceFile
        andSyncFileName: (NSString *) syncFile
{
	return nil;
}


/* never called directly */
- (BOOL) _selfConfigureWithFileName: (NSString *) traceFile
        andSyncFileName: (NSString *) syncFile
{
	NSSet *hosts;
	int i;

	traceFilename = traceFile;
	[traceFilename retain];

	/* loading sync file with synchronization info's */
	if (syncFile != nil){
		syncs = [[NSMutableDictionary alloc] init];
		NSArray *ar = [Sync allMachinesOfFileNamed: syncFile];
		if (ar == nil){
			NSLog (@"KAAPIFileReader(%s): unable to read syncFile %@", __FUNCTION__, syncFile);
			return NO;
		}
		hosts = [NSSet setWithArray: ar];
		for (i = 0; i < (int)[[hosts allObjects] count]; i++){
			Sync *s;
			NSString *host = [[hosts allObjects] objectAtIndex: i];
			s=[[Sync alloc] initWithFileName: syncFile toHostname: host];
			[syncs setObject: s forKey: host];
			[s release];
		}
	}

	/* init */
	NSString *desc = [NSString stringWithFormat: @"%@.descr", [[traceFile componentsSeparatedByString: @"."] objectAtIndex: 0]];

	if ([self initializeWithTraceFile: traceFile 
			andDescriptionFile: desc] == NO){
		NSLog (@"KAAPIFileReader(%s): error loading traceFile %@ (Reason above)", __FUNCTION__, traceFile);
		return NO;
	}

	ticksPerSecond = 0;
	tickEV = 0;
	timeOfDayEV = 0;

	hostname = nil;
	buf = [[NSMutableArray alloc] init];
	waitingListThreadName  = [[NSMutableArray alloc] init];
	topEvent = nil;
	return YES;
}

- (id) initWithFileName: (NSString *) traceFile
        andSyncFileName: (NSString *) syncFile
        andProvider: (KAAPIDataSource *) prov
{
	if ([self _selfConfigureWithFileName: traceFile
			andSyncFileName: syncFile] == NO){
		return nil;
	}
	self = [super init];
	threadNames = [[NSMutableDictionary alloc] init];
	provider = prov;
	[provider retain];
	[self setTopEventInit];
	topEvent = [self topEvent];
	[topEvent retain];
	return self;
}


- (GTimestamp *) time
{
	if (topEvent != nil){
		return [topEvent timestamp];
	}else{
		return nil;
	}
}

- (void) dealloc
{
	[threadNames release];
	[super dealloc];
}

- (GEvent *) event
{
	GEvent *ev = topEvent;
	topEvent = [self topEvent];
	[topEvent retain];
	[ev autorelease];
	return ev;
}

- (NSString *) filename;
{
	return traceFilename;
}

- (double) simpleTime
{
	return simpleTime;
}

- (NSComparisonResult) compareTime: (id<Time>) otherObject
{
	double time = [otherObject simpleTime];
	if (simpleTime < time){
		return NSOrderedAscending;
	}else if (simpleTime > time){
		return NSOrderedDescending;
	}else{
		return NSOrderedSame;
	}
}
@end

