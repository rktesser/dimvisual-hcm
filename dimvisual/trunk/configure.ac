#    DIMVisual, the Data Integration Model for Visualization of trace files.
#    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>
#
#    This file is part of DIMVisual.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
AC_PREREQ(2.59)
AC_INIT(DIMVisual,0.9,schnorr@gmail.com,dimvisual)

# Check for GNUstep
if test -z "$GNUSTEP_SYSTEM_ROOT"; then
	AC_MSG_ERROR([You must run the GNUstep.sh script before configuring])
fi

AC_CONFIG_AUX_DIR($GNUSTEP_MAKEFILES)

# Set location of GNUstep dirs for later use
GNUSTEP_HDIR=$GNUSTEP_SYSTEM_ROOT/Library/Headers
if test "$GNUSTEP_FLATTENED" = yes; then
  GNUSTEP_LDIR=$GNUSTEP_SYSTEM_ROOT/Library/Libraries
else
  clean_target_os=`$GNUSTEP_MAKEFILES/clean_os.sh $target_os`
  clean_target_cpu=`$GNUSTEP_MAKEFILES/clean_cpu.sh $target_cpu`
  obj_dir=$clean_target_cpu/$clean_target_os
  GNUSTEP_LDIR=$GNUSTEP_SYSTEM_ROOT/Library/Libraries/$obj_dir
fi

# Checks for programs.
AC_PROG_CC

# Checks for header files.
AC_HEADER_STDC
AC_CHECK_HEADERS([fcntl.h stdlib.h string.h])

# Checks for typedefs, structures, and compiler characteristics.
AC_C_CONST
AC_HEADER_TIME

# Checks for library functions.
AC_FUNC_STRTOD
AC_CHECK_FUNCS([gettimeofday])

OUTPUTFILES="GNUmakefile Sources/GNUmakefile"
SUBPROJECTS_TO_COMPILE="Sources"

AC_SUBST(SUBPROJECTS_TO_COMPILE VERSION PACKAGE_TARNAME PACKAGE_VERSION)
AC_SUBST(LDFLAGS)
AC_SUBST(LIBS)
AC_OUTPUT($OUTPUTFILES)

AC_MSG_NOTICE([

	Congratulations.

	* Type 'make' to compile and 'make install' to install
	])
