/*
    DIMVisual-KAAPI, the DIMVisual bundle for KAAPI trace files
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual-KAAPI.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "KAAPIConverter.h"
#include <kaapi>

@implementation KAAPIConverter (SockScavanger)
- (id) convertSockScavangerWithEvent: (GEvent *) event
{
	NSMutableArray *ret = [NSMutableArray new];
	
	GFields *f = [event fields];

	gid = [[f fieldWithSimpleStringKey:@"gid"] description];
	cid = [[f fieldWithSimpleStringKey:@"cid"] description];
	time = [[event timestamp] description];
	levelid = [[f fieldWithSimpleStringKey:@"level"] description];
	eventid = [[f fieldWithSimpleStringKey:@"event"] description];
	hostname = [[f fieldWithSimpleStringKey:@"hostname"] description];
	[mappingGidHostname setObject: hostname forKey: gid];
	[mappingGidHostname setObject: gid forKey: hostname];

	int ilevelid, ieventid;
	ilevelid = atoi([levelid cString]);
	ieventid = atoi([eventid cString]);
	
	NSString *site;
	NSString *cluster;
	NSString *machine;
	NSString *process;

	NSArray *aux = [hostname componentsSeparatedByString: @"-"];
	NSString *straux = [aux objectAtIndex: 0];

	if ([grid5000 objectForKey: straux] != nil) { //grid5000
		site = [grid5000 objectForKey: straux];
		cluster = straux;

		NSString *rest = [aux objectAtIndex: 1];
		NSArray *aux2;
		aux2 = [rest componentsSeparatedByString: @"."];

		machine = [aux2 objectAtIndex: 0];
	}else{
		site = cluster = machine = hostname;
	}
	process = gid;

	NSArray *creation = [provider createContainerForEntity:
		[NSArray arrayWithObjects:
			@"site", @"cluster", @"machine",
			@"process", @"sock-scavanger", nil]
		ids:
		[NSArray arrayWithObjects:
			site, cluster, machine, process, cid, nil]
		time: time];
	container = [provider identifierForEntity:
		[NSArray arrayWithObjects:
			@"site", @"cluster", @"machine",
			@"process", @"sock-scavanger", nil]
		ids:
		[NSArray arrayWithObjects:
			site, cluster, machine, process, cid, nil]];
	if ([creation count] != 0){
		[ret addObjectsFromArray: creation];
	}

	//translate events
	switch (ilevelid){
		case Event::TR_KERNEL_LEVEL:
			switch (ieventid){
				case Event::CORE_THREAD_LSTEAL:
					[ret addObject:
					[self addPajePushState:@"LSTEAL"
						toState: @"State8"
						time: time
						container: container]];
					break;
				case Event::CORE_THREAD_RETLSTEAL:
					[ret addObject:
					[self addPajePopState: @"State8"
						time: time
						container: container]];
					break;
			}
			break;
		case Event::TR_WS_LEVEL:
			switch (ieventid){
				case Event::WS_CLOSURE_STEAL0:
					[ret addObject:
					[self addPajePushState:@"STEAL"
						toState: @"State8"
						time: time
						container: container]];
					break;
				case Event::WS_CLOSURE_STEAL1:
					[ret addObject:
					[self addPajePopState: @"State8"
						time: time
						container: container]];
					break;
				case Event::WS_CLOSURE_RECSTL0:
					[ret addObject:
					[self addPajePushState:@"RECSTL"
						toState: @"State8"
						time: time
						container: container]];
					break;
				case Event::WS_CLOSURE_RECSTL1:
					[ret addObject:
					[self addPajePopState: @"State8"
						time: time
						container: container]];
					break;
				case Event::WS_CLOSURE_RECV_DATA0:
					[ret addObject:
					[self addPajePushState:@"RECV_DATA"
						toState: @"State8"
						time: time
						container: container]];
					break;
				case Event::WS_CLOSURE_RECV_DATA1:
					[ret addObject:
					[self addPajePopState: @"State8"
						time: time
						container: container]];
					break;
				case Event::WS_CLOSURE_SIGNAL0:
					[ret addObject:
					[self addPajePushState:@"SIGNAL"
						toState: @"State8"
						time: time
						container: container]];
					break;
				case Event::WS_CLOSURE_SIGNAL1:
					[ret addObject:
					[self addPajePopState: @"State8"
						time: time
						container: container]];
					break;
			}
			break;
	}
	[ret autorelease];
	return ret;
}
@end
