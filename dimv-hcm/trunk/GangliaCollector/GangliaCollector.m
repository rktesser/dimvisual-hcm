#import "DIMVCollector.h"
#import <Foundation/NSAutoreleasePool.h>
#import <Foundation/NSArray.h>
#import <Foundation/NSString.h>
#import <Foundation/NSDictionary.h>
#import <Foundation/NSFileHandle.h>
#import <Foundation/NSFileManager.h>
#import <Foundation/NSDebug.h>
#include <unistd.h>
#include <sys/time.h>
#import "GetGangliaData.h"
#import "GCConf.h"
#import "GCDefs.h"

#define N_DATA_STR_SIZE (24)

int main(int argc, char **argv)
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	NSString *gangliaData;
	NSString *confFileName;
	NSMutableString *concatenatedData;
	GCConf *conf;
	int i, n_data, polling_period;
	BOOL ret;
	char recv_buf[GANGLIA_MAX_RECV_SIZE];
	char host[GC_HOST_STR_SIZE], port[GC_PORT_STR_SIZE];
	#ifdef DIMV_EVALUATE
	NSFileHandle * timesGC;
	NSString * timesGCFName;
	struct timeval to, t1;
	unsigned long long to_usec, t1_usec;
	unsigned send_count = 0;
	#endif
//	GSDebugAllocationActive(YES);
//	printf("%s\n", GSDebugAllocationList(YES));
	#ifdef DIMV_EVALUATE
	NSFileManager * fManager = [NSFileManager defaultManager];
	RETAIN(fManager);
	#endif

	if(argc < 2){
		printf("Error: Insufficient arguments!\n");
		return 1;
	}

	/*Initialize the configuration with a config file.*/
	confFileName = [NSString stringWithCString: argv[1]];
	RETAIN(confFileName);
	AUTORELEASE(confFileName);
	conf = [[GCConf alloc] initWithConfFile: confFileName];
	
	//initialize the DIMVCollector object
	DIMVCollector *collector = [[DIMVCollector alloc] 
	  initWithId: [conf getCollectorId]
	  andType: [conf getCollectorType]
	  andAggregators: [conf getAggregators]];
	
	//get the host name
	ret = [conf getHostAsCString: host];
	if(ret == NO){
		return 1;
	}

	//get the port
	ret = [conf getPortAsCString: port];
	if(ret == NO){
		return 1;
	}

	//Get the number of data receivings to accumulate before sending them.
	n_data = [conf getNData];
	polling_period = [conf getPollingPeriod];

	#ifdef DIMV_EVALUATE
	timesGCFName = [[conf getCollectorId] stringByAppendingString:
	   @"-timesGC.output"];
	RETAIN(timesGCFName);
	if([fManager createFileAtPath: timesGCFName contents: nil 
	 attributes: nil] == NO){
		NSLog(@"Couldn't create the file %@\n", timesGCFName);
		exit(1);
	}
	RELEASE(fManager);
	timesGC = [NSFileHandle fileHandleForWritingAtPath: timesGCFName];
	RETAIN(timesGC);
	RELEASE(timesGCFName);
	#endif

	#ifndef DIMV_EVALUATE
	RELEASE(conf);//The configuration has been completely read.
	#endif

	while(1){//do the work
		NSAutoreleasePool *loopPool = [[NSAutoreleasePool alloc] init];
		concatenatedData = [[NSMutableString alloc] init];
		for(i = 0; i < n_data; i++){
			usleep(polling_period);
//			printf("Host: \"%s\"; Port: \"%s\".\n", host, port);
			if (getGangliaData(host, port, recv_buf) == 0){
				gangliaData = [NSString stringWithCString: recv_buf];
//				RETAIN(gangliaData);
				[concatenatedData appendString: gangliaData];
//				RELEASE(gangliaData);
			}else{//An error has ocurred inside getGangliaData
				NSLog(@"GangliaCollector: An error has ocurred when trying to get data from ganglia!\n");
				i--;
			}
		}
		#ifndef DIMV_EVALUATE
		[collector sendData: concatenatedData];
		#else
		gettimeofday(&to, NULL);
		[collector sendData: concatenatedData 
		   counter: send_count];
		gettimeofday(&t1, NULL);
		to_usec = to.tv_usec + (to.tv_sec * 1000000);
		t1_usec = t1.tv_usec + (t1.tv_sec * 1000000);
		[timesGC writeData: [[NSString stringWithFormat:
		 @"%@ %010u %llu %llu\n", [conf getCollectorId],
		  send_count, to_usec, (t1_usec - to_usec)]
		   dataUsingEncoding: NSUTF8StringEncoding]];
		send_count++;
		#endif
		RELEASE(concatenatedData);
		[loopPool release];
//		printf("%s\n", GSDebugAllocationList(YES));
	}
	#ifdef DIMV_EVALUATE
	RELEASE(conf);//The configuration has been completely read.
	RELEASE(timesGC);
	#endif
	RELEASE(collector);
	[pool release];
	return 0;
}
