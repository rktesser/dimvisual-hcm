/*
    DIMVisual-Ganglia, the DIMVisual bundle for Ganglia trace files
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual-Ganglia.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "GangliaFileReader.h"

@implementation GangliaFileReader
- (GEvent *) eventWithHostname: (NSString *) hostname 
			andReported: (NSString *) reported
			andName: (NSString *) name
			andValue: (NSString *) value
{
	GEvent *ev = [[GEvent alloc] init];
	GName *evname = [[GName alloc] init];
	GFields *fields = [[GFields alloc] init];
	GTimestamp *timestamp = [[GTimestamp alloc] init];
	
	[timestamp setTimestamp: reported];
	[evname setName: name];

	GIdentifier *nameid1 = [[GIdentifier alloc] init];
	GIdentifier *nameid2 = [[GIdentifier alloc] init];
	[nameid1 addValue: @"value"];
	[nameid2 addValue: @"hostname"];

	[fields setFieldWithKey: nameid1 withValue: value];
	[fields setFieldWithKey: nameid2 withValue: hostname];

	[ev setTimestamp: timestamp];
	[ev setName: evname];
	[ev setFields: fields];
	[ev autorelease];
	[evname release];
	[fields release];
	[timestamp release];
	[nameid1 release];
	[nameid2 release];
	return ev;
}

- (void) readingWithNode: (GSXMLNode *) node
{
	GSXMLNode *n;
	n = [node firstChildElement];
	while (n != nil){
		if ([[n name] isEqualToString: @"METRIC"]){
			/* search time in parent's parent */
			NSString *reported;
			GSXMLNode *p = [n parent];
			GSXMLAttribute *at = [[p parent] firstAttribute];
			while (at){
			        if ([[at name] isEqualToString:
							@"LOCALTIME"]){
			                reported = [at value];
					break;
			        }
			        at = [at next];
			}

			/* search hostname for the metric */
			NSString *hostname;
			at = [[n parent] firstAttribute];
			while (at){
			        if ([[at name] isEqualToString:
							@"NAME"]){
			                hostname = [at value];
					break;
			        }
			        at = [at next];
			}
			
			/* search local attributes */
			at = [n firstAttribute];
			if ([[at value] isEqualToString: @"cpu_user"] ||
			        [[at value] isEqualToString: @"mem_free"]){
			        NSString *name = [at value];
			        at = [at next];
				NSString *value = [at value];
		
				GEvent *ev;
				ev = [self eventWithHostname: hostname
					andReported: reported
					andName: name
					andValue: value];
				[events addObject: ev];

				
				
			}
		}
		[self readingWithNode: n];
		n = [n nextElement];
	}
}

- (id) initWithFileName: (NSString *) traceFile
        andSyncFileName: (NSString *) syncFile
{
	return nil;
}

- (id) initWithFileName: (NSString *) traceFile
        andSyncFileName: (NSString *) syncFile
        andProvider: (GangliaDataSource *) prov
{
	NSSet *hosts;
	int i;
	self = [super init];

	/* loading sync file with synchronization info's */
	if (syncFile != nil){
		syncs = [[NSMutableDictionary alloc] init];
		NSArray *ar = [Sync allMachinesOfFileNamed: syncFile];
		if (ar == nil){
			NSLog (@"GangliaFileReader(%s): unable to read "
				"syncFile %@", __FUNCTION__, syncFile);
			return nil;
		}
		hosts = [NSSet setWithArray: ar];
		for (i = 0; i < (int)[[hosts allObjects] count]; i++){
			Sync *s;
			NSString *host = [[hosts allObjects] objectAtIndex: i];
			s = [[Sync alloc] initWithFileName: syncFile
					toHostname: host];
			[syncs setObject: s forKey: host];
			[s release];
		}
	}

	provider = prov;
	[provider retain];

	parser = [GSXMLParser parserWithContentsOfFile: traceFile];
	if (!parser){
		return nil;
	}
	[parser parse];
	document = [parser document];
	current = [document root];

	events = [[NSMutableArray alloc] init];
	[self readingWithNode: current];
	NSArray *r = [events sortedArrayUsingSelector: @selector(compareTime:)];
	NSMutableArray *x = [[NSMutableArray alloc] initWithArray: r];
	[events release];
	events = x;
	return self;
}


- (GTimestamp *) time
{
	if ([events count] > 0){
		return [[events objectAtIndex: 0] timestamp];
	}else{
		return nil;
	}
}

- (void) dealloc
{
	[super dealloc];
}

- (GEvent *) event
{
	if ([events count] == 0){
		return nil;
	}
	GEvent *ev = [events objectAtIndex: 0];
	[ev retain];
	[events removeObjectAtIndex: 0];
	[ev autorelease];
	return ev;
}

- (NSString *) filename;
{
	return @"GangliaXMLFile";
}

- (double) simpleTime
{
	return simpleTime;
}

- (NSComparisonResult) compareTime: (id<Time>) otherObject
{
	double time = [otherObject simpleTime];
	if (simpleTime < time){
		return NSOrderedAscending;
	}else if (simpleTime > time){
		return NSOrderedDescending;
	}else{
		return NSOrderedSame;
	}
}
@end

