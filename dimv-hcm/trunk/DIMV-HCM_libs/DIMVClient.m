#import "DIMVClient.h"
#import <Foundation/NSEnumerator.h>

#define CONNECTIONSNOTINITIALIZED_COND 0
#define CONNECTIONSINITIALIZED_COND 1

@implementation DIMVClient

- (void)runConnections: (id)arg
{
	int i, j;
	id remoteAggregator;
	NSEnumerator *anEnumerator;
	CollectorRegister *colReg;
	AuxCopiableDictionary *collectorsFromAggregator;
	NSArray *subscribers;
	NSArray *aggregatorNames = (NSArray *)arg;
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	RETAIN(aggregatorNames);
	[connectionLock lock];
	for(i = 0; i < [aggregatorNames count]; i++){
		remoteAggregator = (id<DIMVAggregator>) [self connectWithAggregator: [aggregatorNames objectAtIndex: i]];
		[aggregators setObject: remoteAggregator forKey: [remoteAggregator getId]];

		[remoteAggregator registerClient: self withId: clientId];
		collectorsFromAggregator = [remoteAggregator getRegisteredCollectors];
		RETAIN(collectorsFromAggregator);
		anEnumerator = [collectorsFromAggregator objectEnumerator];
		RETAIN(anEnumerator);
		while((colReg = [anEnumerator nextObject]) != nil){	
			subscribers =  [[colReg getSubscribers] allKeys];
			RETAIN(subscribers);
			for(j = 0; j < [subscribers count]; j++){
				[colReg removeSubscriber: 
				  [subscribers objectAtIndex: i]];
			}
			RELEASE(subscribers);
			[colReg setOriginId: [remoteAggregator getId]];
		}
		RELEASE(anEnumerator);
		[registeredCollectors addEntriesFromDictionary: 
		  collectorsFromAggregator];
		RELEASE(collectorsFromAggregator);
	}
	[aggregatorNames release];
	[connectionLock unlockWithCondition: CONNECTIONSINITIALIZED_COND];
	[[NSRunLoop currentRunLoop] run];
	[pool release];
}


- (DIMVClient *) initWithId: (NSString *) clId andAggregators: (NSArray *) aggregatorNames
{
/*	int i, j;
	id remoteAggregator;
	NSEnumerator *anEnumerator;
	AuxCopiableDictionary *collectorsFromAggregator;
	CollectorRegister *colReg;
	NSArray *subscribers;*/
	subscriptionHandler = nil;
	#ifdef DIMV_EVALUATE
	NSString *timesFName = [clId stringByAppendingString:
	  @"-timesClient.output"];
	RETAIN(timesFName);
	if([[NSFileManager defaultManager] createFileAtPath: timesFName
	  contents: nil attributes: nil] == NO){
		NSLog(
		  @"init: Couldn't create the file %@-timesClient.output\n", 
		  clId);
		exit(1);
	}
	timesClientFH = [NSFileHandle fileHandleForWritingAtPath: timesFName];
	RETAIN(timesClientFH);
	RELEASE(timesFName);
	#endif
	[super init];
	
	ASSIGN(clientId, clId);
	aggregators = [NSMutableDictionary dictionary];
	RETAIN(aggregators);
	registeredCollectors = [[AuxCopiableDictionary alloc] initWithCapacity: 13];
	dataSources = [NSMutableDictionary dictionary];
	RETAIN(dataSources);
	connectionLock = [[NSConditionLock alloc] initWithCondition: 
	  CONNECTIONSNOTINITIALIZED_COND];
	connectionThread = [[NSThread alloc] initWithTarget: self
	  selector: @selector(runConnections:) object: aggregatorNames];
	[connectionThread start];
	[connectionLock lockWhenCondition: CONNECTIONSINITIALIZED_COND];
	[connectionLock unlock];
/*	for(i = 0; i < [aggregatorNames count]; i++){
		remoteAggregator = (id<DIMVAggregator>) [self connectWithAggregator: [aggregatorNames objectAtIndex: i]];
		[aggregators setObject: remoteAggregator forKey: [remoteAggregator getId]];

		collectorsFromAggregator = [remoteAggregator getRegisteredCollectors];
		RETAIN(collectorsFromAggregator);
		anEnumerator = [collectorsFromAggregator objectEnumerator];
		RETAIN(anEnumerator);
		while((colReg = [anEnumerator nextObject]) != nil){	
			subscribers =  [[colReg getSubscribers] allKeys];
			RETAIN(subscribers);
			for(j = 0; j < [subscribers count]; j++){
				[colReg removeSubscriber: 
				  [subscribers objectAtIndex: i]];
			}
			RELEASE(subscribers);
			[colReg setOriginId: [remoteAggregator getId]];
		}
		RELEASE(anEnumerator);
		[registeredCollectors addEntriesFromDictionary: collectorsFromAggregator];
		RELEASE(collectorsFromAggregator);
		[remoteAggregator registerClient: self withId: clientId];
	}*/
	return self;
}

- (void) dealloc
{
	RELEASE(clientId);
	RELEASE(aggregators);
	RELEASE(registeredCollectors);
	RELEASE(dataSources);
	TEST_RELEASE(subscriptionHandler);
	RELEASE(connectionLock);
	RELEASE(connectionThread);
	#ifdef DIMV_EVALUATE
	[timesClientFH closeFile];
	RELEASE(timesClientFH);
	#endif
	[super dealloc];
}


- (id<DIMVAggregator>) connectWithAggregator: (NSString *)name
{
	id remoteAggregator;

	do{
		remoteAggregator = (id<DIMVAggregator>) [NSConnection rootProxyForConnectionWithRegisteredName: name host: @"*" usingNameServer: [NSSocketPortNameServer sharedInstance]];
		if(remoteAggregator == nil){
			NSLog(@"Error: Couldn\'t connect with aggregator with name: %@\n", name);
			[NSThread sleepForTimeInterval: DIMV_CONN_RETRY_WAIT];
		}
	}while(remoteAggregator == nil);
	RETAIN(remoteAggregator);
	[[NSNotificationCenter defaultCenter] addObserver: [Observer new] selector: @selector(connectionDied:) name: NSConnectionDidDieNotification object: [(NSDistantObject *) remoteAggregator connectionForProxy]];
	
	AUTORELEASE(remoteAggregator);
	return remoteAggregator;
}

#ifdef DIMV_EVALUATE
- (void) receiveData: (id) data fromCollector:
  (NSString *) collectorId counter: (unsigned) counter
#else
- (void) receiveData: (id) data fromCollector: (NSString *) collectorId
#endif
{
	#ifdef DIMV_EVALUATE
	struct timeval to, tf;
	unsigned long long to_usec;
	gettimeofday(&to, NULL);
	#endif
	
	id dSource;
	NSString * type = nil;
	CollectorRegister * reg;

	RETAIN(collectorId);
	RETAIN(data);
//	NSLog(@"Received data from collector with id: %@\n", collectorId);
//	NSLog(@"Data: \"%@\"\n", data);


	//get the type of the collector
	reg = [registeredCollectors objectForKey: collectorId];
	if(reg != nil){
		RETAIN(reg);
		type = (NSString *)[reg getCollectorType];
		if(type == nil){
			NSLog(@"Houston, we have a problem!\n");
			exit(1);
		}
		RETAIN(type);
	
		//get the integrator dataSource for this type of collector
		dSource = (id<HCMDataProcessor>) [dataSources objectForKey: type];
		if(dSource == nil){
			NSLog(@"Error: There's no data source of type \"%@\" registered.\n", type);
		}else{
			RETAIN(dSource);
			//Forward the data to the data source.
			[dSource inputData: data];
			AUTORELEASE(data);
			RELEASE(dSource);
		}
		RELEASE(type);
		RELEASE(reg);
	}
	#ifdef DIMV_EVALUATE
	gettimeofday(&tf, NULL);
	to_usec = (to.tv_sec * 1000000) + to.tv_usec;
	[timesClientFH writeData: [[NSString 
	  stringWithFormat: @"%@ %010u %llu %llu\n",
	  collectorId, counter, to_usec, ((tf.tv_sec * 1000000 + tf.tv_usec ) - to_usec)]
	  dataUsingEncoding:NSUTF8StringEncoding]];
	#endif
	RELEASE(collectorId);
}
/*
- (void) registerCollectorWithRegister: (CollectorRegister *)colReg 
  andOriginId: (NSString *)originId
{
	RETAIN(colReg);
	RETAIN(originId);
	[colReg setOriginId: originId];
	RELEASE(originId);
	[registeredCollectors setObject: colReg 
	  forKey: [colReg getCollectorId]];
	RELEASE(colReg);
}
*/

- (oneway void) registerCollectorWithId: (bycopy NSString *)colId type: (bycopy NSString *)colType location: (bycopy NSString *)colLocation origin: (bycopy NSString *)originId
{
	RETAIN(colId);
	RETAIN(colType);
	RETAIN(originId);
	RETAIN(colLocation);
	CollectorRegister * colReg;
	
	/*Create the register*/
	colReg = [[CollectorRegister alloc] initWithCollectorId: colId andType: colType andLocation: colLocation];
	[colReg setOriginId: originId];
	RELEASE(originId);
	/*Register the collector using it's Id as key.*/
	[registeredCollectors setObject: colReg forKey: colId];
	if(subscriptionHandler != nil){
		[subscriptionHandler registerCollectorWithId: colId
		  type: colType location: colLocation];
	}
	RELEASE(colId);
	RELEASE(colType);
	RELEASE(colLocation);
	NSLog(@"Registered a collector with Id = \"%@\" and type = \"%@\"\n", [colReg getCollectorId], [colReg getCollectorType]);
	RELEASE(colReg);
	return;
}

- (BOOL) addDataSource: (id) dSource withType: (NSString *)type
{
	if([dSource conformsToProtocol: @protocol(HCMDataProcessor)]){
		[dataSources setObject:dSource forKey: type];
	}else{
		NSLog(@"ERROR: The object supplied as a data source doesn't conform to the HCMDataProcessor protocol.\n");
		return NO;
	}
	return YES;
}

/*Method to be executed in the connectionThread.*/
- (void)internalSubscribeToCollector:(id)arg
{
	NSArray *arguments = (NSArray *)arg;
	[arguments retain];
	id origin = (id<DIMVAggregator>)[arg objectAtIndex: 0];
	NSString *collectorId = (NSString *)[arg objectAtIndex: 1];
	[origin subscribeComponent: clientId toCollector: collectorId];
	[arguments release];
}

- (BOOL)subscribeToCollector: (NSString *)collectorId
{
	CollectorRegister *colReg;
	id origin;
	NSArray *args;
	BOOL returnValue = NO;
	RETAIN(collectorId);
	colReg = [registeredCollectors objectForKey: collectorId];
	if(colReg == nil){
		NSLog(@"ERROR: Impossible to subscribe to collector with id \"%@\". There's no registered collector with this Id.");
	}else{
		origin = (id<DIMVAggregator>)[aggregators 
		  objectForKey: [colReg getOriginId]];
		if(origin == nil){//This shouldn't happen.
			NSLog(@"ERROR: Subscription error. The registered origin of the collector is not a registered aggregator.");
		}else{
//			[origin subscribeComponent: clientId toCollector: collectorId];
			args = [NSArray arrayWithObjects: origin, collectorId,
			  nil];
			[self performSelector: 
			  @selector(internalSubscribeToCollector:)
			  onThread: connectionThread withObject: args
			  waitUntilDone: YES];
			returnValue = YES;//Everything seems to be fine.
		}
	}
	AUTORELEASE(collectorId);
	return returnValue;
}

/*Method to be executed in the connectionThread.*/
- (void)internalUnsubscribeToCollector:(id)arg
{
	NSArray *arguments = (NSArray *)arg;
	[arguments retain];
	id origin = (id<DIMVAggregator>)[arg objectAtIndex: 0];
	NSString *collectorId = (NSString *)[arg objectAtIndex: 1];
	[origin unsubscribeComponent: clientId toCollector: collectorId];
	[arguments release];
}

- (BOOL)unsubscribeToCollector: (NSString *)collectorId
{
	CollectorRegister *colReg;
	id origin;
	NSArray *args;
	BOOL returnValue = NO;
	RETAIN(collectorId);
	colReg = [registeredCollectors objectForKey: collectorId];
	if(colReg == nil){
		NSLog(@"ERROR: Impossible to unsubscribe to collector with id \"%@\". There's no registered collector with this Id.");
	}else{
		origin = (id<DIMVAggregator>)[aggregators 
		  objectForKey: [colReg getOriginId]];
		if(origin == nil){//This shouldn't happen.
			NSLog(@"ERROR: Unsubscription error. The registered origin of the collector is not a registered aggregator.");
		}else{
/*			[origin unsubscribeComponent: clientId 
			  toCollector: collectorId];*/
			args = [NSArray arrayWithObjects: origin, collectorId,
			  nil];
			[self performSelector: 
			  @selector(internalUnsubscribeToCollector:)
			  onThread: connectionThread withObject: args
			  waitUntilDone: YES];
			returnValue = YES;
		}
	}
	AUTORELEASE(collectorId);
	return returnValue;
}

- (BOOL)setSubscriptionHandler: (id<HCMSubscriptionHandler>)aHandler
{
	CollectorRegister *colReg;
	subscriptionHandler = [aHandler retain];
	[subscriptionHandler setClient: self];
	NSEnumerator *objectEnum = [registeredCollectors objectEnumerator];
	[objectEnum retain];
	while((colReg = [objectEnum nextObject])!=nil){
		[subscriptionHandler 
		  registerCollectorWithId: [colReg getCollectorId]
		  type: [colReg getCollectorType]
		  location: [colReg getCollectorLocation]];
	}
	[objectEnum release];
	return YES;
}

@end
