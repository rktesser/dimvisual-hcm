/*
   Copyright (c) 2005 Lucas Mello Schnorr <schnorr@gmail.com>
   
   This file is part of DIMVisual.
   
   DIMVisual is free software; you can redistribute it and/or modify it under
   the terms of the GNU Lesser General Public License as published by the
   Free Software Foundation; either version 2 of the License, or (at your
   option) any later version.
   
   DIMVisual is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
   for more details.
   
   You should have received a copy of the GNU Lesser General Public License
   along with DIMVisual; if not, write to the Free Software Foundation, Inc.,
   59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/
#include "MPIRASTROConverter.h"

static int size = -1;

@implementation MPIRASTROConverter (All)
- (id) convertEventAll: (GEvent *) event
{
	NSMutableArray *ret = nil;
	NSString *container;
	int i;

	if (alltoall == nil){
		alltoall = [[AllToOne alloc] init];
	}
	if (alltoallv == nil){
		alltoallv = [[AllToOne alloc] init];
	}

	GFields *f = [event fields];

	NSString *machine, *thread, *node, *identifier, *time;
	NSString *int32AtIndex1, *members;

        machine = [[f fieldWithSimpleStringKey:@"machine"] description];
        thread = [[f fieldWithSimpleStringKey: @"thread"] description];
        node = [[f fieldWithSimpleStringKey: @"node"] description];
        identifier = [[event identifier] description];
	time = [[event timestamp] description];

	int32AtIndex1 = [[f fieldWithSimpleStringKey: @"int32-1"] description];
	members = [[f fieldWithSimpleStringKey: @"string-0"] description];
	
	ret = [[NSMutableArray alloc] init];

	container = [provider identifierForEntity: 
		[NSArray arrayWithObjects: @"Machine", @"Thread", nil]
				ids:
		[NSArray arrayWithObjects: machine, thread, nil]];

	if (container == nil){
		[ret release];
		return nil;
	}

	int etype = atoi ([identifier cString]);

	
/*******************************************************************************
	 * MPI_ALLTOALL
*******************************************************************************/
	if (etype == MPI_ALLTOALL_IN){
		if (size == -1){
			size = atoi([int32AtIndex1 cString]);
			for (i = 0; i < size; i++){
				AllToOne *x = [[AllToOne alloc] init];
				[alltoall addObject: x];
				[x release];
			}
		}
		for (i = 0; i < size; i++){
			[[alltoall objectAtIndex:i] memberEntering: container
				andEventNode: node
				andEventTime: time
				andRootNode: [NSString stringWithFormat:@"%d",i]
				andCommSize: int32AtIndex1
				andMembersId: members];
		}
		[ret addObject: [self addPajeSetState: @"MPI_ALLTOALL" time: time container: container]];
	}else if (etype == MPI_ALLTOALL_OUT){

		for (i = 0; i < size; i++){
			[ret addObjectsFromArray:
				[[alltoall objectAtIndex:i]
					memberExiting: container
					andEventTime: time
					andOperationName: @"alltoall"
					andRootNode: [NSString stringWithFormat:@"%d",i] 
					andMembersId: members
					andProvider: provider]];
		}
		[ret addObject: [self addPajeSetState: @"running" time: time container: container]];

/*******************************************************************************
	 * MPI_ALLTOALLV
*******************************************************************************/
	}else if (etype == MPI_ALLTOALLV_IN){
		if (size == -1){
			size = atoi([int32AtIndex1 cString]);
			for (i = 0; i < size; i++){
				AllToOne *x = [[AllToOne alloc] init];
				[alltoallv addObject: x];
				[x release];
			}
		}
		for (i = 0; i < size; i++){
			[[alltoallv objectAtIndex:i] memberEntering: container
				andEventNode: node
				andEventTime: time
				andRootNode: [NSString stringWithFormat:@"%d",i]
				andCommSize: int32AtIndex1
				andMembersId: members];
		}
		[ret addObject: [self addPajeSetState: @"MPI_ALLTOALLV" time: time container: container]];
	}else if (etype == MPI_ALLTOALLV_OUT){
		for (i = 0; i < size; i++){
			[ret addObjectsFromArray:
				[[alltoallv objectAtIndex:i]
					memberExiting: container
					andEventTime: time
					andOperationName: @"alltoallv"
					andRootNode: [NSString stringWithFormat:@"%d",i]
					andMembersId: members
					andProvider: provider]];
		}
		[ret addObject: [self addPajeSetState: @"running" time: time container: container]];

/*******************************************************************************
	 * MPI_BARRIER
*******************************************************************************/
	}else if (etype == MPI_BARRIER_IN){
		[ret addObject: [self addPajeSetState: @"MPI_BARRIER" time: time container: container]];
	}else if (etype == MPI_BARRIER_OUT){
		[ret addObject: [self addPajeSetState: @"running" time: time container: container]];
	}

	[ret autorelease];
	return ret;
}
@end
