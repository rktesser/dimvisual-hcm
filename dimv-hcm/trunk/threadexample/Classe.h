#import <Foundation/Foundation.h>

@protocol Classe <NSObject>
- (void)imprime: (id)arg;
- (void) registraRemoto: (id<Classe>) umObjeto;
@end

@interface Classe: NSObject <Classe>
{
	NSConditionLock * initLock;
	NSThread *connecThread;
	id<Classe> objetoRemoto;
}
- (void)fazAlgo: (id)arg;
- (void)imprime: (id)arg;
- (void)registraRemoto: (id<Classe>) umObjeto;
- (void)imprimeRemoto: (NSString *)mensagem;
@end


