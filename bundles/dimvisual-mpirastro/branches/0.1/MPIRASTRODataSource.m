/*
   Copyright (c) 2005 Lucas Mello Schnorr <schnorr@gmail.com>
   
   This file is part of DIMVisual.
   
   DIMVisual is free software; you can redistribute it and/or modify it under
   the terms of the GNU Lesser General Public License as published by the
   Free Software Foundation; either version 2 of the License, or (at your
   option) any later version.
   
   DIMVisual is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
   for more details.
   
   You should have received a copy of the GNU Lesser General Public License
   along with DIMVisual; if not, write to the Free Software Foundation, Inc.,
   59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/
#include "MPIRASTRODataSource.h"

@implementation MPIRASTRODataSource
- (id) initWithConfiguration: (NSDictionary *) configuration provider: (id<Integrator>) provider
{
	self = [super init];

	/* Extract information from configuration provided */
	id files;
	id nodes; 
	id param;
	id ident;
	id syncs;

	if (configuration == nil){
		NSLog (@"MPIRASTRODataSource: configuration '%@'", configuration);
		[self release];
		return nil;
	}
	if (provider == nil){
		NSLog (@"MPIRASTRODataSource: provider '%@'", provider);
		[self release];
		return nil;
	}
	ident = [configuration objectForKey: @"id"];
	if (ident == nil || [ident isEqual: @"MPIRASTRO"] == NO){
		NSLog (@"MPIRASTRODataSource: configuration '%@' hasn't a field "
			"'id' or is different from 'MPIRASTRO'", configuration);
		[self release];
		return nil;
	}
	param = [configuration objectForKey: @"parameters"];
	if (param == nil || [param isKindOfClass: [NSDictionary class]] == NO){
		NSLog (@"MPIRASTRODataSource: configuration '%@' hasn't a field "
			"'parameters' or is not a NSArray", configuration);
		[self release];
		return nil;
	}
	files = [param objectForKey: (id)@"files"];
	if (files == nil || [files isKindOfClass: [NSArray class]] == NO){
		NSLog (@"MPIRASTRODataSource: configuration '%@' hasn't a field "
			"'files' or is not a NSArray", configuration);
		[self release];
		return nil;
	}
	/*
	nodes = [param objectForKey: @"nodes"];
	if (nodes == nil || [nodes isKindOfClass: [NSString class]] == NO){
		NSLog (@"MPIRASTRODataSource: configuration '%@' hasn't a field "
			"'nodes' or is not a NSString", configuration);
		[self release];
		return nil;
	}
	*/
	syncs = [param objectForKey: @"sync"];
	if (syncs == nil || [syncs isKindOfClass: [NSArray class]] == NO){
		NSLog (@"MPIRASTRODataSource: configuration '%@' hasn't a field "
			"'sync' or is not a NSString", configuration);
		[self release];
		return nil;
	}

	//NSLog (@"%@ %@ %@", [files class], [nodes class], [syncs class]);
	//NSLog (@"%@ %@ %@", files, nodes, syncs);
	
	/* Instantiate the reader and the converter */
//	reader = [[MPIRASTROFileReader alloc] initWithFileName: files
//			andNodesFile: nodes andSyncFileName: syncs];
	reader = [[MPIRASTROFileReader alloc] initWithMultipleFiles: files
			andSyncFileName: [syncs objectAtIndex: 0]];
	if (reader == nil){
		NSLog (@"MPIRASTRODataSource: couldn't create a MPIRASTROFileReader with "
			"files='%@', nodes='%@' and sync='%@'",
			files, nodes, syncs);
		return nil;
	}

	converter = [[MPIRASTROConverter alloc] initWithProvider: provider];
	if (converter == nil){
		NSLog (@"MPIRASTRODataSource: couldn't create a MPIRASTROConverter with "
			"provider='%@'", provider);
		[reader release];
		return nil;
	}
	return self;
}

- (NSString *) time;
{
	return [reader time];
}

- (id) convert
{
	id event;
	id ret = nil;

	event = [reader event];
//	NSLog (@"%@ %d", [event type], [event retainCount]);
//	[event retain];
	if (event == nil){
		return nil;
	}
	ret = [converter convertEvent: event];
//	[event release];
//	NSLog (@"%@ %d", [event type], [event retainCount]);
	return ret; 
}

- (void) dealloc
{
	[reader release];
	[converter release];
	[super dealloc];
}

- (NSDictionary *) configuration
{
	NSMutableDictionary *c;
	NSMutableDictionary *parameters;
//	NSArray *entities;

	parameters = [[NSMutableDictionary alloc] init];
	
	[parameters setObject: @"multiple" forKey: @"files"];
//	[parameters setObject: @"single" forKey: @"nodes"];
	[parameters setObject: @"single" forKey: @"sync"];
		
	c = [[NSMutableDictionary alloc] init];
	[c setObject: @"MPIRASTRO" forKey: @"id"];
	[c setObject: parameters forKey: @"parameters"];
	[parameters release];
//	NSLog (@"OI");
//	entities = [converter entities];
//	[entities retain];
//	NSLog (@"%@", entities);
//	[c setObject: entities forKey: @"entities"];
//	[entities release];
	[c autorelease];
	return c;
}
@end
