#import "DIMVAppConf.h"

@implementation DIMVAppConf
- (DIMVAppConf *) initWithConfFile: (NSString *)confFileName
{
	NSDictionary *conf;
	
	self = [super init];
	RETAIN(confFileName);
	conf = [NSDictionary dictionaryWithContentsOfFile: confFileName];
	RETAIN(conf);
	RELEASE(confFileName);

	aggregatorId = [conf objectForKey: @"id"];
	RETAIN(aggregatorId);
	name = [conf objectForKey: @"name"];
	RETAIN(name);
	aggregators = [conf objectForKey: @"aggregators"];
	RETAIN(aggregators);

	RELEASE(conf);
	return self;
}

- (void) dealloc
{
	RELEASE(aggregatorId);
	RELEASE(name);
	RELEASE(aggregators);
	[super dealloc];
}

- (NSString *) getName
{
	RETAIN(name);
	AUTORELEASE(name);
	return name;
}

- (NSString *) getAggregatorId
{
	RETAIN(aggregatorId);
	AUTORELEASE(aggregatorId);
	return aggregatorId;

}


- (NSArray *) getAggregators
{

	RETAIN(aggregators);
	AUTORELEASE(aggregators);
	return aggregators;
}

@end

