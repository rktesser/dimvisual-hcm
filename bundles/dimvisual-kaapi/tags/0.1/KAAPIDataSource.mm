/*
    DIMVisual-KAAPI, the DIMVisual bundle for KAAPI trace files
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual-KAAPI.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "KAAPIDataSource.h"


@implementation KAAPIDataSource
- (id) initWithConfiguration: (NSDictionary *) configuration provider: (id<Integrator>) provider
{
	self = [super init];

	/* Extract information from configuration provided */
	id files;
	id param;
	id ident;
	id sync;
	unsigned int i;

	if (configuration == nil){
		NSLog (@"KAAPIDataSource: configuration '%@'", configuration);
		[self release];
		return nil;
	}
	if (provider == nil){
		NSLog (@"KAAPIDataSource: provider '%@' (Error)", provider);
		[self release];
		return nil;
	}
	ident = [configuration objectForKey: IDKEY];
	if (ident == nil || [ident isEqual: IDVALUE] == NO){
		NSLog (@"KAAPIDataSource: configuration '%@' hasn't a field "
			"'id' or is different from 'KAAPI'", configuration);
		[self release];
		return nil;
	}
	param = [configuration objectForKey: PARAMETERKEY];
	if (param == nil || [param isKindOfClass: [NSDictionary class]] == NO){
		NSLog (@"KAAPIDataSource: configuration '%@' hasn't a field "
			"'%@' or is not a NSArray", configuration,PARAMETERKEY);
		[self release];
		return nil;
	}
	files = [param objectForKey: (id)FILEKEY];
	if (files == nil || [files isKindOfClass: [NSArray class]] == NO){
		NSLog (@"KAAPIDataSource: configuration '%@' hasn't a field "
			"'%@' or is not a NSArray", configuration, FILEKEY);
		[self release];
		return nil;
	}
	sync = [param objectForKey: (id)SYNCKEY];
	if ([sync isKindOfClass: [NSArray class]] && [sync count] >= 1){
		sync = [sync objectAtIndex: 0];
	}
	if (sync == nil || [sync isKindOfClass: [NSString class]] == NO){
		NSLog (@"KAAPIDataSource: configuration '%@' hasn't a field "
			"'%@' or is not a NSString", configuration, SYNCKEY);
		[self release];
		return nil;
	}



	/* Instantiate the readers */
	NSLog (@"KAAPIDataSource: Opening trace files...");
	reader = [[KAAPIOrder alloc] init];
	for (i = 0; i < [files count]; i++){
		KAAPIFileReader *oneReader = [[KAAPIFileReader alloc] initWithFileName: [files objectAtIndex: i] andSyncFileName: sync];
		if (oneReader == nil){
			NSLog (@"KAAPIDataSource: couldn't create a KAAPIFileReader with file='%@' and sync='%@'", [files objectAtIndex: i], sync);
			return nil;
		}
		[reader add: oneReader];
		[oneReader release];
	}
	NSLog (@"KAAPIDataSource: mostRecent is %@", [[reader mostRecent] time]);

	/* and the converter */
	converter = [[KAAPIConverter alloc] initWithProvider: provider];
	if (converter == nil){
		NSLog (@"KAAPIDataSource: couldn't create a KAAPIConverter with "
			"provider='%@'", provider);
		[reader release];
		return nil;
	}
	return self;
}

- (GTimestamp *) time;
{
	return [[reader mostRecent] time];
}

- (id) convert
{
	KAAPIFileReader *oneReader = (KAAPIFileReader *)[reader mostRecent];
	id event = [oneReader event];
	if (event == nil){
		return nil;
	}
	return [converter convertEvent: event];
}

- (void) dealloc
{
	[reader release];
	[converter release];
	[super dealloc];
}

- (NSDictionary *) configuration
{
	NSMutableDictionary *c;
	NSMutableDictionary *parameters;

	parameters = [[NSMutableDictionary alloc] init];
	
	[parameters setObject: @"NSArray" forKey: FILEKEY];
	[parameters setObject: @"NSString" forKey: SYNCKEY];
		
	c = [[NSMutableDictionary alloc] init];
	[c setObject: IDVALUE forKey: IDKEY];
	[c setObject: parameters forKey: PARAMETERKEY];
	[parameters release];
	[c autorelease];
	return c;
}

- (NSDictionary *) hierarchy
{
#ifdef ORGANIZATION_BY_PROCESS
	return [NSDictionary dictionaryWithContentsOfFile: [[NSBundle bundleForClass: [self class]] pathForResource: @"KAAPIConfiguration-process" ofType: @"plist"]];
#else
	return [NSDictionary dictionaryWithContentsOfFile: [[NSBundle bundleForClass: [self class]] pathForResource: @"KAAPIConfiguration-thread" ofType: @"plist"]];
#endif
}
@end
