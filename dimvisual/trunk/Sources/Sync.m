/*
    DIMVisual, the Data Integration Model for Visualization of trace files.
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "Sync.h"

@implementation Sync
- (id) initWithFileName: (NSString *) syncFile toHostname: (NSString *) host
{
	FILE *arq;
	char refhost[200];
	char othhost[200];
	long long reftimel, timel;
	int first = 1;

	self = [super init];

	if (syncFile == nil || host == nil){
		return nil;
	}

	if ([syncFile isKindOfClass: [NSString class]] == NO ||
		[host isKindOfClass: [NSString class]] == NO){
		return nil;
	}

	arq = fopen ((char *)[syncFile cString], "r");
	if (arq == NULL){
		return nil;
	}

//	NSLog (@"host => %@ syncFile => %@", host, syncFile);

	[myHost = host retain];


	myA = 1;
	myRef0 = 0;
	myLoc0 = 0;
	reftimel = 0;
	timel = 0;

	while (!feof (arq)){
		int x;
		x=fscanf(arq,"%s %lld %s %lld",refhost, &reftimel,othhost,&timel);
//		fprintf (stderr, "%s %lld %s %lld\n", refhost, reftimel, othhost, timel);
		if (x != 4){
			break;
		}
		if (feof(arq)){
			break;
		}
//		NSLog (@"%f %lld %lld", myA, myRef0, myLoc0);
		if (strcmp (refhost, [host cString]) == 0){
			myRef0 = reftimel;
			myLoc0 = reftimel;
			myA = 1;
			break;
		}
		if (strcmp (othhost, [host cString]) == 0){
			if (first){
				myRef0 = reftimel;
				myLoc0 = timel;
				first = 0;
			}else{
//				NSLog (@"# %qu %qu", (unsigned long
//				long)reftimel, (unsigned long
//				long)timel);
				
				myA = (double)(reftimel - myRef0) /
				(double)(timel - myLoc0);
//				fprintf (stderr,"%lld %lld\n", reftimel, timel);
//				fprintf (stderr,"%lld\n", reftimel-myRef0);
//				fprintf (stderr,"%lld\n", timel-myLoc0);
//				fprintf (stderr,"%f\n", myA);
			}
		}
	}
//	fprintf (stderr, "### %s %f %lld %lld\n", [host cString], myA, myRef0, myLoc0);
	fclose (arq);
	/* here we have the variable myA, myLoc0 and myRef0 completed */	
	return self;
}

- (NSString *) correctTime: (NSString *) timeToCorrect
{
	long long remote, local;
	char temp[100];
	NSString *ret;

//	fprintf (stderr, "a = %f loc = %lld ref = %lld\n", myA, myLoc0, myRef0);
	remote = atoll ((char *)[timeToCorrect cString]);
//	fprintf (stderr, "remote = %lld\n", remote);
	local = (remote - myLoc0);
	local = ((double)local * myA);
	local = local + myRef0;
//	fprintf (stderr, "local = %lld\n", local);
	sprintf (temp, "%lld", local);
//	fprintf (stderr, "temp = %s\n", temp);

	ret = [[NSString alloc] initWithCString: temp];
	[ret autorelease];
	return ret;
}

- (void) dealloc
{
	[super dealloc];
}

- (NSString *) host
{
	return myHost;
}

+ (NSArray *) allMachinesOfFileNamed: (NSString *) filename
{
	FILE *arq;
	char refhost[200];
	char othhost[200];
	NSMutableArray *ret;
	long long reftimel, timel;
	int n;
//	int first = 1;

	if (filename == nil){
		return nil;
	}

	if ([filename isKindOfClass: [NSString class]] == NO){
		return nil;
	}

	arq = fopen ((char *)[filename cString], "r");
	if (arq == NULL){
		return nil;
	}
	ret = [[NSMutableArray alloc] init];
	while (!feof (arq)){
		n = fscanf(arq,"%s %lld %s %lld", refhost, &reftimel, 
						othhost, &timel);
		if (n == 4){
			[ret addObject: [NSString stringWithCString: othhost]];
		}else if (n == -1){
			continue;
		}else{
			[ret release];
			return nil;
		}
	}
	fclose (arq);
	[ret autorelease];
	return ret;
}
@end
