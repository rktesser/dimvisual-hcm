/*
    DIMVisual-KAAPI, the DIMVisual bundle for KAAPI trace files
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual-KAAPI.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "KAAPIFileReader.h"

double gettime ()
{
        struct timeval tr;
        gettimeofday(&tr, NULL);
        return (double)tr.tv_sec+(double)tr.tv_usec/1000000;
}

@implementation KAAPIFileReader (Read)
- (BOOL) initializeWithTraceFile: (NSString *) traceFile 
		andDescriptionFile: (NSString *) descFile
{
	if (traceFile == nil || descFile == nil){
		return NO;
	}

        Util::RecordReader::Flag flag = Util::RecordReader::TRCFILE_FORMAT;
        input = new Util::RecordReader ([traceFile cString], flag);
	ibeg = input->begin();
	iend = input->end();
	return YES;
}

- (GEvent *) parseKAAPIEvent: (Util::RecordReader::const_iterator) r
{
	return [super parseKAAPIEvent: r];
}

- (GEvent *) read
{
	if (ibeg != iend){
		GEvent *ret = nil;
		while (ibeg != iend && ret == nil){
			ret = [self parseKAAPIEvent: ibeg];
			if (ret != nil){
				[self defineTimestampForEvent: ret];
				[self defineHostnameForEvent: ret];
				[self synchronizeEvent: ret];
				ibeg++;
				break;
			}else{
				ibeg++;
			}
		}
		if (ibeg == iend){
			return nil;
		}else{
			return ret;
		}
	}else{
		return nil;
	}
}

@end
