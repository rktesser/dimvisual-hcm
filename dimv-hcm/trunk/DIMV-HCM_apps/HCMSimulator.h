#import <Foundation/Foundation.h>
#import "AuxCopiableDictionary.h"
#import "DIMVAggregatorProto.h"
#import "DIMVClientProto.h"
#import "CollectorRegister.h"

#define MONDATABUFFERSIZE 100
#define MONDATABUFFERHASDATA 1
#define MONDATABUFFERISEMPTY 0

@interface HCMSimulator: NSObject <DIMVAggregator>
{
	NSString *myId;
	NSString *name;
	NSConnection *connection;
	NSMutableDictionary *clients;
	AuxCopiableDictionary *collectors;
	NSString *directoryName;
	NSMutableArray *metadata;
	NSMutableArray *dataBuffer;
	NSConditionLock *dataLock;
	NSCondition *needData;
	NSDate *startDate;
	NSFileHandle *dataFile;
}

- (HCMSimulator *)initWithId: (NSString *)anId andName: (NSString *)aName;
- (BOOL)simulateWithDataInDirectory: (NSString *)path;

@end
