/*
   Copyright (c) 2005 Lucas Mello Schnorr <schnorr@gmail.com>
   
   This file is part of DIMVisual.
   
   DIMVisual is free software; you can redistribute it and/or modify it under
   the terms of the GNU Lesser General Public License as published by the
   Free Software Foundation; either version 2 of the License, or (at your
   option) any later version.
   
   DIMVisual is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
   for more details.
   
   You should have received a copy of the GNU Lesser General Public License
   along with DIMVisual; if not, write to the Free Software Foundation, Inc.,
   59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/
#include "MPIRASTROConverter.h"

static OneToAll *bcast;

@implementation MPIRASTROConverter (Bcast)
- (id) convertEventBcast: (GEvent *) event
{
	GFields *f = [event fields];
	NSString *machine, *thread, *node, *identifier, *time;

	machine = [[f fieldWithSimpleStringKey:@"machine"] description];
	thread = [[f fieldWithSimpleStringKey: @"thread"] description];
	node = [[f fieldWithSimpleStringKey: @"node"] description];
	identifier = [[event identifier] description];
	time = [[event timestamp] description];


	NSMutableArray *ret = [[NSMutableArray alloc] init];
	NSString *container = [provider identifierForEntity: 
		[NSArray arrayWithObjects: @"Machine", @"Thread", nil]
				ids:
		[NSArray arrayWithObjects: machine, thread, nil]];

	if (bcast == nil){
		bcast = [[OneToAll alloc] init];
	}

	if (container == nil){
		[ret release];
		return nil;
	}
	
	int etype = atoi ([identifier cString]);
	/* MPI_BCAST_IN */
	if (etype == MPI_BCAST_IN){
		NSString *count, *size, *root, *members;

		count = [[f fieldWithSimpleStringKey: @"uint32-0"] description];
		size = [[f fieldWithSimpleStringKey: @"uint32-1"] description];
		root = [[f fieldWithSimpleStringKey: @"uint32-2"] description];
		members=[[f fieldWithSimpleStringKey: @"string-0"] description];
		
		[bcast memberEntering: container
			andEventNode: node
			andEventTime: time
			andRootNode: root
			andCommSize: size
			andMembersId: members];

		PajeSetState *set;
		set = [self addPajeSetState: @"MPI_BCAST" time: time container: container];
		[set retain];
		[set addFieldNamed: @"COUNT" withType: @"string"];
		[set setValue: count forFieldNamed: @"COUNT"];
		[ret addObject: set];
		[set release];
	
	/* MPI_BCAST_OUT */
	}else if (etype == MPI_BCAST_OUT){
		NSString *root, *members;

		root = [[f fieldWithSimpleStringKey: @"uint32-0"] description];
		members=[[f fieldWithSimpleStringKey: @"string-0"] description];

		[ret addObjectsFromArray: [bcast memberExiting: container andEventTime: time andOperationName: @"bcast" andRootNode: root andMembersId: members andProvider: provider]];
		[ret addObject: [self addPajeSetState: @"running" time: time container: container]];
	}

	[ret autorelease];
	return ret;
}
@end
