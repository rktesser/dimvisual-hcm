/*
    DIMVisual-KAAPI, the DIMVisual bundle for KAAPI trace files
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual-KAAPI.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "KAAPIParser.h"

@implementation KAAPIParser (Kernel)
- (GEvent *) parseKAAPIKernelLevel: (Util::Record *) r
	withIdentifier: (GIdentifier *) identifier
	withTimestamp: (GTimestamp *) timestamp
{
	GName *name = [[GName alloc] init]; 
	GFields *fields = [[GFields alloc] init];
	GIdentifier *nameid = [[GIdentifier alloc] init];
	GIdentifier *nameid2 = [[GIdentifier alloc] init];

	switch (r->_event){
		case Event::CORE_THREAD_RUN:
			[name setName: @"EVT_CORE_THREAD_RUN"];
			break;
		case Event::CORE_THREAD_COMPUTE:
			[name setName: @"EVT_CORE_THREAD_COMPUTE"];
			break;
		case Event::CORE_THREAD_SUSPEND:
			[name setName: @"EVT_CORE_THREAD_SUSPEND"];
			break;
		case Event::CORE_THREAD_WAKEUP:
			[name setName: @"EVT_CORE_THREAD_WAKEUP"];
			break;
		case Event::CORE_THREAD_STEAL:
			[name setName: @"EVT_CORE_THREAD_STEAL"];
			break;
		case Event::CORE_THREAD_TERM:
			[name setName: @"EVT_CORE_THREAD_TERM"];
			break;
		case Event::CORE_THREAD_SCHED:
			[name setName: @"EVT_CORE_THREAD_SCHED"];
			{
			EVT_CORE_THREAD_SCHED_Record *kaapiev;
			kaapiev = (EVT_CORE_THREAD_SCHED_Record *)r;

			NSString *value = [NSString stringWithFormat: @"%l", kaapiev->processorid];
			[nameid addValue: @"processorid"];

			[fields setFieldWithKey: nameid withValue: value];
			}
			break;
		case Event::CORE_THREAD_SIGNALWAKEUP:
			[name setName: @"EVT_CORE_THREAD_SIGNALWAKEUP"];
			{
			EVT_CORE_THREAD_SIGNALWAKEUP_Record *kaapiev;
			kaapiev = (EVT_CORE_THREAD_SIGNALWAKEUP_Record *)r;

			NSString *value = [NSString stringWithFormat: @"%l", kaapiev->victim];
			[nameid addValue: @"victim"];

			[fields setFieldWithKey: nameid withValue: value];
			}

			break;
		case Event::CORE_THREAD_LSTEAL:
			[name setName: @"EVT_CORE_THREAD_LSTEAL"];
			{
			EVT_CORE_THREAD_LSTEAL_Record *kaapiev;
			kaapiev = (EVT_CORE_THREAD_LSTEAL_Record *)r;
			
			NSString *value = [NSString stringWithFormat: @"%l", kaapiev->victim];
			[nameid addValue: @"victim"];

			[fields setFieldWithKey: nameid withValue: value];
			}

			break;
		case Event::CORE_THREAD_RETLSTEAL:
			[name setName: @"EVT_CORE_THREAD_RETLSTEAL"];
			{
			EVT_CORE_THREAD_RETLSTEAL_Record *kaapiev;
			kaapiev = (EVT_CORE_THREAD_RETLSTEAL_Record *)r;

			NSString *value = [NSString stringWithFormat: @"%l", kaapiev->victim];
			NSString *value2 = [NSString stringWithFormat: @"%hd", kaapiev->retval];

			[nameid addValue: @"victim"];
			[nameid2 addValue: @"retval"];	

			[fields setFieldWithKey: nameid withValue: value];
			[fields setFieldWithKey: nameid2 withValue: value2];
			}

			break;
		case Event::CORE_THREAD_RSTEAL:
			[name setName: @"EVT_CORE_THREAD_RSTEAL"];
			{
			EVT_CORE_THREAD_RSTEAL_Record *kaapiev;
			kaapiev = (EVT_CORE_THREAD_RSTEAL_Record *)r;

			NSString *value = [NSString stringWithFormat: @"%i", kaapiev->gidvictim];
	
			[nameid addValue: @"gidvictim"];

			[fields setFieldWithKey: nameid withValue: value];
			}

			break;
		case Event::CORE_THREAD_RETRSTEAL:
			[name setName: @"EVT_CORE_THREAD_RETRSTEAL"];
			{
			EVT_CORE_THREAD_RETRSTEAL_Record *kaapiev;
			kaapiev = (EVT_CORE_THREAD_RETRSTEAL_Record *)r;

			NSString *value = [NSString stringWithFormat: @"%i", kaapiev->gidvictim];
			NSString *value2 = [NSString stringWithFormat: @"%hd", kaapiev->retval];

			[nameid addValue: @"gidvictim"];
			[nameid2 addValue: @"retval"];

			[fields setFieldWithKey: nameid withValue: value];
			[fields setFieldWithKey: nameid2 withValue: value2];
			}

			break;
		default:
			NSLog (@"Warning: Kernel_Level -> type = %d, not completely translated", r->_event);
			break;
	}
	GEvent *ev = [[GEvent alloc] init];
	[ev setTimestamp: timestamp];
	[ev setIdentifier: identifier];
	[ev setName: name];
	[ev setFields: fields];
	[ev autorelease];

	[name release];
	[fields release];
	[nameid release];
	[nameid2 release];

	return ev;
}

@end

