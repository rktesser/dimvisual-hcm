/*
    DIMVisual, the Data Integration Model for Visualization of trace files.
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#import <Foundation/Foundation.h>
#import "Protocols.h"
#import "Ordering.h"

#define CONDQUEUEEMPTY 0
#define CONDQUEUEHASDATA 1

@interface BinaryMinHeap : NSObject <Time>
{
	NSMutableArray *sources;
	id lastRemoved;
	NSMutableArray *DSQueue;//Stores wich are waiting to be added.
	NSConditionLock *DSQueueLock;
	NSMutableArray *HCMDSReg;
}
- (void) orderToTopIndex: (int) i;
- (void) orderToBottomIndex: (int) i;
- (id) mostRecent;
- (void) add: (id<Time>) source;
- (id<Time>) remove;
- (void)enqueueDS: (id<HCMDataSource>)ds;
- (id<HCMDataSource>) unqueueDS;
- (void)registerHCMDataSource: (id<HCMDataSource>) ds;
- (unsigned int) count;
@end

