#import <stdio.h>
#import <stdlib.h>
#import <unistd.h>
#import <string.h>
#import<errno.h>
#import<netdb.h>
#import <sys/socket.h>
#import <netinet/in.h>
#import <arpa/inet.h>
#import<Foundation/NSAutoreleasePool.h>

#define GANGLIA_MAX_RECV_SIZE  100000

/*This function connects to port in a host and receives data from it,
 * which is stored in buf. It returns 0 in case of succes*/
int getGangliaData(char *host, char *port, char * buf);
