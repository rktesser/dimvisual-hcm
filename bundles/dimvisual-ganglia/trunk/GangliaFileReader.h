/*
    DIMVisual-Ganglia, the DIMVisual bundle for Ganglia trace files
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual-Ganglia.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __GangliaFILEREADER__
#define __GangliaFILEREADER__
#include <Foundation/Foundation.h>
#include <DIMVisual/Protocols.h>
#include <DIMVisual/Sync.h>
#include <GenericEvent/GEvent.h>
#include <GenericEvent/GEvent.h>
#include <GNUstepBase/GSXML.h>

@class GangliaDataSource;

@interface GangliaFileReader : NSObject <FileReader>
{
	GEvent *topEvent;
	NSMutableDictionary *syncs;
	NSMutableArray *buf;
	Sync *sync;
	GangliaDataSource *provider;

	double simpleTime;

	GSXMLParser *parser;
	GSXMLDocument *document;
	GSXMLNode *current;

	NSMutableArray *events; /* of GEvent */
}
- (id) initWithFileName: (NSString *) traceFile
	andSyncFileName: (NSString *) syncFile
	andProvider: (GangliaDataSource *) prov;
- (NSString *) filename;
- (void) readingWithNode: (GSXMLNode *) node;
- (GEvent *) eventWithHostname: (NSString *) hostname
                        andReported: (NSString *) reported
                        andName: (NSString *) name
                        andValue: (NSString *) value;
@end

#include "GangliaDataSource.h"
#endif
