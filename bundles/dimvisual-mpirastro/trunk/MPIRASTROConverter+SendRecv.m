/*
   Copyright (c) 2005 Lucas Mello Schnorr <schnorr@gmail.com>
   
   This file is part of DIMVisual.
   
   DIMVisual is free software; you can redistribute it and/or modify it under
   the terms of the GNU Lesser General Public License as published by the
   Free Software Foundation; either version 2 of the License, or (at your
   option) any later version.
   
   DIMVisual is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
   for more details.
   
   You should have received a copy of the GNU Lesser General Public License
   along with DIMVisual; if not, write to the Free Software Foundation, Inc.,
   59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/
#include "MPIRASTROConverter.h"
#include "OneToOne.h"

static OneToOne *sendrecv;
static NSMutableDictionary *async;

@implementation MPIRASTROConverter (SendRecv)
- (id) convertEventSendRecv: (GEvent *) event 
{
	GFields *f = [event fields];

        NSString *machine, *thread, *node, *identifier;
	NSString *time, *int32AtIndex0, *int32AtIndex1, *int32AtIndex2;

	machine = [[f fieldWithSimpleStringKey:@"machine"] description];
	thread = [[f fieldWithSimpleStringKey: @"thread"] description];
	node = [[f fieldWithSimpleStringKey: @"node"] description];
	identifier = [[event identifier] description];
	time = [[event timestamp] description];
	int32AtIndex0 = [[f fieldWithSimpleStringKey: @"uint32-0"] description];
	int32AtIndex1 = [[f fieldWithSimpleStringKey: @"uint32-1"] description];
	int32AtIndex2 = [[f fieldWithSimpleStringKey: @"uint32-2"] description];

	NSMutableArray *ret = [[NSMutableArray alloc] init];
	NSString *container = [provider identifierForEntity: 
		[NSArray arrayWithObjects: @"Machine", @"Thread", nil]
				ids:
		[NSArray arrayWithObjects:machine,thread,nil]];

	if (async == nil){
		async = [[NSMutableDictionary alloc] init];
	}
	if (sendrecv == nil){
		sendrecv = [[OneToOne alloc] init];
	}
	if (container == nil){
		[ret release];
		return nil;
	}
	
	int etype = atoi ([identifier cString]);

	/* MPI_SEND_IN */
        if (etype == MPI_SEND_IN){
		NSString *count = int32AtIndex0;
		NSString *tag = int32AtIndex1;
		NSString *receiver = int32AtIndex2;
		NSString *sender = node;

		[ret addObjectsFromArray: 
			[sendrecv containerSend: container
			andSenderNode: sender
			andReceiverNode: receiver
			andTime: time
			andOperation: @"send-recv"
			andTag: tag
			andCount: count
			andProvider: provider]];

		PajeSetState *set;
		set = [self addPajeSetState: @"MPI_SEND"
					time: time
					container: container];
		[set retain];
//		[set addFieldNamed: @"TAG" withType: @"string"];
//		[set addFieldNamed: @"COUNT" withType: @"string"];
//		[set setValue: tag forFieldNamed: @"TAG"];
//		[set setValue: count forFieldNamed: @"COUNT"];
		[ret addObject: set];
		[set release];
	
	/* MPI_ISEND_IN */
	}else if (etype == MPI_ISEND_IN){
		NSString *count = int32AtIndex0;
		NSString *tag = int32AtIndex1;
		NSString *receiver = int32AtIndex2;
		NSString *sender = node;
		
		[ret addObjectsFromArray: 
			[sendrecv containerSend: container
			andSenderNode: sender
			andReceiverNode: receiver
			andTime: time
			andOperation: @"send-recv"
			andTag: tag
			andCount: count
			andProvider: provider]];

		PajeSetState *set;
		set = [self addPajeSetState: @"MPI_ISEND"
					time: time
					container: container];
		[ret addObject: set];

	/* MPI_RECV_IN */
        }else if (etype == MPI_RECV_IN){
		PajeSetState *set;
		set = [self addPajeSetState: @"MPI_RECV"
					time: time
					container: container];
		[ret addObject: set];
	
	/* MPI_IRECV_IN */
	}else if (etype == MPI_IRECV_IN){
		/*
		NSString *count = int32AtIndex0;
		NSString *tag = int32AtIndex1;
		NSString *sender = int32AtIndex2;
	
		[async setObject: sender forKey: container];
		*/

		PajeSetState *set;
		set = [self addPajeSetState: @"MPI_IRECV"
					time: time
					container: container];
		[ret addObject: set];

	
	/* MPI_WAIT_IN */
	}else if (etype == MPI_WAIT_IN){
		[ret addObject: [self addPajeSetState: @"MPI_WAIT" time: time container: container]];
	
	/* MPI_IRECV_OUT */
	}else if (etype == MPI_IRECV_OUT){
		[ret addObject: [self addPajeSetState: @"running" time: time container: container]];

	/* MPI_WAIT_OUT */
	}else if (etype == MPI_WAIT_OUT){
		/*
		NSString *sender = [async objectForKey: container];
		if (sender != nil){
			[ret addObjectsFromArray:
				[sendrecv containerReceive: container
					andReceiverNode: node
					andSenderNode: sender
					andTime: time
					andOperation: @"send-recv"
					andProvider: provider]];
			[async removeObjectForKey: container];
		}else{
			NSLog (@"Sender is nil");
		}	
		*/
		[ret addObject: [self addPajeSetState: @"running" time: time container: container]];
	/* MPI_SEND_OUT */
	}else if (etype == MPI_SEND_OUT){
		[ret addObject: [self addPajeSetState: @"running" time: time container: container]];

	/* MPI_RECV_OUT */
	}else if (etype == MPI_RECV_OUT){
		NSString *sender = int32AtIndex0;
		NSString *receiver = node;
		
		NSMutableArray *x = 
			[sendrecv containerReceive: container
				andReceiverNode: receiver
				andSenderNode: sender
				andTime: time
				andOperation: @"send-recv"
				andProvider: provider];
		[ret addObjectsFromArray: x];
		[ret addObject: [self addPajeSetState: @"running" time: time container: container]];

		/* not instrumented below 
	}else if (etype == MPI_BSEND_IN){
		[ret addObject: [self addPajeSetState: @"MPI_BSEND" time: time container: container]];
      }else if (etype == MPI_BSEND_INIT_IN){
		[ret addObject: [self addPajeSetState: @"MPI_BSEND_INIT" time: time container: container]];
      }else if (etype == MPI_RECV_INIT_IN){
		[ret addObject: [self addPajeSetState: @"MPI_RECV_INIT" time: time container: container]];
      }else if (etype == MPI_SSEND_IN){
		[ret addObject: [self addPajeSetState: @"MPI_SSEND" time: time container: container]];
      }else if (etype == MPI_SEND_INIT_IN){
		[ret addObject: [self addPajeSetState: @"MPI_SEND_INIT" time: time container: container]];
      }else if (etype == MPI_IBSEND_IN){
		[ret addObject: [self addPajeSetState: @"MPI_IBSEND" time: time container: container]];
      }else if (etype == MPI_IPROBE_IN){
		[ret addObject: [self addPajeSetState: @"MPI_IPROBE" time: time container: container]];
      }else if (etype == MPI_IRSEND_IN){
		[ret addObject: [self addPajeSetState: @"MPI_IRSEND" time: time container: container]];
      }else if (etype == MPI_ISSEND_IN){
		[ret addObject: [self addPajeSetState: @"MPI_ISSEND" time: time container: container]];
      }else if (etype == MPI_PROBE_IN){
		[ret addObject: [self addPajeSetState: @"MPI_PROBE" time: time container: container]];
      }else if (etype == MPI_RSEND_IN){
		[ret addObject: [self addPajeSetState: @"MPI_RSEND" time: time container: container]];
      }else if (etype == MPI_RSEND_INIT_IN){
		[ret addObject: [self addPajeSetState: @"MPI_RSEND_INIT" time: time container: container]];
      }else if (etype == MPI_SENDRECV_IN){
		[ret addObject: [self addPajeSetState: @"MPI_SENDRECV" time: time container: container]];
      }else if (etype == MPI_SENDRECV_REPLACE_IN){
		[ret addObject: [self addPajeSetState: @"MPI_SENDRECV_REPLACE" time: time container: container]];
      }else if (etype == MPI_SSEND_INIT_IN){
		[ret addObject: [self addPajeSetState: @"MPI_SSEND_INIT" time: time container: container]];
      }else if (etype == MPI_WAITALL_IN){
		[ret addObject: [self addPajeSetState: @"MPI_WAITALL" time: time container: container]];
      }else if (etype == MPI_WAITANY_IN){
		[ret addObject: [self addPajeSetState: @"MPI_WAITANY" time: time container: container]];
      }else if (etype == MPI_WAITSOME_IN){
		[ret addObject: [self addPajeSetState: @"MPI_WAITSOME" time: time container: container]];
      }else if (etype == MPI_ISEND_OUT
			|| etype == MPI_IRSEND_OUT
			|| etype == MPI_BSEND_OUT
			|| etype == MPI_BSEND_INIT_OUT
			|| etype == MPI_RECV_INIT_OUT
			|| etype == MPI_SEND_INIT_OUT
			|| etype == MPI_IBSEND_OUT
			|| etype == MPI_ISSEND_OUT
			|| etype == MPI_RSEND_OUT
			|| etype == MPI_RSEND_INIT_OUT
			|| etype == MPI_SENDRECV_OUT
			|| etype == MPI_SENDRECV_REPLACE_OUT
			|| etype == MPI_SSEND_OUT
			|| etype == MPI_SSEND_INIT_OUT
			|| etype == MPI_WAITALL_OUT
			|| etype == MPI_WAITANY_OUT
			|| etype == MPI_WAITSOME_OUT){
		[ret addObject: [self addPajeSetState: @"running" time: time container: container]];
*/
	}
	[ret autorelease];
	return ret;
}
@end

