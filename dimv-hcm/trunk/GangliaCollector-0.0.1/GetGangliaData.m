#import "GetGangliaData.h"

int getGangliaData(char *host, char *port, char * buf)
{

	int ret, n, total, unused_buf;
	struct addrinfo hints, *res; 
	int sockfd;
	
	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	if ((ret = getaddrinfo(host, port, &hints, &res))){
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(ret));
		return -1;
	}
	sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
	if (connect(sockfd, res->ai_addr, res->ai_addrlen) == -1){
		close(sockfd);
		perror("getGangliaData : connect");
		freeaddrinfo(res);
		return -1;
	}

	//Now we receive data until the other side closes the connection
	//or an error happens or the buffer becomes full
	total = 0;
	unused_buf = GANGLIA_MAX_RECV_SIZE;
	while (((n = recv(sockfd, buf + total, unused_buf, 0)) > 0) &&
	  unused_buf){
		
		unused_buf -= n;//decrease
		total += n;
	}
	if (n < 0){
		perror("recv");
	}else if (unused_buf == 0){
		fprintf(stderr, "recv: The buffer is full!\n");
	}else if (n == 0){//Ganglia closes the connection after sending the xml
		buf[total] = 0;
		freeaddrinfo(res);
		return 0;
	}
	freeaddrinfo(res);
	return -1;
}
/*
int main(int argc, char **argv)
{
	int ret;
	char buf[GANGLIA_MAX_RECV_SIZE];
	DIMVCollector * aCollector
	CREATE_AUTORELEASE_POOL(pool);
	
	if (argc < 3){
		printf("Format:\n %s <host> <port>\n", argv[0]);
		return 1;
	}
	
	aCollector = [[DIMVCollector alloc] initWith
	if((ret = receive_data( argv[1], argv[2], buf)) == 0){
		printf("Dados:\n%s\n", buf);
	}

	RELEASE(pool);
	return 0;
}
*/
