#import "OutraClasse.h"
@implementation OutraClasse
- (void)fazAlgo: (id)arg
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
	objetoRemoto = (id<Classe>)[NSConnection rootProxyForConnectionWithRegisteredName: @"objetoRemoto" host: @"*" usingNameServer: [NSSocketPortNameServer sharedInstance]]; 
	if (!objetoRemoto){
		NSLog(@"Conexao falhou!");
	}
	[objetoRemoto retain];
	[initLock lock];
	[objetoRemoto registraRemoto: self];
	[initLock unlockWithCondition: 1];
	NSLog(@"Runlooparei!");
	[[NSRunLoop currentRunLoop] run];
	[objetoRemoto release];
	[pool release];
}

- (OutraClasse *)init
{
	self=[super init];
	objetoRemoto = nil;
	initLock = [[NSConditionLock alloc] initWithCondition: 0];
	NSThread * umaThread = [[NSThread alloc] initWithTarget: self
	  selector: @selector(fazAlgo:) object: nil];
	NSLog(@"Esperando runloopar.");
	[umaThread start];
	[initLock lockWhenCondition: 1];
	NSLog(@"Runloopando!");
	connecThread = umaThread;
	return self;
}

- (void)imprime: (id)arg
{
	NSLog(@"%@", (NSString *)arg);
}

- (void) registraRemoto: (id<Classe>) umObjeto
{
	objetoRemoto = [umObjeto retain];
}
- (void) connecImprimeRemoto: (id)mensagem
{
	[mensagem retain];
	if(objetoRemoto != nil){
		[objetoRemoto imprime: mensagem];
	}
	[mensagem autorelease];
}

- (void)imprimeRemoto: (NSString *)mensagem
{
	[mensagem retain];
	[self performSelector: @selector(connecImprimeRemoto:)
	  onThread: connecThread withObject: mensagem
	  waitUntilDone: YES];
	[mensagem autorelease];
}
@end
