#ifndef DIMVCLIENTPROTO_H_
#define DIMVCLIENTPROTO_H_

#import <Foundation/NSString.h>

@class DIMVClient;
@protocol  DIMVClient

#ifdef DIMV_EVALUATE
- (oneway void) receiveData: (in bycopy id)data fromCollector:
  (in bycopy NSString *)collectorId counter: (in bycopy unsigned)counter;
#else
- (oneway void) receiveData: (in bycopy id) data fromCollector:
  (in bycopy NSString *) collectorId;
#endif

- (oneway void) registerCollectorWithId: (bycopy NSString *)colReg type: (bycopy NSString *)colType location: (bycopy NSString *)colLocation origin: (bycopy NSString *)originId;

@end


@protocol HCMDataProcessor
- (void)inputData: (id) data;
@end

@protocol HCMSubscriptionHandler <NSObject> 
- (void )setClient: (DIMVClient *)aClient;
- (BOOL)registerCollectorWithId: (NSString *) collectorId
  type: (NSString *) type location: (NSString *)location;
- (BOOL)unregisterCollectorWithId: (NSString *) collectorId;
@end

#endif
