#import <Foundation/NSNotification.h>
@interface Observer : NSObject
- (void) connectionDied: (NSNotification *)notification;
@end
