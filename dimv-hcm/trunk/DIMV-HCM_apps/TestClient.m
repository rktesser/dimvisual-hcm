#import "DIMVClient.h" 
#import <Foundation/NSAutoreleasePool.h>
#import <unistd.h>

@interface printStringSource: NSObject <HCMDataProcessor>

- (void) inputData: (id) dSource;

@end

@implementation printStringSource

- (void) inputData: (id) dSource
{
	#ifndef DIMV_EVALUATE
	printf("%s\n",[(NSString *)dSource cString]);
//	printf("Data Received\n");
	#endif
	return;
}
@end


@interface  testSubscriptionHandler: NSObject <HCMSubscriptionHandler>
{
	DIMVClient *client;
	NSMutableDictionary *registeredCollectors;
}
- (testSubscriptionHandler *)initWithClient: (DIMVClient *)aClient;
- (NSMutableDictionary *)getRegisteredCollectors;
@end

@implementation testSubscriptionHandler

- (testSubscriptionHandler *)initWithClient: (DIMVClient *)aClient
{
	self = [super init];
	client = [aClient retain];
	registeredCollectors = [[NSMutableDictionary alloc] init];
	return self;

}

- (BOOL)registerCollectorWithId: (NSString *) collectorId
  type: (NSString *) type location: (NSString *)location
{
	BOOL ret;
	NSDictionary *colReg = [NSDictionary dictionaryWithObjectsAndKeys:
	collectorId, @"collectorId", type, @"collectorType",
	location, @"collectorLocation", nil];
	[registeredCollectors setObject: colReg forKey: collectorId];
	NSLog(@"Subscribing to %@", collectorId);
	ret = [client subscribeToCollector: collectorId];
	if(ret == NO){
		NSLog(@"WARNING: unsucessful subscription.");
	}
	return YES;
}

- (BOOL)unregisterCollectorWithId: (NSString *) collectorId
{
	NSLog(@"Unsubscribing to %@", collectorId);
	[client unsubscribeToCollector: collectorId];
	[registeredCollectors removeObjectForKey: collectorId];
	return YES;
}

- (NSMutableDictionary *)getRegisteredCollectors
{
	return [NSDictionary dictionaryWithDictionary: registeredCollectors];
}

- (void)setClient: (DIMVClient *)aClient
{
	return;
}

@end


int main(int argc, char **argv)
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	int i;
	
	NSMutableArray *aggregNames = nil; //names direct superiors in the hierarchy
	DIMVClient *client = nil;
	NSString *clientName;
	NSString *clientId;
	testSubscriptionHandler *subscrHandler;
	printStringSource *pSSource= [[printStringSource alloc] init];

	if(argc < 4){
		printf("Error: Insuficient arguments\n");
		return 1;
	}
	aggregNames = [NSMutableArray arrayWithCapacity: (argc - 3)];
	for(i=3; i < argc; i++){
		[aggregNames addObject: [[NSString alloc] initWithCString: argv[i]]];
//		NSLog(@"Received aggregator name \"%@\"\n",[aggregNames objectAtIndex: (i - 3)]);
	}
	clientId = [[NSString alloc] initWithCString: argv[1]];
//	NSLog(@"Client Id is: %@\n", clientId);
	clientName = [[NSString alloc] initWithCString: argv[2]];
//	NSLog(@"The client name is: %@\n", clientName);
	client = [[DIMVClient alloc] initWithId: clientId andAggregators: aggregNames];
	if(client != nil){
		[client addDataSource: pSSource withType: @"ganglia"];
		subscrHandler = [[testSubscriptionHandler alloc] initWithClient: client];
		[client setSubscriptionHandler: subscrHandler];
/*		[[NSRunLoop currentRunLoop] run];*/
		while(1){
			sleep(3600);
		}
	}else{
		NSLog(@"ERROR: Client creation failed! Aborting.\n");
	}
	[pool release];
	return 0;
}
