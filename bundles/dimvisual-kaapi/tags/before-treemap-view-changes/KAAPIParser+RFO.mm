/*
    DIMVisual-KAAPI, the DIMVisual bundle for KAAPI trace files
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual-KAAPI.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "KAAPIParser.h"

@implementation KAAPIParser (RFO)
- (GEvent *) parseKAAPIRfoLevel: (Util::RecordReader::const_iterator) r
        withIdentifier: (GIdentifier *) identifier
        withTimestamp: (GTimestamp *) timestamp
{
        GName *name = [[GName alloc] init];
        GFields *fields = [[GFields alloc] init];
        GIdentifier *nameid = [[GIdentifier alloc] init];

/*
        switch (r->_event){
                case Event::RFO_CLOSURE_EXEC0:
                        [name setName: @"EVT_RFO_CLOSURE_EXEC0"];
                        {
                        EVT_RFO_CLOSURE_EXEC0_Record *kaapiev;
                        kaapiev = (EVT_RFO_CLOSURE_EXEC0_Record *)r;
                        NSString *value = [NSString stringWithFormat: @"%p", kaapiev->closure];
                        [nameid addValue: @"closure"];
                        [fields setFieldWithKey: nameid withValue: value];
                        }
                        break;
                case Event::RFO_CLOSURE_EXEC1:
                        [name setName: @"EVT_RFO_CLOSURE_EXEC1"];
                        {
                        EVT_RFO_CLOSURE_EXEC1_Record *kaapiev;
                        kaapiev = (EVT_RFO_CLOSURE_EXEC1_Record *)r;
                        NSString *value = [NSString stringWithFormat: @"%p", kaapiev->closure];
                        [nameid addValue: @"closure"];
                        [fields setFieldWithKey: nameid withValue: value];
                        }
                        break;
                case Event::RFO_CLOSURE_SIGNAL0:
                        [name setName: @"EVT_RFO_CLOSURE_SIGNAL0"];
                        {
                        EVT_RFO_CLOSURE_SIGNAL0_Record *kaapiev;
                        kaapiev = (EVT_RFO_CLOSURE_SIGNAL0_Record *)r;
                        NSString *value = [NSString stringWithFormat: @"%p", kaapiev->closure];
                        [nameid addValue: @"closure"];
                        [fields setFieldWithKey: nameid withValue: value];
                        }
                        break;
                case Event::RFO_CLOSURE_SIGNAL1:
                        [name setName: @"EVT_RFO_CLOSURE_SIGNAL1"];
                        {
                        EVT_RFO_CLOSURE_SIGNAL1_Record *kaapiev;
                        kaapiev = (EVT_RFO_CLOSURE_SIGNAL1_Record *)r;
                        NSString *value = [NSString stringWithFormat: @"%p", kaapiev->closure];
                        [nameid addValue: @"closure"];
                        [fields setFieldWithKey: nameid withValue: value];
                        }
                        break;
                default:
                        NSLog (@"Warning: RFO_Level -> type = %d, not completely translated", r->_event);
                        break;
        }

*/
        GEvent *ev = [[GEvent alloc] init];
        [ev setTimestamp: timestamp];
        [ev setIdentifier: identifier];
        [ev setName: name];
        [ev setFields: fields];
        [ev autorelease];

        [name release];
        [fields release];
        [nameid release];

        return ev;
}

@end

