#import "CollectorRegister.h"

@implementation CollectorRegister

/*Some methods for future implementation of the subscription mechanism*/

- (NSMutableDictionary *)getSubscribers
{
	return subscribers;
}

- (void)addSubscriber: (id)subscriber withId: (NSString *)subscriberId
{
	[subscribers setObject: subscriber forKey: subscriberId];
}

- (void)removeSubscriber: (NSString *)subscriberId
{
	if([subscribers objectForKey: subscriberId] != nil){
		[subscribers removeObjectForKey: subscriberId];
	}else{
		NSLog(@"ERROR: Threre's no registered subscriber with Id=%@ for %@.", subscriberId, collectorId);
	}
}

- (void) setOriginId: (NSString *)origId
{
	ASSIGN(originId, origId);
}

- (void) setCollectorId: (NSString *) colId
{
	ASSIGN(collectorId, colId);
}

- (void) setCollectorType: (NSString *) colType
{
	ASSIGN(collectorType, colType);
}

- (void)setLocation: (NSString *)aLocation
{
	ASSIGN(collectorLocation, aLocation);
}

- (NSString *) getCollectorId
{
	return collectorId;
}

- (NSString *) getCollectorType
{
	return collectorType;
}

- (NSString *) getCollectorLocation
{
	return collectorLocation;
}
- (NSString *) getOriginId
{
	return originId;
}


- (CollectorRegister *)initWithCollectorId: (NSString *)colId andType: (NSString *)type andLocation: (NSString *)aLocation;
{
	[super init];
	ASSIGN(collectorId, colId);
	ASSIGN(collectorType, type);
	ASSIGN(collectorLocation, aLocation);
	subscribers = [[NSMutableDictionary alloc] init];
	originId = nil;
	return self;
}

//Methods from the NSCoding protocol
- (void) encodeWithCoder: (NSCoder *)coder
{
//	[super encodeWithCoder: coder];
	if([coder allowsKeyedCoding]){
		[coder encodeObject: collectorId forKey: @"collectorId"];
		[coder encodeObject: collectorType forKey:@"collectorType"];
		[coder encodeObject: originId forKey:@"originId"];
		[coder encodeObject: collectorLocation 
		  forKey:@"collectorLocation"];
	}else{
		[coder encodeObject: collectorId];
		[coder encodeObject: collectorType];
		[coder encodeObject: originId];
		[coder encodeObject: collectorLocation];
	}
	/*TODO: "encodar" os subscribers*/
}

- (id) initWithCoder: (NSCoder *)coder
{
	self = [super init];
	if([coder allowsKeyedCoding]){
		collectorId = [[coder decodeObjectForKey: @"collectorId"] retain];
		collectorType = [[coder decodeObjectForKey: @"collectorType"] retain];
		originId = [[coder decodeObjectForKey: @"originId"] retain];
		collectorLocation = [[coder decodeObjectForKey: @"collectorLocation"] retain];
	}else{
		collectorId = [[coder decodeObject] retain];
		collectorType = [[coder decodeObject] retain];
		originId = [[coder decodeObject] retain];
		collectorLocation = [[coder decodeObject] retain];
	}
	subscribers = [[NSMutableDictionary alloc] init];
	return self;
}	

- (id)replacementObjectForPortCoder:(NSPortCoder *)encoder
{
	if([encoder isBycopy]){
//		NSLog(@"Encode bycopy.\n");
		return self;
	}else{
		return [super replacementObjectForPortCoder: encoder];
	}
}

- (void) dealloc
{
	RELEASE(collectorId);
	RELEASE(collectorType);
	RELEASE(collectorLocation);
	if(originId != nil){
		RELEASE(originId);
	}
	RELEASE(subscribers);
	[super dealloc];
}

@end
