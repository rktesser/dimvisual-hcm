/*
    DIMVisual-KAAPI, the DIMVisual bundle for KAAPI trace files
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual-KAAPI.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "KAAPIDataSource.h"


@implementation KAAPIDataSource
- (id) initWithProvider: (id<Integrator>) provider
{
	self = [super init];
	prov = provider;
	[prov retain];
	if (provider == nil){
		NSLog (@"KAAPIDataSource: provider '%@' (Error)", provider);
		[self release];
		return nil;
	}
	eventsSelected = nil;
	return self;
}

- (void) resetConfiguration
{
	if (reader){
		[reader release];
	}
	if (converter){
		[converter release];
	}
}

- (BOOL) setConfiguration: (NSDictionary *) configuration
{
	[self resetConfiguration];

	/* Extract information from configuration provided */
	id files;
	id param;
	id ident;
	id sync;
	unsigned int i;


	if (configuration == nil){
		NSLog (@"KAAPIDataSource: configuration '%@'", configuration);
		[self release];
		return NO;
	}
	ident = [configuration objectForKey: IDKEY];
	if (ident == nil || [ident isEqual: IDVALUE] == NO){
		NSLog (@"KAAPIDataSource: configuration '%@' hasn't a field "
			"'ID' or is different from 'KAAPI'", configuration);
		[self release];
		return NO;
	}
	param = [configuration objectForKey: PARAMETERKEY];
	if (param == nil || [param isKindOfClass: [NSDictionary class]] == NO){
		NSLog (@"KAAPIDataSource: configuration '%@' hasn't a field "
			"'%@' or is not a NSArray", configuration,PARAMETERKEY);
		[self release];
		return NO;
	}
	files = [param objectForKey: (id)FILEKEY];
	if (files == nil || [files isKindOfClass: [NSArray class]] == NO){
		NSLog (@"KAAPIDataSource: configuration '%@' hasn't a field "
			"'%@' or is not a NSArray", configuration, FILEKEY);
		[self release];
		return NO;
	}
	sync = [param objectForKey: (id)SYNCKEY];
/*
	if ([sync isKindOfClass: [NSArray class]] && [sync count] >= 1){
		sync = [sync objectAtIndex: 0];
	}
	if (sync == nil || [sync isKindOfClass: [NSString class]] == NO){
		NSLog (@"KAAPIDataSource: configuration '%@' hasn't a field "
			"'%@' or is not a NSString", configuration, SYNCKEY);
//		[self release];
//		return NO;
	}
*/

	/* Check for events selected */
	if(![self setSelectedEvents: [param objectForKey: @"events"]]){
		return NO;
	}
	/* Check for type selected */
	if (![self setSelectedType: [param objectForKey: @"type"]]){
		return NO;
	}

	/* Instantiate the readers */
	NSLog (@"KAAPIDataSource: Opening trace files... (%@)",files);
	reader = [[KAAPIOrder alloc] init];
	for (i = 0; i < [files count]; i++){
		KAAPIFileReader *oneReader = [[KAAPIFileReader alloc] initWithFileName: [files objectAtIndex: i] andSyncFileName: sync andProvider: self];
		if (oneReader == nil){
			NSLog (@"KAAPIDataSource: couldn't create a KAAPIFileReader with file='%@' and sync='%@'", [files objectAtIndex: i], sync);
			[self resetConfiguration];
			return NO;
		}
		[reader add: oneReader];
		[oneReader release];
	}
	NSLog (@"KAAPIDataSource: mostRecent is %@", [[reader mostRecent] time]);

	/* and the converter */
	converter = [[KAAPIConverter alloc] initWithProvider: prov];
	if (converter == nil){
		NSLog (@"KAAPIDataSource: couldn't create a KAAPIConverter with "
			"provider='%@'", prov);
		[self resetConfiguration];
		return NO;
	}
	[converter setConversionType: conversionType];
	return YES;
}


- (GTimestamp *) time;
{
	return [[reader mostRecent] time];
}

- (id) convert
{
	KAAPIFileReader *oneReader = (KAAPIFileReader *)[reader mostRecent];
	id event = [oneReader event];
	if (event == nil){
		return nil;
	}else if ([event name]){
		GName *n = (GName *)[event name];
		if ([[n description] isEqual: @"KAAPI_NOT_READ"]){
			NSMutableArray *ar = [NSMutableArray array];
			return ar;
		}
	}
	return [converter convertEvent: event];
}

- (void) dealloc
{
	[reader release];
	[converter release];
	[eventsSelected release];
	[super dealloc];
}

- (NSDictionary *) configuration
{
	NSMutableDictionary *c;
	NSMutableDictionary *parameters;

	parameters = [[NSMutableDictionary alloc] init];
	
	[parameters setObject: [NSArray array] forKey: FILEKEY];
	[parameters setObject: [NSString string] forKey: SYNCKEY];
	[parameters setObject: [NSDictionary dictionaryWithContentsOfFile:
[[NSBundle bundleForClass: [self class]] pathForResource: @"KAAPIEvents" ofType:
@"plist"]] forKey: @"events"];

	NSMutableSet *types = [[NSMutableSet alloc] init];
	[types addObject: @"process"];
	[types addObject: @"thread"];
	[parameters setObject: types forKey: @"type"];
	[types release];
		
	c = [[NSMutableDictionary alloc] init];
	[c setObject: IDVALUE forKey: IDKEY];
	[c setObject: parameters forKey: PARAMETERKEY];
	[parameters release];
	[c autorelease];

	return c;
}

- (NSDictionary *) hierarchy
{
	if (conversionType == Process){
		return [NSDictionary dictionaryWithContentsOfFile: [[NSBundle bundleForClass: [self class]] pathForResource: @"KAAPIConfiguration-process" ofType: @"plist"]];
	}else{
		return [NSDictionary dictionaryWithContentsOfFile: [[NSBundle bundleForClass: [self class]] pathForResource: @"KAAPIConfiguration-thread" ofType: @"plist"]];
	}
}

- (KAAPIConversionType) conversionType
{
	return conversionType;
}
@end
