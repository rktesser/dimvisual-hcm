/*
   Copyright (c) 2005 Lucas Mello Schnorr <schnorr@gmail.com>
   
   This file is part of DIMVisual.
   
   DIMVisual is free software; you can redistribute it and/or modify it under
   the terms of the GNU Lesser General Public License as published by the
   Free Software Foundation; either version 2 of the License, or (at your
   option) any later version.
   
   DIMVisual is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
   for more details.
   
   You should have received a copy of the GNU Lesser General Public License
   along with DIMVisual; if not, write to the Free Software Foundation, Inc.,
   59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/
#include "MPIRASTROConverter.h"
#include "OneToAll.h"

static OneToAll *scatter;
static OneToAll *scatterv;


@implementation MPIRASTROConverter (Scatter)
- (id) convertEventScatter: (MPIRASTROEvent *) event
{
	NSMutableArray *ret = nil;
	NSString *container;
	//int type;

	if (scatter == nil){
		scatter = [[OneToAll alloc] init];
	}
	if (scatterv == nil){
		scatterv = [[OneToAll alloc] init];
	}
	
	ret = [[NSMutableArray alloc] init];

	container = [provider identifierForEntity: 
		[NSArray arrayWithObjects: @"Machine", @"Thread", nil]
				ids:
		[NSArray arrayWithObjects: [event machine], [event thread], nil]];

	if (container == nil){
		[ret release];
		return nil;
	}
	
	int etype = atoi ([[event type] cString]);


	/* MPI_SCATTER_IN */
	if (etype == MPI_SCATTER_IN){
		NSString *count = [event int32AtIndex: 0];
		NSString *size = [event int32AtIndex: 1];
		NSString *root = [event int32AtIndex: 2];
		NSString *members = [event stringAtIndex: 0];
		
		//int sendcnt = atoi([[event int32AtIndex: 0] cString]);

		[scatter memberEntering: container
			andEventNode: [event node]
			andEventTime: [event time]
			andRootNode: root
			andCommSize: size
			andMembersId: members];

		PajeSetState *set;
		set = [self addPajeSetState: @"MPI_SCATTER" time: [event time] container: container];
		[set retain];
		[set addFieldNamed: @"COUNT" withType: @"string"];
		[set setValue: count forFieldNamed: @"COUNT"];
		[ret addObject: set];
		[set release];

	/* MPI_SCATTER_OUT */
	}else if (etype == MPI_SCATTER_OUT){
		NSString *recvcnt = [event int32AtIndex: 0];
		NSString *root = [event int32AtIndex: 1];
		NSString *members = [event stringAtIndex: 0];

		[ret addObjectsFromArray: [scatter memberExiting: container andEventTime: [event time] andOperationName: @"scatter" andRootNode: root andMembersId: members andProvider: provider]];

		[ret addObject: [self addPajeSetState: @"running" time: [event time] container: container]];
		
		PajeNewEvent *ev;
		ev = [[PajeNewEvent alloc] initWithTime: [event time]];
		[ev setContainer: container];
		[ev setEntityType: [provider aliasToName: @"Events"]];
		[ev setValue: [NSString stringWithFormat: @"MPI_Scatter (%@ bytes)", recvcnt]];
		[ret addObject:ev];
		[ev release];

	
	/* MPI_SCATTERV_IN */
	}else if (etype == MPI_SCATTERV_IN){
		NSString *size = [event int32AtIndex: 0];
		NSString *root = [event int32AtIndex: 1];
		NSString *members = [event stringAtIndex: 0];

		[scatterv memberEntering: container
			andEventNode: [event node]
			andEventTime: [event time]
			andRootNode: root
			andCommSize: size
			andMembersId: members];
		[ret addObject: [self addPajeSetState: @"MPI_SCATTERV" time: [event time] container: container]];

	/* MPI_SCATTERV_OUT */
	}else if (etype == MPI_SCATTERV_OUT){
		NSString *recvcnt = [event int32AtIndex: 0];
		NSString *root = [event int32AtIndex: 1];
		NSString *members = [event stringAtIndex: 0];
		

		[ret addObjectsFromArray: [scatterv memberExiting: container andEventTime: [event time] andOperationName: @"scatterv" andRootNode: root andMembersId: members andProvider: provider]];

		[ret addObject: [self addPajeSetState: @"running" time: [event time] container: container]];

		PajeNewEvent *ev;
		ev = [[PajeNewEvent alloc] initWithTime: [event time]];
		[ev setContainer: container];
		[ev setEntityType: [provider aliasToName: @"Events"]];
		[ev setValue: [NSString stringWithFormat: @"MPI_Scatterv (%@ bytes)", recvcnt]];
		[ret addObject:ev];
		[ev release];
	}

	[ret autorelease];
	return ret;
}
@end
