/*
    DIMVisual-Ganglia, the DIMVisual bundle for Ganglia trace files
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual-Ganglia.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "GangliaDataSource.h"


@implementation GangliaDataSource
- (id) initWithProvider: (id<Integrator>) provider
{
	NSLog(@"Initializing the dimvisual-ganglia-hcm dataSource.\n");
	self = [super init];
	prov = provider;
	[prov retain];
	if (provider == nil){
		NSLog (@"GangliaDataSource: provider '%@' (Error)", provider);
		[self release];
		return nil;
	}
	NSLog(@"The dimvisual-ganglia-hcm data source is initialized.\n");
	return self;
}

- (void) resetConfiguration
{
	if (reader){
		[reader release];
	}
	if (converter){
		[converter release];
	}
}

- (BOOL) setConfiguration: (NSDictionary *) configuration
{
	[self resetConfiguration];

	/* Extract information from configuration provided */
	id files;
	id param;
	id ident;
	id sync;
	unsigned int i;

	NSLog(@"Configuring the dimvisual-ganglia-hcm data source.\n");
	if (configuration == nil){
		NSLog (@"GangliaDataSource: configuration '%@'", configuration);
		[self release];
		return NO;
	}
	ident = [configuration objectForKey: IDKEY];
	if (ident == nil || [ident isEqual: IDVALUE] == NO){
		NSLog (@"GangliaDataSource: configuration '%@' hasn't a field "
			"'ID' or is different from 'Ganglia'", configuration);
		[self release];
		return NO;
	}
	param = [configuration objectForKey: PARAMETERKEY];
	if (param == nil || [param isKindOfClass: [NSDictionary class]] == NO){
		NSLog (@"GangliaDataSource: configuration '%@' hasn't a field "
			"'%@' or is not a NSArray", configuration,PARAMETERKEY);
		[self release];
		return NO;
	}
	files = [param objectForKey: (id)FILEKEY];
	if (files == nil || [files isKindOfClass: [NSArray class]] == NO){
		NSLog (@"GangliaDataSource: configuration '%@' hasn't a field "
			"'%@' or is not a NSArray", configuration, FILEKEY);
		[self release];
		return NO;
	}
	sync = [param objectForKey: (id)SYNCKEY];
/*
	if ([sync isKindOfClass: [NSArray class]] && [sync count] >= 1){
		sync = [sync objectAtIndex: 0];
	}
	if (sync == nil || [sync isKindOfClass: [NSString class]] == NO){
		NSLog (@"GangliaDataSource: configuration '%@' hasn't a field "
			"'%@' or is not a NSString", configuration, SYNCKEY);
//		[self release];
//		return NO;
	}
*/

	/* Instantiate the readers */
	NSLog (@"GangliaDataSource: Opening trace files... (%@)",files);
	reader = [[NSMutableArray alloc] init];
	for (i = 0; i < [files count]; i++){
		GangliaFileReader *oneReader = [[GangliaFileReader alloc] initWithFileName: [files objectAtIndex: i] andSyncFileName: sync andProvider: self];
		if (oneReader == nil){
			NSLog (@"GangliaDataSource: couldn't create a GangliaFileReader with file='%@' and sync='%@'", [files objectAtIndex: i], sync);
			[self resetConfiguration];
			return NO;
		}
		[reader addObject: oneReader];
		[oneReader release];
	}
	NSLog (@"GangliaDataSource: mostRecent is %@",
		[[reader objectAtIndex: 0] time]);

	/* and the converter */
	converter = [[GangliaConverter alloc] initWithProvider: prov];
	if (converter == nil){
		NSLog (@"GangliaDataSource: couldn't create a GangliaConverter"
			" with provider='%@'", prov);
		[self resetConfiguration];
		return NO;
	}
	NSLog(@"The dimvisual-ganglia-hcm data source is configured.\n");
	return YES;
}


- (GTimestamp *) time;
{
	if ([reader count] > 0){
		return [[reader objectAtIndex: 0] time];
	}else{
		return nil;
	}
}

- (id) convert
{
	if ([reader count] == 0){ /* end of Ganglia XML files */
		return nil;
	}
	GangliaFileReader *oneReader = [reader objectAtIndex: 0];
	id event = [oneReader event];
	if (event == nil){
		[reader removeObjectAtIndex: 0];
		return [self convert];
	}else if ([event name]){
		GName *n = (GName *)[event name];
		if ([[n description] isEqual: @"Ganglia_NOT_READ"]){
			NSMutableArray *ar = [NSMutableArray array];
			return ar;
		}
	}
	return [converter convertEvent: event];
}

- (void) dealloc
{
	[reader release];
	[converter release];
	[super dealloc];
}

- (NSDictionary *) configuration
{
	NSMutableDictionary *c;
	NSMutableDictionary *parameters;

	parameters = [[NSMutableDictionary alloc] init];
	
	[parameters setObject: [NSArray array] forKey: FILEKEY];
	[parameters setObject: [NSString string] forKey: SYNCKEY];
//	[parameters setObject: [NSDictionary dictionaryWithContentsOfFile: [[NSBundle bundleForClass: [self class]] pathForResource: @"GangliaEvents" ofType: @"plist"]] forKey: @"events"];

//	NSMutableSet *types = [[NSMutableSet alloc] init];
//	[types addObject: @"process"];
//	[types addObject: @"thread"];
//	[types addObject: @"grid"];
//	[parameters setObject: types forKey: @"type"];
//	[types release];
		
	c = [[NSMutableDictionary alloc] init];
	[c setObject: IDVALUE forKey: IDKEY];
	[c setObject: parameters forKey: PARAMETERKEY];
	[parameters release];
	[c autorelease];

	return c;
}

- (NSDictionary *) hierarchy
{
	return [NSDictionary dictionaryWithContentsOfFile: [[NSBundle bundleForClass: [self class]] pathForResource: @"GangliaConfiguration-grid" ofType: @"plist"]];
}

- (double) simpleTime
{
	return [[reader mostRecent] simpleTime];
}

- (NSComparisonResult) compareTime: (id<Time>) otherObject
{
        double time = [otherObject simpleTime];
	double simpleTime = [self simpleTime];
        if (simpleTime < time){
                return NSOrderedAscending;
        }else if (simpleTime > time){
                return NSOrderedDescending;
        }else{
                return NSOrderedSame;
        }
}
@end
