/*
    DIMVisual-KAAPI, the DIMVisual bundle for KAAPI trace files
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual-KAAPI.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "KAAPIConverter.h"
#include <kaapi> /* for the trace and level stuff (identifier) */

@implementation KAAPIConverter (Kernel)
- (id) convertEventKernel: (GEvent *) event
{
	NSMutableArray *ret = [[NSMutableArray alloc] init];

	/* converting events */
	switch (atoi ([eventid cString])){

/*
		case Event::CORE_THREAD_START:
			{	
				PajeNewEvent *ev = [[PajeNewEvent alloc]
					initWithTime: time];
				[ev setContainer: container];
				[ev setEntityType: [provider 
					aliasToName: @"KAAPIKernelEvent"]];
				[ev setValue: [provider aliasToName:
@"CORE_THREAD_START"]];
				[ret addObject: ev];
				[ev release];
			}
			break;
		case Event::CORE_THREAD_COMPUTE:
			{	
				PajeNewEvent *ev = [[PajeNewEvent alloc]
					initWithTime: time];
				[ev setContainer: container];
				[ev setEntityType: [provider 
					aliasToName: @"KAAPIKernelEvent"]];
				[ev setValue: [provider aliasToName:
@"CORE_THREAD_COMPUTE"]];
				[ret addObject: ev];
				[ev release];
			}
			break;
		case Event::CORE_THREAD_SUSPEND:
			{	
				PajeNewEvent *ev = [[PajeNewEvent alloc]
					initWithTime: time];
				[ev setContainer: container];
				[ev setEntityType: [provider 
					aliasToName: @"KAAPIKernelEvent"]];
				[ev setValue: [provider aliasToName:
@"CORE_THREAD_SUSPEND"]];
				[ret addObject: ev];
				[ev release];
			}
			break;
		case Event::CORE_THREAD_WAKEUP:
			{	
				PajeNewEvent *ev = [[PajeNewEvent alloc]
					initWithTime: time];
				[ev setContainer: container];
				[ev setEntityType: [provider 
					aliasToName: @"KAAPIKernelEvent"]];
				[ev setValue: [provider aliasToName:
@"CORE_THREAD_WAKEUP"]];
				[ret addObject: ev];
				[ev release];
			}
			break;
		case Event::CORE_THREAD_STEAL:
			{	
				PajeNewEvent *ev = [[PajeNewEvent alloc]
					initWithTime: time];
				[ev setContainer: container];
				[ev setEntityType: [provider 
					aliasToName: @"KAAPIKernelEvent"]];
				[ev setValue: [provider aliasToName:
@"CORE_THREAD_STEAL"]];
				[ret addObject: ev];
				[ev release];
			}
			break;
		case Event::CORE_THREAD_TERM:
			{	
				PajeNewEvent *ev = [[PajeNewEvent alloc]
					initWithTime: time];
				[ev setContainer: container];
				[ev setEntityType: [provider 
					aliasToName: @"KAAPIKernelEvent"]];
				[ev setValue: [provider aliasToName:
@"CORE_THREAD_TERM"]];
				[ret addObject: ev];
				[ev release];
			}
			break;
		case Event::CORE_THREAD_SIGNALWAKEUP:
			{	
				PajeNewEvent *ev = [[PajeNewEvent alloc]
					initWithTime: time];
				[ev setContainer: container];
				[ev setEntityType: [provider 
					aliasToName: @"KAAPIKernelEvent"]];
				[ev setValue: [provider aliasToName:
@"CORE_THREAD_SIGNALWAKEUP"]];
				[ret addObject: ev];
				[ev release];
			}
			break;
		case Event::CORE_THREAD_LSTEAL:
			{
				PajePushState *push = [[PajePushState alloc]
						initWithTime: time];
				[push setContainer: container];
				[push setEntityType: [provider aliasToName:
						@"KAAPIKernelState"]];
				[push setValue: [provider aliasToName:
						@"CORE_THREAD_LSTEAL"]];
				[ret addObject: push];
				[push release];
			}
			break;
		case Event::CORE_THREAD_RETLSTEAL:
			{
				PajePopState *pop = [[PajePopState alloc] 
						initWithTime: time];
				[pop setContainer: container];
				[pop setEntityType: [provider aliasToName:
						@"KAAPIKernelState"]];
				[ret addObject: pop];
				[pop release];
			}
			break;
		case Event::CORE_THREAD_RSTEAL:
			{
				PajePushState *push = [[PajePushState alloc]
						initWithTime: time];
				[push setContainer: container];
				[push setEntityType: [provider aliasToName:
						@"KAAPIKernelState"]];
				[push setValue: [provider aliasToName:
						@"CORE_THREAD_RSTEAL"]];
				[ret addObject: push];
				[push release];
			}
			break;
		case Event::CORE_THREAD_RETRSTEAL:
			{
				PajePopState *pop = [[PajePopState alloc] 
						initWithTime: time];
				[pop setContainer: container];
				[pop setEntityType: [provider aliasToName:
						@"KAAPIKernelState"]];
				[ret addObject: pop];
				[pop release];
			}
			break;
*/
	}

	[ret autorelease];
	return ret;
}
@end
