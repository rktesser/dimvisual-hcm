/*
    DIMVisual-KAAPI, the DIMVisual bundle for KAAPI trace files
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual-KAAPI.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "KAAPIConverter.h"
#include <kaapi> /* for the trace and level stuff (identifier) */

@implementation KAAPIConverter (RFO)
- (id) convertEventRFO: (GEvent *) event
{
	NSMutableArray *ret = [[NSMutableArray alloc] init];

        switch (atoi ([eventid cString])){
/*
		case Event::RFO_CLOSURE_EXEC0: 
			{
				PajePushState *push = [[PajePushState alloc] 
					initWithTime: time];
				[push setContainer: container];
				[push setEntityType: [provider aliasToName:
					@"KAAPIRfoState"]];
				[push setValue: [provider aliasToName:
					@"RFO_CLOSURE_EXEC"]];
				[ret addObject: push];
				[push release];
			}
			break;
		case Event::RFO_CLOSURE_EXEC1: 
			{
				PajePopState *pop = [[PajePopState alloc]
					initWithTime: time];
				[pop setContainer: container];
				[pop setEntityType: [provider aliasToName:
					@"KAAPIRfoState"]];
				[ret addObject: pop];
				[pop release];
			}
			break;
		case Event::RFO_CLOSURE_SIGNAL0:
			{
				PajePushState *push = [[PajePushState alloc] 
					initWithTime: time];
				[push setContainer: container];
				[push setEntityType: [provider aliasToName:
					@"KAAPIRfoState"]];
				[push setValue: [provider aliasToName:
					@"RFO_CLOSURE_SIGNAL"]];
				[ret addObject: push];
				[push release];
			}
			break;
		case Event::RFO_CLOSURE_SIGNAL1:
			{
				PajePopState *pop = [[PajePopState alloc]
					initWithTime: time];
				[pop setContainer: container];
				[pop setEntityType: [provider aliasToName:
					@"KAAPIRfoState"]];
				[ret addObject: pop];
				[pop release];
			}
			break;
*/
		default: break;
	}

	[ret autorelease];
	return ret;
}
@end
