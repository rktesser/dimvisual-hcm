/*
   Copyright (c) 2005 Lucas Mello Schnorr <schnorr@gmail.com>
   
   This file is part of DIMVisual.
   
   DIMVisual is free software; you can redistribute it and/or modify it under
   the terms of the GNU Lesser General Public License as published by the
   Free Software Foundation; either version 2 of the License, or (at your
   option) any later version.
   
   DIMVisual is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
   for more details.
   
   You should have received a copy of the GNU Lesser General Public License
   along with DIMVisual; if not, write to the Free Software Foundation, Inc.,
   59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/
#include "MPIRASTROConverter.h"

@implementation MPIRASTROConverter (Others)
- (id) convertEventOthers: (GEvent *) event
{
	NSMutableArray *ret = nil;
	NSString *container;

	GFields *f = [event fields];
	NSString *machine, *thread, *node, *identifier, *time;

	machine = [[f fieldWithSimpleStringKey:@"machine"] description];
	thread = [[f fieldWithSimpleStringKey: @"thread"] description];
	node = [[f fieldWithSimpleStringKey: @"node"] description];
	identifier = [[event identifier] description];
	time = [[event timestamp] description];
	
	ret = [[NSMutableArray alloc] init];

	container = [provider identifierForEntity: 
		[NSArray arrayWithObjects: @"Machine", @"Thread", nil]
				ids:
		[NSArray arrayWithObjects: machine, thread, nil]];

	if (container == nil){
		[ret release];
		return nil;
	}
	
	int etype = atoi ([identifier cString]);

	if (etype == MPI_OP_CREATE_IN){
		[ret addObject: [self addPajeSetState: @"MPI_OP_CREATE" time: time container: container]];
	}else if (etype == MPI_OP_FREE_IN){
		[ret addObject: [self addPajeSetState: @"MPI_OP_FREE" time: time container: container]];
	}else if (etype == MPI_SCAN_IN){
		[ret addObject: [self addPajeSetState: @"MPI_SCAN" time: time container: container]];
	}else if (etype == MPI_ATTR_DELETE_IN){
		[ret addObject: [self addPajeSetState: @"MPI_ATTR_DELETE" time: time container: container]];
	}else if (etype == MPI_ATTR_GET_IN){
		[ret addObject: [self addPajeSetState: @"MPI_ATTR_GET" time: time container: container]];
	}else if (etype == MPI_ATTR_PUT_IN){
		[ret addObject: [self addPajeSetState: @"MPI_ATTR_PUT" time: time container: container]];
	}else if (etype == MPI_COMM_COMPARE_IN){
		[ret addObject: [self addPajeSetState: @"MPI_COMM_COMPARE" time: time container: container]];
	}else if (etype == MPI_COMM_CREATE_IN){
		[ret addObject: [self addPajeSetState: @"MPI_COMM_CREATE" time: time container: container]];
	}else if (etype == MPI_COMM_DUP_IN){
		[ret addObject: [self addPajeSetState: @"MPI_COMM_DUP" time: time container: container]];
	}else if (etype == MPI_COMM_FREE_IN){
		[ret addObject: [self addPajeSetState: @"MPI_COMM_FREE" time: time container: container]];
	}else if (etype == MPI_COMM_GROUP_IN){
		[ret addObject: [self addPajeSetState: @"MPI_COMM_GROUP" time: time container: container]];
	}else if (etype == MPI_COMM_RANK_IN){
		[ret addObject: [self addPajeSetState: @"MPI_COMM_RANK" time: time container: container]];
	}else if (etype == MPI_COMM_REMOTE_GROUP_IN){
		[ret addObject: [self addPajeSetState: @"MPI_COMM_REMOTE_GROUP" time: time container: container]];
	}else if (etype == MPI_COMM_REMOTE_SIZE_IN){
		[ret addObject: [self addPajeSetState: @"MPI_COMM_REMOTE_SIZE" time: time container: container]];
	}else if (etype == MPI_COMM_SIZE_IN){
		[ret addObject: [self addPajeSetState: @"MPI_COMM_SIZE" time: time container: container]];
	}else if (etype == MPI_COMM_SPLIT_IN){
		[ret addObject: [self addPajeSetState: @"MPI_COMM_SPLIT" time: time container: container]];
	}else if (etype == MPI_COMM_TEST_INTER_IN){
		[ret addObject: [self addPajeSetState: @"MPI_COMM_TEST_INTER" time: time container: container]];
	}else if (etype == MPI_GROUP_COMPARE_IN){
		[ret addObject: [self addPajeSetState: @"MPI_GROUP_COMPARE" time: time container: container]];
	}else if (etype == MPI_GROUP_DIFFERENCE_IN){
		[ret addObject: [self addPajeSetState: @"MPI_GROUP_DIFFERENCE" time: time container: container]];
	}else if (etype == MPI_GROUP_EXCL_IN){
		[ret addObject: [self addPajeSetState: @"MPI_GROUP_EXCL" time: time container: container]];
	}else if (etype == MPI_GROUP_FREE_IN){
		[ret addObject: [self addPajeSetState: @"MPI_GROUP_FREE" time: time container: container]];
	}else if (etype == MPI_GROUP_INCL_IN){
		[ret addObject: [self addPajeSetState: @"MPI_GROUP_INCL" time: time container: container]];
	}else if (etype == MPI_GROUP_INTERSECTION_IN){
		[ret addObject: [self addPajeSetState: @"MPI_GROUP_INTERSECTION" time: time container: container]];
	}else if (etype == MPI_GROUP_RANK_IN){
		[ret addObject: [self addPajeSetState: @"MPI_GROUP_RANK" time: time container: container]];
	}else if (etype == MPI_GROUP_RANGE_EXCL_IN){
		[ret addObject: [self addPajeSetState: @"MPI_GROUP_RANGE_EXCL" time: time container: container]];
	}else if (etype == MPI_GROUP_RANGE_INCL_IN){
		[ret addObject: [self addPajeSetState: @"MPI_GROUP_RANGE_INCL" time: time container: container]];
	}else if (etype == MPI_GROUP_SIZE_IN){
		[ret addObject: [self addPajeSetState: @"MPI_GROUP_SIZE" time: time container: container]];
	}else if (etype == MPI_GROUP_TRANSLATE_RANKS_IN){
		[ret addObject: [self addPajeSetState: @"MPI_GROUP_TRANSLATE_RANKS" time: time container: container]];
	}else if (etype == MPI_GROUP_UNION_IN){
		[ret addObject: [self addPajeSetState: @"MPI_GROUP_UNION" time: time container: container]];
	}else if (etype == MPI_INTERCOMM_CREATE_IN){
		[ret addObject: [self addPajeSetState: @"MPI_INTERCOMM_CREATE" time: time container: container]];
	}else if (etype == MPI_INTERCOMM_MERGE_IN){
		[ret addObject: [self addPajeSetState: @"MPI_INTERCOMM_MERGE" time: time container: container]];
	}else if (etype == MPI_KEYVAL_CREATE_IN){
		[ret addObject: [self addPajeSetState: @"MPI_KEYVAL_CREATE" time: time container: container]];
	}else if (etype == MPI_KEYVAL_FREE_IN){
		[ret addObject: [self addPajeSetState: @"MPI_KEYVAL_FREE" time: time container: container]];
	}else if (etype == MPI_ABORT_IN){
		[ret addObject: [self addPajeSetState: @"MPI_ABORT" time: time container: container]];
	}else if (etype == MPI_ERROR_CLASS_IN){
		[ret addObject: [self addPajeSetState: @"MPI_ERROR_CLASS" time: time container: container]];
	}else if (etype == MPI_ERRHANDLER_CREATE_IN){
		[ret addObject: [self addPajeSetState: @"MPI_ERRHANDLER_CREATE" time: time container: container]];
	}else if (etype == MPI_ERRHANDLER_FREE_IN){
		[ret addObject: [self addPajeSetState: @"MPI_ERRHANDLER_FREE" time: time container: container]];
	}else if (etype == MPI_ERRHANDLER_GET_IN){
		[ret addObject: [self addPajeSetState: @"MPI_ERRHANDLER_GET" time: time container: container]];
	}else if (etype == MPI_ERROR_STRING_IN){
		[ret addObject: [self addPajeSetState: @"MPI_ERROR_STRING" time: time container: container]];
	}else if (etype == MPI_ERRHANDLER_SET_IN){
		[ret addObject: [self addPajeSetState: @"MPI_ERRHANDLER_SET" time: time container: container]];
	}else if (etype == MPI_GET_PROCESSOR_NAME_IN){
		[ret addObject: [self addPajeSetState: @"MPI_GET_PROCESSOR_NAME" time: time container: container]];
	}else if (etype == MPI_INITIALIZED_IN){
		[ret addObject: [self addPajeSetState: @"MPI_INITIALIZED" time: time container: container]];
	}else if (etype == MPI_WTICK_IN){
		[ret addObject: [self addPajeSetState: @"MPI_WTICK" time: time container: container]];
	}else if (etype == MPI_WTIME_IN){
		[ret addObject: [self addPajeSetState: @"MPI_WTIME" time: time container: container]];
	}else if (etype == MPI_ADDRESS_IN){
		[ret addObject: [self addPajeSetState: @"MPI_ADDRESS" time: time container: container]];
	}else if (etype == MPI_BSEND_IN){
		[ret addObject: [self addPajeSetState: @"MPI_BSEND" time: time container: container]];
	}else if (etype == MPI_BSEND_INIT_IN){
		[ret addObject: [self addPajeSetState: @"MPI_BSEND_INIT" time: time container: container]];
	}else if (etype == MPI_BUFFER_ATTACH_IN){
		[ret addObject: [self addPajeSetState: @"MPI_BUFFER_ATTACH" time: time container: container]];
	}else if (etype == MPI_BUFFER_DETACH_IN){
		[ret addObject: [self addPajeSetState: @"MPI_BUFFER_DETACH" time: time container: container]];
	}else if (etype == MPI_CANCEL_IN){
		[ret addObject: [self addPajeSetState: @"MPI_CANCEL" time: time container: container]];
	}else if (etype == MPI_REQUEST_FREE_IN){
		[ret addObject: [self addPajeSetState: @"MPI_REQUEST_FREE" time: time container: container]];
	}else if (etype == MPI_RECV_INIT_IN){
		[ret addObject: [self addPajeSetState: @"MPI_RECV_INIT" time: time container: container]];
	}else if (etype == MPI_SEND_INIT_IN){
		[ret addObject: [self addPajeSetState: @"MPI_SEND_INIT" time: time container: container]];
	}else if (etype == MPI_GET_ELEMENTS_IN){
		[ret addObject: [self addPajeSetState: @"MPI_GET_ELEMENTS" time: time container: container]];
	}else if (etype == MPI_GET_COUNT_IN){
		[ret addObject: [self addPajeSetState: @"MPI_GET_COUNT" time: time container: container]];
	}else if (etype == MPI_IBSEND_IN){
		[ret addObject: [self addPajeSetState: @"MPI_IBSEND" time: time container: container]];
	}else if (etype == MPI_IPROBE_IN){
		[ret addObject: [self addPajeSetState: @"MPI_IPROBE" time: time container: container]];
	}else if (etype == MPI_IRECV_IN){
		[ret addObject: [self addPajeSetState: @"MPI_IRECV" time: time container: container]];
	}else if (etype == MPI_IRSEND_IN){
		[ret addObject: [self addPajeSetState: @"MPI_IRSEND" time: time container: container]];
	}else if (etype == MPI_ISEND_IN){
		[ret addObject: [self addPajeSetState: @"MPI_ISEND" time: time container: container]];
	}else if (etype == MPI_ISSEND_IN){
		[ret addObject: [self addPajeSetState: @"MPI_ISSEND" time: time container: container]];
	}else if (etype == MPI_PACK_IN){
		[ret addObject: [self addPajeSetState: @"MPI_PACK" time: time container: container]];
	}else if (etype == MPI_PACK_SIZE_IN){
		[ret addObject: [self addPajeSetState: @"MPI_PACK_SIZE" time: time container: container]];
	}else if (etype == MPI_PROBE_IN){
		[ret addObject: [self addPajeSetState: @"MPI_PROBE" time: time container: container]];
	}else if (etype == MPI_RSEND_IN){
		[ret addObject: [self addPajeSetState: @"MPI_RSEND" time: time container: container]];
	}else if (etype == MPI_RSEND_INIT_IN){
		[ret addObject: [self addPajeSetState: @"MPI_RSEND_INIT" time: time container: container]];
	}else if (etype == MPI_SENDRECV_IN){
		[ret addObject: [self addPajeSetState: @"MPI_SENDRECV" time: time container: container]];
	}else if (etype == MPI_SENDRECV_REPLACE_IN){
		[ret addObject: [self addPajeSetState: @"MPI_SENDRECV_REPLACE" time: time container: container]];
	}else if (etype == MPI_SSEND_IN){
		[ret addObject: [self addPajeSetState: @"MPI_SSEND" time: time container: container]];
	}else if (etype == MPI_SSEND_INIT_IN){
		[ret addObject: [self addPajeSetState: @"MPI_SSEND_INIT" time: time container: container]];
	}else if (etype == MPI_START_IN){
		[ret addObject: [self addPajeSetState: @"MPI_START" time: time container: container]];
	}else if (etype == MPI_STARTALL_IN){
		[ret addObject: [self addPajeSetState: @"MPI_STARTALL" time: time container: container]];
	}else if (etype == MPI_TEST_IN){
		[ret addObject: [self addPajeSetState: @"MPI_TEST" time: time container: container]];
	}else if (etype == MPI_TESTALL_IN){
		[ret addObject: [self addPajeSetState: @"MPI_TESTALL" time: time container: container]];
	}else if (etype == MPI_TESTANY_IN){
		[ret addObject: [self addPajeSetState: @"MPI_TESTANY" time: time container: container]];
	}else if (etype == MPI_TEST_CANCELLED_IN){
		[ret addObject: [self addPajeSetState: @"MPI_TEST_CANCELLED" time: time container: container]];
	}else if (etype == MPI_TESTSOME_IN){
		[ret addObject: [self addPajeSetState: @"MPI_TESTSOME" time: time container: container]];
	}else if (etype == MPI_TYPE_COMMIT_IN){
		[ret addObject: [self addPajeSetState: @"MPI_TYPE_COMMIT" time: time container: container]];
	}else if (etype == MPI_TYPE_CONTIGUOUS_IN){
		[ret addObject: [self addPajeSetState: @"MPI_TYPE_CONTIGUOUS" time: time container: container]];
	}else if (etype == MPI_TYPE_EXTENT_IN){
		[ret addObject: [self addPajeSetState: @"MPI_TYPE_EXTENT" time: time container: container]];
	}else if (etype == MPI_TYPE_FREE_IN){
		[ret addObject: [self addPajeSetState: @"MPI_TYPE_FREE" time: time container: container]];
	}else if (etype == MPI_TYPE_HINDEXED_IN){
		[ret addObject: [self addPajeSetState: @"MPI_TYPE_HINDEXED" time: time container: container]];
	}else if (etype == MPI_TYPE_HVECTOR_IN){
		[ret addObject: [self addPajeSetState: @"MPI_TYPE_HVECTOR" time: time container: container]];
	}else if (etype == MPI_TYPE_INDEXED_IN){
		[ret addObject: [self addPajeSetState: @"MPI_TYPE_INDEXED" time: time container: container]];
	}else if (etype == MPI_TYPE_LB_IN){
		[ret addObject: [self addPajeSetState: @"MPI_TYPE_LB" time: time container: container]];
	}else if (etype == MPI_TYPE_SIZE_IN){
		[ret addObject: [self addPajeSetState: @"MPI_TYPE_SIZE" time: time container: container]];
	}else if (etype == MPI_TYPE_STRUCT_IN){
		[ret addObject: [self addPajeSetState: @"MPI_TYPE_STRUCT" time: time container: container]];
	}else if (etype == MPI_TYPE_UB_IN){
		[ret addObject: [self addPajeSetState: @"MPI_TYPE_UB" time: time container: container]];
	}else if (etype == MPI_TYPE_VECTOR_IN){
		[ret addObject: [self addPajeSetState: @"MPI_TYPE_VECTOR" time: time container: container]];
	}else if (etype == MPI_UNPACK_IN){
		[ret addObject: [self addPajeSetState: @"MPI_UNPACK" time: time container: container]];
	}else if (etype == MPI_WAIT_IN){
		[ret addObject: [self addPajeSetState: @"MPI_WAIT" time: time container: container]];
	}else if (etype == MPI_WAITALL_IN){
		[ret addObject: [self addPajeSetState: @"MPI_WAITALL" time: time container: container]];
	}else if (etype == MPI_WAITANY_IN){
		[ret addObject: [self addPajeSetState: @"MPI_WAITANY" time: time container: container]];
	}else if (etype == MPI_WAITSOME_IN){
		[ret addObject: [self addPajeSetState: @"MPI_WAITSOME" time: time container: container]];
	}else if (etype == MPI_CART_COORDS_IN){
		[ret addObject: [self addPajeSetState: @"MPI_CART_COORDS" time: time container: container]];
	}else if (etype == MPI_CART_CREATE_IN){
		[ret addObject: [self addPajeSetState: @"MPI_CART_CREATE" time: time container: container]];
	}else if (etype == MPI_CART_GET_IN){
		[ret addObject: [self addPajeSetState: @"MPI_CART_GET" time: time container: container]];
	}else if (etype == MPI_CART_MAP_IN){
		[ret addObject: [self addPajeSetState: @"MPI_CART_MAP" time: time container: container]];
	}else if (etype == MPI_CART_SHIFT_IN){
		[ret addObject: [self addPajeSetState: @"MPI_CART_SHIFT" time: time container: container]];
	}else if (etype == MPI_CARTDIM_GET_IN){
		[ret addObject: [self addPajeSetState: @"MPI_CARTDIM_GET" time: time container: container]];
	}else if (etype == MPI_DIMS_CREATE_IN){
		[ret addObject: [self addPajeSetState: @"MPI_DIMS_CREATE" time: time container: container]];
	}else if (etype == MPI_GRAPH_CREATE_IN){
		[ret addObject: [self addPajeSetState: @"MPI_GRAPH_CREATE" time: time container: container]];
	}else if (etype == MPI_GRAPH_GET_IN){
		[ret addObject: [self addPajeSetState: @"MPI_GRAPH_GET" time: time container: container]];
	}else if (etype == MPI_GRAPH_MAP_IN){
		[ret addObject: [self addPajeSetState: @"MPI_GRAPH_MAP" time: time container: container]];
	}else if (etype == MPI_GRAPH_NEIGHBORS_IN){
		[ret addObject: [self addPajeSetState: @"MPI_GRAPH_NEIGHBORS" time: time container: container]];
	}else if (etype == MPI_GRAPH_NEIGHBORS_COUNT_IN){
		[ret addObject: [self addPajeSetState: @"MPI_GRAPH_NEIGHBORS_COUNT" time: time container: container]];
	}else if (etype == MPI_GRAPHDIMS_GET_IN){
		[ret addObject: [self addPajeSetState: @"MPI_GRAPHDIMS_GET" time: time container: container]];
	}else if (etype == MPI_TOPO_TEST_IN){
		[ret addObject: [self addPajeSetState: @"MPI_TOPO_TEST" time: time container: container]];
	}else if (etype == MPI_RECV_IDLE_IN){
		[ret addObject: [self addPajeSetState: @"MPI_RECV_IDLE" time: time container: container]];
	}else if (etype == MPI_CART_RANK_IN){
		[ret addObject: [self addPajeSetState: @"MPI_CART_RANK" time: time container: container]];
	}else if (etype == MPI_CART_SUB_IN){
		[ret addObject: [self addPajeSetState: @"MPI_CART_SUB" time: time container: container]];
	}else if (
		etype == MPI_OP_CREATE_OUT
		|| etype == MPI_OP_FREE_OUT
		|| etype == MPI_SCAN_OUT
		|| etype == MPI_ATTR_DELETE_OUT
		|| etype == MPI_ATTR_GET_OUT
		|| etype == MPI_ATTR_PUT_OUT
		|| etype == MPI_COMM_COMPARE_OUT
		|| etype == MPI_COMM_CREATE_OUT
		|| etype == MPI_COMM_DUP_OUT
		|| etype == MPI_COMM_FREE_OUT
		|| etype == MPI_COMM_GROUP_OUT
		|| etype == MPI_COMM_RANK_OUT
		|| etype == MPI_COMM_REMOTE_GROUP_OUT
		|| etype == MPI_COMM_REMOTE_SIZE_OUT
		|| etype == MPI_COMM_SIZE_OUT
		|| etype == MPI_COMM_SPLIT_OUT
		|| etype == MPI_COMM_TEST_INTER_OUT
		|| etype == MPI_GROUP_COMPARE_OUT
		|| etype == MPI_GROUP_DIFFERENCE_OUT
		|| etype == MPI_GROUP_EXCL_OUT
		|| etype == MPI_GROUP_FREE_OUT
		|| etype == MPI_GROUP_INCL_OUT
		|| etype == MPI_GROUP_INTERSECTION_OUT
		|| etype == MPI_GROUP_RANK_OUT
		|| etype == MPI_GROUP_RANGE_EXCL_OUT
		|| etype == MPI_GROUP_RANGE_INCL_OUT
		|| etype == MPI_GROUP_SIZE_OUT
		|| etype == MPI_GROUP_TRANSLATE_RANKS_OUT
		|| etype == MPI_GROUP_UNION_OUT
		|| etype == MPI_INTERCOMM_CREATE_OUT
		|| etype == MPI_INTERCOMM_MERGE_OUT
		|| etype == MPI_KEYVAL_CREATE_OUT
		|| etype == MPI_KEYVAL_FREE_OUT
		|| etype == MPI_ABORT_OUT
		|| etype == MPI_ERROR_CLASS_OUT
		|| etype == MPI_ERRHANDLER_CREATE_OUT
		|| etype == MPI_ERRHANDLER_FREE_OUT
		|| etype == MPI_ERRHANDLER_GET_OUT
		|| etype == MPI_ERROR_STRING_OUT
		|| etype == MPI_ERRHANDLER_SET_OUT
		|| etype == MPI_GET_PROCESSOR_NAME_OUT
		|| etype == MPI_INITIALIZED_OUT
		|| etype == MPI_WTICK_OUT
		|| etype == MPI_WTIME_OUT
		|| etype == MPI_ADDRESS_OUT
		|| etype == MPI_BSEND_OUT
		|| etype == MPI_BSEND_INIT_OUT
		|| etype == MPI_BUFFER_ATTACH_OUT
		|| etype == MPI_BUFFER_DETACH_OUT
		|| etype == MPI_CANCEL_OUT
		|| etype == MPI_REQUEST_FREE_OUT
		|| etype == MPI_RECV_INIT_OUT
		|| etype == MPI_SEND_INIT_OUT
		|| etype == MPI_GET_ELEMENTS_OUT
		|| etype == MPI_GET_COUNT_OUT
		|| etype == MPI_IBSEND_OUT
		|| etype == MPI_IPROBE_OUT
		|| etype == MPI_IRECV_OUT
		|| etype == MPI_IRSEND_OUT
		|| etype == MPI_ISEND_OUT
		|| etype == MPI_ISSEND_OUT
		|| etype == MPI_PACK_OUT
		|| etype == MPI_PACK_SIZE_OUT
		|| etype == MPI_PROBE_OUT
		|| etype == MPI_RSEND_OUT
		|| etype == MPI_RSEND_INIT_OUT
		|| etype == MPI_SENDRECV_OUT
		|| etype == MPI_SENDRECV_REPLACE_OUT
		|| etype == MPI_SSEND_OUT
		|| etype == MPI_SSEND_INIT_OUT
		|| etype == MPI_START_OUT
		|| etype == MPI_STARTALL_OUT
		|| etype == MPI_TEST_OUT
		|| etype == MPI_TESTALL_OUT
		|| etype == MPI_TESTANY_OUT
		|| etype == MPI_TEST_CANCELLED_OUT
		|| etype == MPI_TESTSOME_OUT
		|| etype == MPI_TYPE_COMMIT_OUT
		|| etype == MPI_TYPE_CONTIGUOUS_OUT
		|| etype == MPI_TYPE_EXTENT_OUT
		|| etype == MPI_TYPE_FREE_OUT
		|| etype == MPI_TYPE_HINDEXED_OUT
		|| etype == MPI_TYPE_HVECTOR_OUT
		|| etype == MPI_TYPE_INDEXED_OUT
		|| etype == MPI_TYPE_LB_OUT
		|| etype == MPI_TYPE_SIZE_OUT
		|| etype == MPI_TYPE_STRUCT_OUT
		|| etype == MPI_TYPE_UB_OUT
		|| etype == MPI_TYPE_VECTOR_OUT
		|| etype == MPI_UNPACK_OUT
		|| etype == MPI_WAIT_OUT
		|| etype == MPI_WAITALL_OUT
		|| etype == MPI_WAITANY_OUT
		|| etype == MPI_WAITSOME_OUT
		|| etype == MPI_CART_COORDS_OUT
		|| etype == MPI_CART_CREATE_OUT
		|| etype == MPI_CART_GET_OUT
		|| etype == MPI_CART_MAP_OUT
		|| etype == MPI_CART_SHIFT_OUT
		|| etype == MPI_CARTDIM_GET_OUT
		|| etype == MPI_DIMS_CREATE_OUT
		|| etype == MPI_GRAPH_CREATE_OUT
		|| etype == MPI_GRAPH_GET_OUT
		|| etype == MPI_GRAPH_MAP_OUT
		|| etype == MPI_GRAPH_NEIGHBORS_OUT
		|| etype == MPI_GRAPH_NEIGHBORS_COUNT_OUT
		|| etype == MPI_GRAPHDIMS_GET_OUT
		|| etype == MPI_TOPO_TEST_OUT
		|| etype == MPI_RECV_IDLE_OUT
		|| etype == MPI_CART_RANK_OUT
		|| etype == MPI_CART_SUB_OUT){
			[ret addObject: [self addPajeSetState: @"running" time: time container: container]];
	}
	[ret autorelease];
	return ret;
}
@end
