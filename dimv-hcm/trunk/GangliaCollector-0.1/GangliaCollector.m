#import "DIMVCollector.h"
#import <Foundation/NSAutoreleasePool.h>
#import <Foundation/NSArray.h>
#import <Foundation/NSString.h>
#import <Foundation/NSDictionary.h>
#include <unistd.h>
#import "GetGangliaData.h"
#import "GCConf.h"
#import "GCDefs.h"

#define N_DATA_STR_SIZE (24)

int main(int argc, char **argv)
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	NSString *gangliaData;
	NSString *confFileName;
	NSMutableString *concatenatedData;
	GCConf *conf;
	int i, n_data, polling_period;
	BOOL ret;
	char recv_buf[GANGLIA_MAX_RECV_SIZE];
	char host[GC_HOST_STR_SIZE], port[GC_PORT_STR_SIZE];

	if(argc < 2){
		printf("Error: Insufficient arguments!\n");
		return 1;
	}

	/*Initialize the configuration with a config file.*/
	confFileName = [NSString stringWithCString: argv[1]];
	RETAIN(confFileName);
	AUTORELEASE(confFileName);
	conf = [[GCConf alloc] initWithConfFile: confFileName];
	
	//initialize the DIMVCollector object
	DIMVCollector *collector = [[DIMVCollector alloc] 
	  initWithId: [conf getCollectorId]
	  andType: [conf getCollectorType]
	  andAggregators: [conf getAggregators]];
	
	//get the host name
	ret = [conf getHostAsCString: host];
	if(ret == NO){
		return 1;
	}

	//get the port
	ret = [conf getPortAsCString: port];
	if(ret == NO){
		return 1;
	}

	//Get the number of data receivings to accumulate before sending them.
	n_data = [conf getNData];
	polling_period = [conf getPollingPeriod];

	RELEASE(conf);//The configuration has been completely read.
	
	while(1){//do the work
		concatenatedData = [NSMutableString alloc];
		for(i = 0; i < n_data; i++){
			usleep(polling_period);
			if (getGangliaData(host, port, recv_buf) == 0){
				gangliaData = [NSString stringWithCString: recv_buf];
				RETAIN(gangliaData);
				AUTORELEASE(gangliaData);
				[concatenatedData appendString: gangliaData];
			}
		}
			AUTORELEASE(concatenatedData);
			[collector sendData: concatenatedData];
	}
	RELEASE(collector);
	[pool release];
	return 0;
}
