/*
    DIMVisual, the Data Integration Model for Visualization of trace files.
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __INTEGRATOR_LIB_H
#define __INTEGRATOR_LIB_H
//#include "../config.h"
#include <sys/time.h>
#include <Foundation/Foundation.h>
#include <GenericEvent/GTimestamp.h>
#include <Paje/PajeHierarchy.h>
#include "Order.h"
#include "Command.h"
#include "Integrator.h"

@interface IntegratorLib : Integrator
{

	/* AS LIBRARY */
	BundleCenter *bundleCenter;
	NSMutableArray *outputArrayAsLibrary;
        PajeHeaderCenter *headcenter;
	NSMutableArray *hierarchies;
	NSMutableArray *HCMDataSources;
}
- (NSMutableArray *) convert;
- (NSArray *) dimvisualBundlesAvailable;
- (BOOL) loadDIMVisualBundle: (NSString *) name;
- (BOOL) isDIMVisualBundleLoaded: (NSString *) name;
- (NSDictionary *) getConfigurationOptionsFromDIMVisualBundle: (NSString *)name;
- (BOOL) setConfiguration: (NSDictionary *) conf forDIMVisualBundle: (NSString *) name;
- (PajeHeaderCenter *) pajeHeaderCenter;
- (BOOL)launchDIMVClientWithId: (NSString *)clientId andAggregators: (NSArray *)aggregNames;
- (BOOL)subscribeToCollector: (NSString *)collectorId;
- (BOOL)unsubscribeToCollector: (NSString *)collectorId;
- (void)setHCMSubscriptionHandler: (id<HCMSubscriptionHandler>)aSubscriptionHandler;
@end

#endif
