#import "Classe.h"
@implementation Classe
- (void)fazAlgo: (id)arg
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc]init];
	NSPort * port = [NSSocketPort port];
	NSConnection *conn = [NSConnection connectionWithReceivePort: port sendPort: port];
	[conn retain];
	[conn setRootObject: self];
	if (![conn registerName: @"objetoRemoto" 
	withNameServer: [NSSocketPortNameServer sharedInstance]]){
		NSLog(@"Registro do nome falhou!");
	}
	[initLock lock];
	[initLock unlockWithCondition: 1];
	NSLog(@"Runlooparei!");
	[[NSRunLoop currentRunLoop] run];
	[conn release];
	[pool release];
}

- (Classe *)init
{
	self=[super init];
	objetoRemoto = nil;
	initLock = [[NSConditionLock alloc] initWithCondition: 0];
	NSThread * umaThread = [[NSThread alloc] initWithTarget: self
	  selector: @selector(fazAlgo:) object: nil];
	NSLog(@"Esperando runloopar.");
	[umaThread start];
	[initLock lockWhenCondition: 1];
	NSLog(@"Runloopando!");
	connecThread = umaThread;
	return self;
}

- (void)imprime: (id)arg
{
	NSLog(@"%@", (NSString *)arg);
}

- (void) registraRemoto: (id<Classe>) umObjeto
{
	objetoRemoto = [umObjeto retain];
}
- (void) connecImprimeRemoto: (id)mensagem
{
	[mensagem retain];
	if(objetoRemoto != nil){
		[objetoRemoto imprime: mensagem];
	}
	[mensagem autorelease];
}

- (void)imprimeRemoto: (NSString *)mensagem
{
	[mensagem retain];
	[self performSelector: @selector(connecImprimeRemoto:)
	  onThread: connecThread withObject: mensagem
	  waitUntilDone: YES];
	[mensagem autorelease];
}
@end
