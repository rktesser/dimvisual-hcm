#import <Foundation/NSString.h>
#import <Foundation/NSArray.h>
#import <Foundation/NSDictionary.h>
#import <string.h>
#import <stdio.h>
#import <unistd.h>
#import "GCDefs.h"

#define N_DATA_STR_SIZE (24)

@interface GCConf : NSObject
{
	NSString *host;
	NSString *port;
	NSString *collectorId;
	NSString *collectorType;
	NSArray *aggregators;
	int nData;
	int pollingPeriod;
}

- (GCConf *) initWithConfFile: (NSString *) confFileName;

- (BOOL)getHostAsCString: (char *)host;
- (BOOL)getPortAsCString: (char *)host;
- (int)getNData;
- (int)getPollingPeriod;
- (NSArray *)getAggregators;
- (NSString *)getCollectorType;
- (NSString *)getCollectorId;
- (void) dealloc;

@end

