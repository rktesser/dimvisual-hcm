#import <Foundation/Foundation.h>
#import "Classe.h"

@interface OutraClasse: NSObject <Classe>
{
	NSConditionLock * initLock;
	NSThread *connecThread;
	id<Classe> objetoRemoto;
}
- (void)fazAlgo: (id)arg;
- (void)imprime: (id)arg;
- (void)registraRemoto: (id<Classe>) umObjeto;
- (void)imprimeRemoto: (NSString *)mensagem;
@end

