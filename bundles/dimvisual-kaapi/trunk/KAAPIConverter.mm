/*
    DIMVisual-KAAPI, the DIMVisual bundle for KAAPI trace files
    Copyright (c) 2008 Lucas Mello Schnorr <schnorr@gmail.com>

    This file is part of DIMVisual-KAAPI.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "KAAPIConverter.h"
#include <kaapi> /* for the trace and level stuff (identifier) */


@implementation KAAPIConverter
- (id) initWithProvider: (id<Integrator>) prov
{
	self = [super init];

	grid5000 = [NSDictionary dictionaryWithContentsOfFile:
			[[NSBundle bundleForClass: [self class]]
		pathForResource: @"Grid5000" ofType: @"plist"]];
/*
#ifdef ORGANIZATION_BY_PROCESS
	NSArray *entities = [[NSArray alloc] initWithObjects: @"gid", nil];
#else
	NSArray *entities = [[NSArray alloc] initWithObjects: @"gid", @"cid", nil];
#endif
	NSArray *ret = [prov entitiesExists: entities];
	if ([ret count] != 0){
		NSLog (@"KAAPIConverter: Entities %@ not defined in "
			"the hierarchy configuration provided.", entities);
		[super release];
		return nil;
	}
*/

	buffer = [[NSMutableArray alloc] init];
	links = [[NSMutableDictionary alloc] init];
	names = [[NSMutableDictionary alloc] init];
	mappingGidHostname = [[NSMutableDictionary alloc] init];
	workStealingLinks = [[NSMutableDictionary alloc] init];
	kernelRstealTimes = [[NSMutableDictionary alloc] init];


	provider = prov;
	return self;
}

- (void) dealloc
{
	[links release];
	[kernelRstealTimes release];
	[super dealloc];
}

/*
 *
 */
- (PajeSetState *) addPajeSetState: (NSString *) state toState: (NSString *)   statename time: (NSString *) timex container: (NSString *) cont
{
        PajeSetState *set = [[PajeSetState alloc] initWithTime: timex];
        [set setEntityType: [provider aliasToName: statename]];
        [set setValue: [provider aliasToName: state]];
        [set setContainer: cont];
        [set autorelease];
        return set;
}

/*
 *
 */
- (PajePushState *) addPajePushState: (NSString *) state toState: (NSString *)   statename time: (NSString *) timex container: (NSString *) cont
{
        PajePushState *set = [[PajePushState alloc] initWithTime: timex];
        [set setEntityType: [provider aliasToName: statename]];
        [set setValue: [provider aliasToName: state]];
        [set setContainer: cont];
        [set autorelease];
        return set;
}

- (PajePopState *) addPajePopState: (NSString *) statename time: (NSString *) timex container: (NSString *) cont
{
        PajePopState *set = [[PajePopState alloc] initWithTime: timex];
        [set setEntityType: [provider aliasToName: statename]];
        [set setContainer: cont];
        [set autorelease];
        return set;
}

- (id) cleanBuffer
{
	NSMutableArray *ret;
	ret = [[NSMutableArray alloc] init];
	unsigned int i;

	for (i = 0; i < [buffer count]; i++){
		GEvent *g = [buffer objectAtIndex: i];
		[ret addObjectsFromArray: [self convertEvent: g]];
/*
		GFields *f = [g fields];
		NSString *t = [[f fieldWithSimpleStringKey:@"thread"] description];
		NSString *typeOfThread = [names objectForKey: t];
		if (typeOfThread != nil){
			NSLog (@"typeOfThread defined %@", typeOfThread);
			[ret addObjectsFromArray: [self convertEvent: g]];
			[buffer removeObjectAtIndex: i];
			i = 0;
		}
*/
	}
	[buffer removeAllObjects];
	[ret autorelease];
	return ret;
}

/*
 *
 */
- (id) convertEvent: (GEvent *) event
{
	NSMutableArray *ret = [[NSMutableArray alloc] init];
	int ilevelid, ieventid;

//	NSLog (@"(%s) event = %@", __FUNCTION__, event);
	
	GFields *f = [event fields];

	gid = [[f fieldWithSimpleStringKey:@"gid"] description];
	cid = [[f fieldWithSimpleStringKey:@"cid"] description];
	NSString *cidtype = [[f fieldWithSimpleStringKey:@"threadName"] description];
	time = [[event timestamp] description];
	levelid = [[f fieldWithSimpleStringKey:@"level"] description];
	eventid = [[f fieldWithSimpleStringKey:@"event"] description];
	hostname = [[f fieldWithSimpleStringKey:@"hostname"] description];
	[mappingGidHostname setObject: hostname forKey: gid];
	[mappingGidHostname setObject: gid forKey: hostname];

	ilevelid = atoi([levelid cString]);
	ieventid = atoi([eventid cString]);
	
	if (hostname == nil){
		NSString *str;
		str = [NSString stringWithFormat: @"KAAPIConverter (%@): "
			"hostname is not defined. It expect a hostname in "
			"the trace", self];
		[[NSException exceptionWithName: @"DIMVisual-KAAPIConverter" 
			reason: str userInfo: nil] raise];
	}

	NSString *gidhostname;
	gidhostname = [NSString stringWithFormat: @"%@_at_%@", gid, hostname];

#ifdef SEARCH_THREAD_NAME
	NSString *typeOfThread;
	if ([self isEventUtilThreadName]){
		[self convertEventUtilThreadName: event];
//		NSLog (@"defining name %@ for thread %@", [names objectForKey: cid], cid);
		[ret addObjectsFromArray: [self cleanBuffer]];
		[ret autorelease];
		return ret;
	}

	typeOfThread = [names objectForKey: cid];;
	if (typeOfThread == nil){ 
//		NSLog (@"add type of thread = %@ is null", cid);
		[buffer addObject: event];
		[ret autorelease];
		return ret;
	}
	NSString *threadname = [NSString stringWithFormat: @"%@-%@", cid,
typeOfThread];
	cid = threadname;
#endif

	if (conversionType == Process){
	}else if (conversionType == Thread){
	}else if (conversionType == Grid){
	}else if (conversionType == GridThread){
		if ([cidtype isEqualToString: @"7"]){ //only k-processor
			[ret addObjectsFromArray: 
				[self convertKProcessorWithEvent: event]];
		}else if ([cidtype isEqualToString: @"6"]){ //sock_scavanger
			[ret addObjectsFromArray: 
				[self convertSockScavangerWithEvent: event]];
		}
	}

	[ret autorelease];
	return ret;
}

- (void) setConversionType: (KAAPIConversionType) convType
{
	conversionType = convType;
}
@end
