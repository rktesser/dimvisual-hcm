#import "DIMVCollector.h"
#import <Foundation/NSAutoreleasePool.h>
#import <Foundation/NSFileManager.h>
#import <Foundation/NSFileHandle.h>
#import <Foundation/NSThread.h>
#include <sys/time.h>
#include <unistd.h>

@interface TestCollector: NSObject
{
}
+ (void)collectDataForCollector: (id)collector;
@end

@implementation TestCollector
+ (void)collectDataForCollector: (id)aCollector
{
	DIMVCollector *collector;
	NSString *collecId;
	unsigned i;
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	#ifdef DIMV_EVALUATE
        NSFileHandle * timesGC;
        NSString * timesGCFName;
        struct timeval to, t1;
        unsigned long long to_usec, t1_usec;
        NSFileManager * fManager = [NSFileManager defaultManager];
        RETAIN(fManager);
        #endif	
	collector = (DIMVCollector *)aCollector;
	[collector retain];
	collecId = [[collector getId] retain];
	#ifdef DIMV_EVALUATE
        timesGCFName = [collecId stringByAppendingString:
           @"-timesGC.output"];
        RETAIN(timesGCFName);
        if([fManager createFileAtPath: timesGCFName contents: nil
         attributes: nil] == NO){
                NSLog(@"Couldn't create the file %@\n", timesGCFName);
                exit(1);
        }
        RELEASE(fManager);
        timesGC = [NSFileHandle fileHandleForWritingAtPath: timesGCFName];
        RETAIN(timesGC);
        RELEASE(timesGCFName);
        #endif
	for(i = 0; 1; i++){
		#ifdef DIMV_EVALUATE
		gettimeofday(&to, NULL);
		[collector sendData: [NSString stringWithFormat:@"data number %d from %@", i, collecId] counter: i];
		gettimeofday(&t1, NULL);
                to_usec = to.tv_usec + (to.tv_sec * 1000000);
                t1_usec = t1.tv_usec + (t1.tv_sec * 1000000);
                [timesGC writeData: [[NSString stringWithFormat:
                @"%@ %010u %llu %llu\n", collecId,
                i, to_usec, (t1_usec - to_usec)]
                dataUsingEncoding: NSUTF8StringEncoding]];
		#else
		[collector sendData: [NSString stringWithFormat:@"data number %d from %@", i, collecId]];
				
		#endif
		usleep(10000);
	}
	#ifdef DIMV_EVALUATE
        RELEASE(timesGC);
        #endif
	RELEASE(collecId);
	RELEASE(collector);
	[pool release];
}
@end

int main(int argc, char **argv)
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	NSMutableArray *aggregators;
	NSString *collecType;
	NSString *collecId;
	unsigned i;
	if(argc < 4){
		printf("Error: Insufficient arguments!\n");
		return 1;
	}

	collecId = [[NSString alloc] initWithCString: argv[1]];//the first argument is the Id of the collector
	NSLog(@"Suplied collector id is: %@\n", collecId);
	collecType = [[NSString alloc] initWithCString: argv[2]];
	NSLog(@"Suplied collector type is: %@\n", collecType);
	aggregators = [NSMutableArray arrayWithCapacity: 5];
	RETAIN(aggregators);
	for(i = 3; i < argc; i++){//populate the array of Aggregator names
		[aggregators addObject: [[NSString alloc] initWithCString: argv[i]]];
		NSLog(@"\"%@\" has been added to the aggregator names list.\n", [aggregators objectAtIndex: (i - 3)]);
	}
	DIMVCollector *collector = [[DIMVCollector alloc] initWithId: collecId andType: collecType andAggregators: aggregators];
	RELEASE(collecType);
	RELEASE(aggregators);
	[TestCollector collectDataForCollector: collector];
	RELEASE(collecId);
	RELEASE(collector);
	[pool release];
	return 0;
}
