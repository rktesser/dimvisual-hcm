#import "DIMVCollector.h"

@implementation DIMVCollector

- (void)runConnections: (id)arg
{
	int i;
	id remoteAggregator; 
	NSString *aggrName;

	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	NSArray *aggregatorNames=(NSArray *)arg;
	[connectionLock lock];
	for(i = 0; i < [aggregatorNames count]; i++)//connect to the aggregators
	{
		aggrName = [aggregatorNames objectAtIndex: i];
	 	NSLog(@"Connecting with: %@\n", aggrName);
		remoteAggregator = (id<DIMVAggregator>) [self connectWithAggregator: aggrName];
		[aggregators setObject: remoteAggregator forKey: [remoteAggregator getId]]; 
		[remoteAggregator registerCollector: self];
	}
	[connectionLock unlockWithCondition: COLLECTORINITIALIZED_COND];
	[[NSRunLoop currentRunLoop] run];
	[pool release];
}

- (DIMVCollector *) initWithId: (NSString *) colId  andType: (NSString *) colType andAggregators: (NSArray *) aggregatorNames
{
/*	int i = 0;
	id remoteAggregator; 
	NSString *aggrName;
*/
	self = [super init];
	ASSIGN(collectorId, colId);
	ASSIGN(type, colType);
	aggregators = [NSMutableDictionary dictionary];
	RETAIN(aggregators);
	RETAIN(aggregatorNames);
	subscribers = [[NSMutableDictionary alloc] init];
	connectionLock = [[NSConditionLock alloc] 
	  initWithCondition: COLLECTORNOTINITIALIZED_COND];
/*	
	for(i=0; i < [aggregatorNames count];i++)//conect to the aggregators
	{
		aggrName = [aggregatorNames objectAtIndex: i];
	 	NSLog(@"Connecting with: %@\n", aggrName);
		remoteAggregator = (id<DIMVAggregator>) [self connectWithAggregator: aggrName];
		[aggregators setObject: remoteAggregator forKey: [remoteAggregator getId]]; 
		[remoteAggregator registerCollector: self];
	}*/
	connectionThread = [[NSThread alloc] initWithTarget: self
	  selector: @selector(runConnections:) object: aggregatorNames];
	AUTORELEASE(aggregatorNames);
	[connectionThread start];
	/*Wait for the connnections to be estabilished.*/
	[connectionLock lockWhenCondition: COLLECTORINITIALIZED_COND];
	/*Now the connectionThread must be initialized.*/
	[connectionLock unlock];
	return self;
}

- (void) dealloc
{
	RELEASE(collectorId);
	RELEASE(type);
	RELEASE(aggregators);
	TEST_RELEASE(subscribers);
	[super dealloc];
}

- (void) subscribe: (NSString *) aggregatorId
{
	NSLog(@"Aggregator \"%@\" required subscription.\n", aggregatorId);
	return;
}

- (void) unsubscribe: (NSString *) aggregatorId
{
	return;
}

- (id<DIMVAggregator>) connectWithAggregator: (NSString *)name
{
	id remoteAggregator;
	do{
		remoteAggregator = (id<DIMVAggregator>) [NSConnection rootProxyForConnectionWithRegisteredName: name host: @"*" usingNameServer: [NSSocketPortNameServer sharedInstance]];
		if (remoteAggregator == nil){
			NSLog(@"Error: Couldn\'t connect with aggregator with name: %@\n", name);
			[NSThread sleepForTimeInterval: DIMV_CONN_RETRY_WAIT];
		}
	}while (remoteAggregator == nil);
	RETAIN(remoteAggregator);
	[[NSNotificationCenter defaultCenter] addObserver: [Observer new] selector: @selector(connectionDied:) name: NSConnectionDidDieNotification object: [(NSDistantObject *) remoteAggregator connectionForProxy]];

	AUTORELEASE(remoteAggregator);
	return remoteAggregator;
}

- (BOOL) registerWithAggregator: (id<DIMVAggregator>)remoteAggregator
{
	if(remoteAggregator != nil){
		[remoteAggregator registerCollector: self];
		return YES;
	}else{
		return NO;
	}
}


- (NSString *) getId
{
	return collectorId;
}

- (NSString *) getType
{
	return type;
}

#ifdef DIMV_EVALUATE
- (void) internalSendData: (id) data counter: (unsigned)counter
#else
- (void) internalSendData: (id) data
#endif
{
	NSEnumerator * aggregEnum;
	id remoteAggregator;
	RETAIN(data);
	aggregEnum = [subscribers objectEnumerator];
//	NSLog(@"I will send some data\n");
	while((remoteAggregator = (id<DIMVAggregator>)[aggregEnum nextObject])){
//		NSLog(@"Sending data to an aggregator.\n");
		#ifndef DIMV_EVALUATE
		[remoteAggregator receiveData: data fromCollector: collectorId];
		#else
		[remoteAggregator receiveData: data fromCollector: collectorId counter: counter];
		#endif
	}
	RELEASE(data);
}

- (void)connectionSendData: (id)arg
{
	NSArray *arguments=(NSArray *)arg;
	RETAIN(arguments);
	id data = [arguments objectAtIndex: 0];
	#ifdef DIMV_EVALUATE
	unsigned counter;
	[[arguments objectAtIndex: 1] getValue: &counter];
	[self internalSendData: data counter: counter];
	#else
	[self internalSendData: data];
	#endif
	RELEASE(arguments);
}

#ifdef DIMV_EVALUATE
- (void) sendData: (id) data counter: (unsigned)counter
#else
- (void) sendData: (id) data
#endif
{
	NSArray *arg;
	RETAIN(data);
	#ifdef DIMV_EVALUATE
	arg = [NSArray arrayWithObjects: data, 
	  [NSValue value: &counter withObjCType: @encode(unsigned)], nil];
	#else
	arg = [NSArray arrayWithObjects: data, nil];
	#endif
	RELEASE(data);
	[self performSelector: @selector(connectionSendData:)
	  onThread: connectionThread withObject: arg waitUntilDone: NO];
}


- (NSString *)getLocation
{
	return [[NSHost localHost] name];
}

- (void)subscribeComponent: (NSString*)componentId toCollector:
(NSString *)aCollectorId
{
	id component;
	RETAIN(componentId);
	RETAIN(aCollectorId);
	if([aCollectorId compare: collectorId] != NSOrderedSame){
		NSLog(@"ERROR: Wrong collector id in subscription request.");
	}else{
		component = (id<DIMVAggregator>)
		  [aggregators objectForKey: componentId];
		if(component == nil){
			NSLog(@"Error: Invalid aggregator Id in subscription request");
		}else{
			[subscribers setObject: component forKey: componentId];
			NSLog(@"Registered subscription for \"%@\".", componentId);
		}
	}
	RELEASE(componentId);
	RELEASE(aCollectorId);
	return;
}

- (void) unsubscribeComponent: (NSString*)componentId toCollector:
(NSString *)aCollectorId
{
	id component;
	RETAIN(componentId);
	RETAIN(aCollectorId);
	if([aCollectorId compare: collectorId] != NSOrderedSame){
		NSLog(@"ERROR: Wrong collector id in unsubscription request.");
	}else{
		component = (id<DIMVAggregator>)[subscribers objectForKey: componentId];
		if(component == nil){
			NSLog(@"Error: Unsubscription impossible. There's no registered subscription for aggregator with Id \"%@\"", componentId);
		}else{
			[subscribers removeObjectForKey: componentId];
			NSLog(@"Unsubscribed  \"%@\" to this collector.", componentId);
		}
	}
	RELEASE(componentId);
	RELEASE(aCollectorId);
	return;
}
@end
