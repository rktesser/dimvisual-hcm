#import "FileWriterSource.h"
@implementation FileWriterSource
- (void)writeToFile: (NSString *)data withDate: (NSDate *)recDate
{
	NSData *dataAsData;
	double timestamp;
	NSString *metaData;
	unsigned dataSize;
	[data retain];
	timestamp = [recDate timeIntervalSinceDate: startDate];
	dataAsData = [data dataUsingEncoding: NSISOLatin1StringEncoding];
	[dataAsData retain];
	[data release];
	dataSize = [dataAsData length];
	[dataFile writeData: dataAsData];
	[dataAsData release];
	metaData = [NSString stringWithFormat: @",(\"%@\",\"%lf\",\"%u\")",
	  type, timestamp, dataSize];
	[metaFile writeData: [metaData dataUsingEncoding:
	  NSUTF8StringEncoding]];
	[metaFile synchronizeFile];
}

- (void)initWriterThread: (id)args
{
//	NSLog(@"%s", __PRETTY_FUNCTION__);
	int condition, i;
	NSString *data;
	NSArray *dataItem;
	NSDate *recDate;
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	[metaFile writeData: [@"((\"BEGIN\",\"IGNORE\")" dataUsingEncoding: NSUTF8StringEncoding]];
	while(!finish){
		[dataLock lockWhenCondition: 1];
		if(finish){
			[dataLock unlock];
			continue;
		}
		dataItem = [[dataBuffer objectAtIndex: 0] retain];
		[dataBuffer removeObjectAtIndex: 0];
		condition = (([dataBuffer count] > 0)?1:0);
		[dataLock unlockWithCondition: condition];
		recDate = [dataItem objectAtIndex: 0];
		data = [dataItem objectAtIndex: 1];
		[self writeToFile: data withDate: recDate];
		[dataItem release];
	}
	[dataLock lock];
	NSLog(@"Writing data that remains in the buffer.");
	for(i = 0; i < [dataBuffer count]; i++){
		dataItem = [[dataBuffer objectAtIndex: i] retain];
		recDate = [dataItem objectAtIndex: 0];
		data = [dataItem objectAtIndex: 1];
		[self writeToFile: data withDate: recDate];
		[dataItem release];
	}
	[dataBuffer removeAllObjects];
	[metaFile writeData: [@")\n" dataUsingEncoding: NSUTF8StringEncoding]];
	[dataLock unlock];
	[pool release];
}

- (FileWriterSource *)initWithType: (NSString *)aType;
{
	NSString *metaFName;
	NSString *fName;
	self = [super init];
	type = [aType retain];
	startDate = [[NSDate date] retain];
	count = 0;
	finish = NO;
	dataBuffer = [[NSMutableArray alloc] init];
	metaFName = [NSString stringWithFormat: @"HCM-%@.HCMMeta", type];
	[[NSFileManager defaultManager] createFileAtPath: metaFName
	  contents: nil attributes: nil];
	metaFile = [NSFileHandle fileHandleForWritingAtPath: metaFName];
	[metaFile retain];
	fName = [NSString stringWithFormat: @"HCM.HCMData"];
	[[NSFileManager defaultManager] createFileAtPath: fName 
	  contents: nil attributes: nil];
	dataFile = [[NSFileHandle fileHandleForWritingAtPath: fName] retain];
	dataLock = [[NSConditionLock alloc] initWithCondition: NO];
	writerThread = [[NSThread alloc] initWithTarget: self 
	  selector: @selector(initWriterThread:) object: nil];
	[writerThread start];
	return self;
}

- (void)dealloc
{
	[writerThread release];
	[dataBuffer release];
	[metaFile closeFile];
	[metaFile release];
	[dataFile closeFile];
	[dataFile release];
	[dataLock release];
	[super dealloc];
}

- (void) inputData: (id)data
{	
/*	NSLog(@"%s", __PRETTY_FUNCTION__);*/
	NSDate *now;
	NSArray *dataItem;
	[data retain];
	now = [NSDate date];
	dataItem = [NSArray arrayWithObjects: now, data, nil];
	[data release];
	[dataLock lock];
	if(finish){
		[dataLock unlockWithCondition: YES];
		return;
	}
	[dataBuffer addObject: dataItem]; 
	[dataLock unlockWithCondition: YES];
	return;
}

- (void)finish
{
	finish = YES;
	NSLog(@"Waiting for the writer thread to finish.");
	[dataLock lock];
	[dataLock unlockWithCondition: 1];
	while(![writerThread isFinished]){
		[NSThread sleepForTimeInterval: 0.5];
	}
	NSLog(@"The writer thread is finished.");
}
@end


